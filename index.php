<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");


$APPLICATION->SetTitle("Портал VisageHall");

$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "main_page",
    array(
        "ROOT_MENU_TYPE" => "left",
        "MAX_LEVEL" => "3",
        "CHILD_MENU_TYPE" => "left",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => array(),
        "COMPONENT_TEMPLATE" => "main_page"
    ),
    false
);


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
<?php
/** @var CBitrixComponent $this */

$arResult = [];

$arResult['DIV_ID'] = $arParams['DIV_ID'] = !empty($arParams['DIV_ID']) ? $arParams['DIV_ID'] : uniqid('marketing-order-');

$arResult['IS_ORDER_PAGE'] = !empty($arParams['ORDER_ID']);
$pHelper = \VH\Portal\Helper::getInstance();
if ($arParams['SHOW_ALL'] === 'Y'){
    if ($arParams['ADMIN_MODE'] !== 'Y') {
        $iter = \Bitrix\Sale\Order::getList(['filter' => array('=USER_ID' => $USER->GetID())]);
        $orders = [];
        while ($i = $iter->fetch()) {
            $orders[] = $i['ACCOUNT_NUMBER'];
        }
        $filter = ['@BASKET_PROP.VALUE' => array_column($pHelper->getUserBrands() ?: [], 'UF_NAME')];
    } else {
        $filter = [];
    }
} else {
    $filter = [
        '=ORDER_ID' => $arParams['ORDER_ID'],
    ];
}

$filter['=BASKET_PROP.CODE'] = 'BRAND';

$iter = ExtBasketItemTable::getList([
    'filter' => $filter,
    'select' => [
        '*', 'MONTH',
        'PRODUCT_ID' => 'BASKET.PRODUCT.ID',
        'SHOP_ID' => 'SHOP.VALUE',
        'DISCOUNT' => 'DISCOUNT_PROP.VALUE',
        'BRAND' => 'BASKET_PROP.VALUE',
        'NAME' => 'BASKET.NAME',
        'PRICE' => 'BASKET.PRICE',
        'QUANTITY' => 'BASKET.QUANTITY',
    ],
    'order' => [
        'DATE_FROM' => 'ASC',
        'ID'        => 'ASC',
    ],
    'runtime' => [
        new Bitrix\Main\ORM\Fields\Relations\Reference(
            'SHOP',
            Bitrix\Sale\Internals\BasketPropertyTable::class,
            array(
                '=this.BASKET_ID' => 'ref.BASKET_ID',
                '=ref.CODE' => new Bitrix\Main\DB\SqlExpression("'SHOP'"),
            )
        ),
        new Bitrix\Main\ORM\Fields\Relations\Reference(
            'DISCOUNT_PROP',
            Bitrix\Sale\Internals\BasketPropertyTable::class,
            array(
                '=this.BASKET_ID' => 'ref.BASKET_ID',
                '=ref.CODE' => new Bitrix\Main\DB\SqlExpression("'DISCOUNT'"),
            )
        ),
    ]
]);

$arResult['ITEMS'] = [];
$arResult['ITEMS_BY_MONTH'] = [];

$mapProductToItem = [];
while ($t = $iter->fetch()){
    $arResult['ITEMS'][] = &$t;
    $arResult['ITEMS_BY_MONTH'][$t['MONTH']][] = &$t;

    $mapProductToItem[$t['PRODUCT_ID']][] = count($arResult['ITEMS']) - 1;
    unset($t);
}
unset($t);

/**
 * Разделы
 */
$iter = CIBlockElement::GetList(
    [],
    ['=ID' => array_keys($mapProductToItem)],
    false,
    false,
    ['ID', 'IBLOCK_SECTION_ID', 'PROPERTY_CML2_LINK.IBLOCK_SECTION_ID']
);
$mapSectionToProduct = [];
while ($i = $iter->Fetch()){
    $sectionId = $i['PROPERTY_CML2_LINK_IBLOCK_SECTION_ID'] > 0
        ? $i['PROPERTY_CML2_LINK_IBLOCK_SECTION_ID']
        : $i['IBLOCK_SECTION_ID'];
    if (!$sectionId) continue;
    $mapSectionToProduct[$sectionId][] = $i['ID'];
}

$iter = CIBlockSection::GetList(
    [],
    ['@ID' => array_keys($mapSectionToProduct)],
    false,
    ['ID', 'NAME']
);
while ($i = $iter->Fetch()){
    foreach ($mapSectionToProduct[$i['ID']] as $productId){
        foreach ($mapProductToItem[$productId] as $index){
            $arResult['ITEMS'][ $index ]['SECTION_NAME'] = $i['NAME'];

            if ($GLOBALS['USER']->IsAdmin()){
//                p($index);
            }
        }
    }
}


/**
 * Магазины
 */

$shopsIter = CIBlockElement::GetList(
    ['sort' => 'asc'],
    ['=IBLOCK_ID' => CATALOG_SHOPS, 'ACTIVE' => 'Y'],
    false,
    false,
    [
        'ID', 'IBLOCK_ID', 'NAME', 'CODE',
        'IBLOCK_SECTION_ID',
        'PROPERTY_ADDRESS', 'PROPERTY_BIRTHDAY',
        'PROPERTY_POST_DATE_FROM', 'PROPERTY_POST_DATE_TO'
    ]
);
$shopCities = [];
$shops      = [];
while ($shop = $shopsIter->Fetch()){
    if (!empty($shop['IBLOCK_SECTION_ID']) && empty($shopCities[$shop['IBLOCK_SECTION_ID']])){
        $shopCities[$shop['IBLOCK_SECTION_ID']] = CIBlockSection::GetByID($shop['IBLOCK_SECTION_ID'])->Fetch();
    }

    $shops[$shop['ID']] = [
        'ID' => $shop['ID'],
        'CODE' => $shop['CODE'],
        'NAME' => $shop['NAME'],
        'CITY' => $shopCities[$shop['IBLOCK_SECTION_ID']]['NAME'],
        'ADDRESS' => $shop['PROPERTY_ADDRESS_VALUE'],
        'BIRTHDAY' => $shop['PROPERTY_BIRTHDAY_VALUE'],
        'POST_DATE_FROM' => $shop['PROPERTY_POST_DATE_FROM_VALUE'],
        'POST_DATE_TO' => $shop['PROPERTY_POST_DATE_TO_VALUE'],
    ];
}

$arResult['SHOPS'] = $shops;

$this->IncludeComponentTemplate();
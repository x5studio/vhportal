<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

$productId = (int) $_REQUEST['product_id'];
$action    = (string) $_REQUEST['action'];


if (empty($productId) || empty($action)){
    die(json_encode([
        'success' => false,
        'error'  => 'Неверно указан товар или действие'
    ]));
}

$iblock = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_MARKETING);


// Получение товара
$element = $iblock->getEntityDataClass()::getList([
    'filter' => ['=ID' => $productId],
    'select' => [
        '*',
        'SECTION_NAME' => 'IBLOCK_SECTION.NAME',
        'SECTION_CODE' => 'IBLOCK_SECTION.CODE'
    ],
])->fetchObject();


if (empty($element)){
    die(json_encode([
        'success' => false,
        'error'  => 'Товар не найден'
    ]));
}

$siteTemplatePath = SITE_TEMPLATE_PATH;

switch ($action){
    case 'desc_popup':
        try {
            $content = $element->getPopupContent();
        } catch (\Throwable $e){
            $content = null;
        }

        if (empty($content)){
            $content = CIBlockElement::GetProperty($element->getIblockId(), $element->getId(), [], ['CODE' => 'POPUP_CONTENT'])->Fetch()['VALUE'];
        }


        if (empty($content)){
            die(json_encode([
                'success' => false,
                'error'  => 'Ошибка...'
            ]));
        }

        $img = $element->getDetailPicture() ?: $element->getPreviewPicture();
        if (!empty($img)){
            $img = CFile::GetPath($img);
            $img = "<div class=\"to-cart-form__img-wrap\"><img class=\"to-cart-form__img\" src=\"{$img}\" alt=\"{$element->getName()}\"></div>";
        } else {
            $img = '';
        }

        $html = <<<HTML
<div class="modal__overlay" tabindex="-1" data-micromodal-close="">
    <div class="modal-default">
        <div class="modal-close modal-close_callback" data-micromodal-close="">
            <img src="{$siteTemplatePath}/img/icons/remove.svg" alt="">
        </div>
        <div class="modal-default__content">
            <div class="to-cart-form">
                <div class="to-cart-form__wrap">
                    <div class="to-cart-form__header">
                        <div class="to-cart-form__type-wrap">
                            {$img}
                            <div class="to-cart-form__type-right">
                                <div class="to-cart-form__type">{$element->getName()}</div>
                                <div class="to-cart-form__title">
                                    <div class="to-cart-form__title-link">{$element->getIblockSection()->getName()}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {$content}
                </div>
            </div>
        </div>
    </div>
</div>
HTML;

        die(json_encode([
            'html' => $html,
        ]));
    default:
        die(json_encode([
            'success' => false,
            'error'  => 'Неверное действие'
        ]));
}

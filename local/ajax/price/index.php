<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

$productId = (int) $_REQUEST['product_id'];

$date = is_string($_REQUEST['date']) ? $_REQUEST['date'] : null;

if (empty($date) && !empty($_REQUEST['dates']) && is_array($_REQUEST['dates'])){
    $date = (string)$_REQUEST['dates'][0];
    list($date) = explode('-', $date);
}
$date = trim($date);


if (empty($date) || empty($productId)){
    die(json_encode([
        'success' => false,
        'error'  => 'Неверно указан товар или дата'
    ]));
}


// Получение товара
$product = \Bitrix\Iblock\ElementTable::getRow([
    'filter' => ['=ID' => $productId],
    'select' => [
        '*',
        'SECTION_NAME' => 'IBLOCK_SECTION.NAME',
        'SECTION_CODE' => 'IBLOCK_SECTION.CODE'
    ],
]);

$beginDate = DateTime::createFromFormat(getDateFormat(), $date);

if (empty($product) || !$beginDate){
    die(json_encode([
        'success' => false,
        'error'  => 'Неверно указан товар или дата'
    ]));
}



$propsCodes = getIblockProperties(OFFERS_CATALOG);
$propsCodes[] = 'POST_DATE_FROM';
$propsCodes[] = 'POST_DATE_TO';

$propsCodes = array_unique($propsCodes);


$offers = CCatalogSKU::getOffersList(
    $productId,
    0,
    ['ACTIVE' => 'Y'],
    ['ID', 'PRICE_'.BASE_PRICE],
    [
        'CODE' => $propsCodes
    ]
);
$offers = $offers[$productId];


$availableDates = [];
$prices         = [];
foreach ($offers as $offer){
    $from = $offer['PROPERTIES']['POST_DATE_FROM']['VALUE'];
    $to   = $offer['PROPERTIES']['POST_DATE_TO']['VALUE'];

    $prices[$offer['ID']] = $offer['PRICE_'.BASE_PRICE];

    $fromDate = DateTime::createFromFormat(getDateFormat(), $from);
    $toDate = DateTime::createFromFormat(getDateFormat(), $to);

    $tmp = [];
    foreach ($offer['PROPERTIES'] as $prop){
        if (
            empty($prop['VALUE'])
            || substr($prop['CODE'], 0, 5) === 'CML2_'
            || $prop['CODE'] === 'POST_DATE_FROM'
            || $prop['CODE'] === 'POST_DATE_TO'
        ){
            continue;
        }

        $tmp[$prop['CODE']] = array_filter($prop, function ($i){
            return strpos($i, 'VALUE') !== false;
        }, ARRAY_FILTER_USE_KEY);
    }

    $availableDates[$offer['ID']] = [
        'FROM' => $from,
        'TO'   => $to,

        'FROM_DATE' => $fromDate,
        'TO_DATE'   => $toDate,

        'PROPS'     => $tmp,
    ];
}

$offerId = getOfferByDate($beginDate, $availableDates, 0, !empty($_REQUEST['props']) && is_array($_REQUEST['props']) ? $_REQUEST['props'] : []);

if (!$offerId){
    die(json_encode([
        'success' => false,
        'error'   => 'Неверно указан товар или дата'
    ]));
}

$r = [
    'price' => $prices[$offerId] * intval($_REQUEST['quantity'] ?: 1),
    'price_formatted' => CurrencyFormat($prices[$offerId] * intval($_REQUEST['quantity'] ?: 1), BASE_CURRENCY),
    'end_date' => $availableDates[$offerId]['TO'],
    'end_date_formatted' => 'До ' . $availableDates[$offerId]['TO']
];

die(json_encode([
    'success' => true,
    'result'  => $r
]));
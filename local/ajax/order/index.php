<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

$action = $_REQUEST['action'];

$actions = [
    'item_status' => function(){
        return setStatusExtBasketItem();
    }
];

if (!isset($actions[$action])){
    CHTTP::SetStatus(403);
    die(json_encode([
        'success' => false,
    ]));
}


echo json_encode($actions[$action]());


function setStatusExtBasketItem(){

    try {
        $extItem = ExtBasketItemTable::getById($_REQUEST['id'])->fetch();
    } catch (\Throwable $e){
        return [
            'success' => false,
            'error'   => 'Не найдена запись',
            'throw'   => $e->getMessage(),
        ];
    }
    if (empty($extItem)){
        return [
            'success' => false,
            'error'   => 'Не найдено данных'
        ];
    }

    $data = [];

    $mode = $_REQUEST['mode'];
    $mode = empty($mode) ? 'request' : $mode;


    switch ($mode){
        case 'request':
            if ($_REQUEST['type'] == 'cancelByUser'){
                $res = ExtBasketItemTable::cancel($extItem['ID']);
                $data['success'] = $res->isSuccess();
                $data['error']   = implode("\n", $res->getErrorMessages());

                $tmp = $res->getData();

                $data['status'] = $tmp['STATUS'];
                $data['need_refresh'] = (bool)$tmp['NEED_REFRESH_PAGE'];
            } else {
                $data['success'] = false;
                $data['error'] = 'not implementation';
            }

            break;
        case 'admin':
            if (!$GLOBALS['USER']->IsAdmin()){
                CHTTP::SetStatus(403);
                return [
                    'success' => false,
                    'error'   => 'not admin',
                ];
            }

            if ($_REQUEST['type'] == 'confirm'){
                $res = ExtBasketItemTable::confirm($extItem['ID']);
            } else {
                $res = ExtBasketItemTable::cancel($extItem['ID']);
            }

            $data['success'] = $res->isSuccess();
            $data['error']   = implode("\n", $res->getErrorMessages());

            $tmp = $res->getData();

            $data['status'] = $tmp['STATUS'];
            $data['need_refresh'] = (bool)$tmp['NEED_REFRESH_PAGE'];

            break;
        default:
            return [
                'success' => false,
                'error'   => 'incorrect mode',
            ];
    }

    return $data;
}
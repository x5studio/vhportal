<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

$productId = (int) $_REQUEST['product_id'];

$pHelper = \VH\Portal\Helper::getInstance();

$basket = $pHelper->getBasket();

// Получение товара
$product = \Bitrix\Iblock\ElementTable::getRow([
    'filter' => ['=ID' => $productId],
    'select' => [
        '*',
        'SECTION_NAME' => 'IBLOCK_SECTION.NAME',
        'SECTION_CODE' => 'IBLOCK_SECTION.CODE'
    ],
]);

// Получение всех свойств выбранного товара
$productProps = [];
if ($product){
    CIBlockElement::GetPropertyValuesArray(
        $productProps,
        $product['IBLOCK_ID'],
        ['=ID' => $productId],
        false,
        ['GET_RAW_DATA' => 'Y']
    );
    $productProps = $productProps[$productId];
}

/**
 * Картинка товара
 */
$imageId = $productProps['SVG_IMAGE']['VALUE'];
if (empty($imageId)){
    $imageId = $product['PREVIEW_PICTURE'] ?? $product['DETAIL_PICTURE'];
}

$product['IMAGE_SRC'] = CFile::GetFileSRC(CFile::GetByID($imageId)->Fetch());
/**
 * Конец: картинка
 */

$brands = $pHelper->getUserBrands();

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

// Добавление нового элемента
if (isset($_REQUEST['add_item'])){
    $_REQUEST['brands'][] = '';
}

// При первом запросе, что бы был выбор бренда
if (empty($_REQUEST['brands']) && $request->getRequestMethod() == 'GET'){
    $_REQUEST['brands'] = [''];
}

$selectedBrands = $_REQUEST['brands'];
$selectedDates  = $_REQUEST['dates'];


// Добавление в корзину
$isAddToBasket = false;
if ($request->isPost()) {
    $siteTemplatePath = SITE_TEMPLATE_PATH;

    $errorsTpl = <<<HTML
<div class="modal__overlay" tabindex="-1" data-micromodal-close="">
    <div class="modal-default">
        <div class="modal-close modal-close_callback" data-micromodal-close="">
            <img src="{$siteTemplatePath}/img/icons/remove.svg" alt="">
        </div>
        <div class="modal-default__content">
            <div class="to-cart-form">
                <div class="to-cart-form__header">Ошибка</div>
                <div class="to-cart-form__wrap">
                    <div class="to-cart-form__content">#ERRORS#</div>
                </div>
            </div>
        </div>
    </div>
</div>
HTML;
    $error = false;
    if (empty($selectedBrands)) {

        die(json_encode([
            'errors' => 'Вы ничего не выбрали',
            'html' => str_replace('#ERRORS#', 'Вы ничего не выбрали', $errorsTpl),
        ]));
        $error = true;
    }
    if ($productProps['SHOW_ARTICLE']['VALUE'] == 'Y' && empty($_REQUEST['article'])) {

        die(json_encode([
            'errors' => 'Введите артикул или штрихкод продукта',
            'html' => str_replace('#ERRORS#', 'Введите артикул или штрихкод продукта', $errorsTpl),
        ]));
        $error = true;

    }
    if ($productId == 98236 && empty($_REQUEST['time'])) {

        die(json_encode([
            'errors' => 'Выберите время проведения',
            'html' => str_replace('#ERRORS#', 'Выберите время проведения', $errorsTpl),
        ]));
        $error = true;

    }
    if ($productId == 672 && empty($_REQUEST['prize_fond'])) {
        die(json_encode([
            'errors' => 'Укажите призовой фонд',
            'html' => str_replace('#ERRORS#', 'Укажите призовой фонд', $errorsTpl),
        ]));
        $error = true;
    }

    $isAddToBasket = !$error;


}


$propsCodes = getIblockProperties(OFFERS_CATALOG);
$propsCodes[] = 'POST_DATE_FROM';
$propsCodes[] = 'POST_DATE_TO';

$propsCodes = array_unique($propsCodes);


$offers = CCatalogSKU::getOffersList(
    $productId,
    0,
    ['ACTIVE' => 'Y'],
    ['ID', 'PRICE_'.BASE_PRICE, 'AVAILABLE',],
    [
        'CODE' => $propsCodes
    ]
);
$offers = $offers[$productId];

$offersProps = [];

$availableDates = [];
$prices         = [];
foreach ($offers as $offer){
    $from = $offer['PROPERTIES']['POST_DATE_FROM']['VALUE'];
    $to   = $offer['PROPERTIES']['POST_DATE_TO']['VALUE'];

    $prices[$offer['ID']] = $offer['PRICE_'.BASE_PRICE];

    $fromDate = DateTime::createFromFormat(getDateFormat(), $from);
    $toDate = DateTime::createFromFormat(getDateFormat(), $to);

    $tmp = [];
    foreach ($offer['PROPERTIES'] as $prop){
        if (
            empty($prop['VALUE'])
            || substr($prop['CODE'], 0, 5) === 'CML2_'
            || $prop['CODE'] === 'POST_DATE_FROM'
            || $prop['CODE'] === 'POST_DATE_TO'
        ){
            continue;
        }

        $tmp[$prop['CODE']] = array_filter($prop, function ($i){
            return strpos($i, 'VALUE') !== false;
        }, ARRAY_FILTER_USE_KEY);

        if (!isset($offersProps[$prop['CODE']])){
            $offersProps[$prop['CODE']] = array_filter($prop, function ($i){
                return strpos($i, 'VALUE') === false;
            }, ARRAY_FILTER_USE_KEY);
        }

        if (!isset($offersProps[$prop['CODE']]['VALUES'][$prop['VALUE']])){
            $offersProps[$prop['CODE']]['VALUES'][$prop['VALUE']] = array_filter($prop, function ($i){
                return strpos($i, 'VALUE') !== false;
            }, ARRAY_FILTER_USE_KEY);
        }

        $offersProps[$prop['CODE']]['VALUES'][$prop['VALUE']]['OFFERS'][] = $offer['ID'];
    }

    $availableDates[$offer['ID']] = [
        'AVAILABLE'=>$offer['AVAILABLE'],
        'FROM' => $from,
        'TO'   => $to,

        'FROM_DATE' => $fromDate,
        'TO_DATE'   => $toDate,

        'PROPS'     => $tmp,
    ];


}

uasort($availableDates, function($a, $b)
{
    return $a['FROM_DATE'] < $b['FROM_DATE'] ? -1 : ($a['FROM_DATE'] == $b['FROM_DATE'] ? 0 : 1);
});


// тип
$types = [
    'site'              => 'site',
    'digital-site'              => 'site',

    'client-days'       => 'client-days',
    'beauty-studio'     => 'client-days',
    'staff-motivation'  => 'client-days',
    'ramadan'           => 'client-days',

    'wedding'           => 'wedding',
    'pro'               => 'pro',
    'brand-days'        => 'client-days',

    'trainings'         => 'trainings',

    'target-ads'        => 'target-ads',
    'inst-castings'     => 'target-ads',
    'inst-live'         => 'target-ads',
    'out-advertising'   => 'target-ads',

    'app'               => 'target-ads',

    'inst-posts'        => 'target-ads', //'inst-posts',

    'birthday-store'    => 'birthday',
];


$type = $product['CODE'] == 'city-format' ? 'city-format' : $types[$product['SECTION_CODE']];

// Магазины
$shops = [];

$needShops = $type === 'client-days' || $type === 'birthday';

if ($needShops){

    $shopsFilter = ['=IBLOCK_ID' => CATALOG_SHOPS, 'ACTIVE' => 'Y'];
    if (!empty($productProps['STORES']['VALUE'])){
        $shopsFilter['=ID'] = $productProps['STORES']['VALUE'];
    }

    $shopsIter = CIBlockElement::GetList(
        ['sort' => 'asc'],
        $shopsFilter,
        false,
        false,
        [
            'ID', 'IBLOCK_ID', 'NAME',
            'IBLOCK_SECTION_ID',
            'PROPERTY_ADDRESS', 'PROPERTY_BIRTHDAY','PROPERTY_SHOW_DISCOUNT',
            'PROPERTY_POST_DATE_FROM', 'PROPERTY_POST_DATE_TO'
        ]
    );
    $shopCities = [];
    while ($shop = $shopsIter->Fetch()){
        if (!empty($shop['IBLOCK_SECTION_ID']) && empty($shopCities[$shop['IBLOCK_SECTION_ID']])){
            $shopCities[$shop['IBLOCK_SECTION_ID']] = CIBlockSection::GetByID($shop['IBLOCK_SECTION_ID'])->Fetch();
        }
        if (!empty($shopCities[$shop['IBLOCK_SECTION_ID']])){
            $shop['NAME'] .= ', ' . $shopCities[$shop['IBLOCK_SECTION_ID']]['NAME'];
        }

        $shops[$shop['ID']] = [
            'ID'        => $shop['ID'],
            'NAME'      => $shop['NAME'],
            'ADDRESS'   => $shop['PROPERTY_ADDRESS_VALUE'],
            'BIRTHDAY'  => $shop['PROPERTY_BIRTHDAY_VALUE'],
            'SHOW_DISCOUNT'  => $shop['PROPERTY_SHOW_DISCOUNT_VALUE'],
            'POST_DATE_FROM'  => $shop['PROPERTY_POST_DATE_FROM_VALUE'],
            'POST_DATE_TO'    => $shop['PROPERTY_POST_DATE_TO_VALUE'],
        ];
    }
}


$periodDays = $productProps['PERIOD_DAYS']['VALUE'] !== null ? (int)$productProps['PERIOD_DAYS']['VALUE'] : 7;
$periodDays = $periodDays > 0 ? $periodDays : false;
$showArticle = $productProps['SHOW_ARTICLE']['VALUE'] == 'Y';
$showTime = $productId == 98236;
$showPrizeFond = $productId == 672;
// Добавление в корзину
if ($isAddToBasket){
    $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), SITE_ID);
    $basketChanged = false;
    $addedItems = [];


    foreach ($selectedBrands as $i => $brand){
        $offerId = null;
        $addProps = [];

        if ($type == 'city-format'){

            if (!empty($_POST['props']) && is_array($_POST['props'])){

                $beginDate = trim(explode('-', $selectedDates[$i])[0]);
                $beginDate = DateTime::createFromFormat(getDateFormat(), $beginDate);

                if ($beginDate > new DateTime) {
                    $offerId = getOfferByDate($beginDate, $availableDates, 0, $_POST['props']);
                }
                if ($offerId > 0){
                    foreach ($_POST['props'] as $code => $value){
                        if (
                            empty($offersProps[$code])
                            || empty($offersProps[$code]['VALUES'][$value])
                        ){
                            continue;
                        }
                        if (empty($tmpOffersIds)){
                            $tmpOffersIds = $offersProps[$code]['VALUES'][$value]['OFFERS'];
                        }

                        $tmpOffersIds = array_intersect($tmpOffersIds, $offersProps[$code]['VALUES'][$value]['OFFERS']);

                        $addProps[] = [
                            'CODE'  => $code,
                            'NAME'  => $offersProps[$code]['NAME'],
                            'VALUE' => $value,
                        ];
                    }
                }
            }

        } elseif ($type === 'site' || $type === 'target-ads') {

            $beginDate = trim(explode('-', $selectedDates[$i])[0]);
            $beginDate = DateTime::createFromFormat(getDateFormat(), $beginDate);

            if ($beginDate > new DateTime) {
                $offerId = getOfferByDate($beginDate, $availableDates, $periodDays);
            }
            if ($type === 'target-ads') {

                $article = is_array($_REQUEST['article']) ? array_shift($_REQUEST['article']) : $_REQUEST['article'];

                if (!empty($article)) {
                    $addProps[] = [
                        'CODE' => 'ARTICLE',
                        'NAME' => 'Артикул, штрихкод',
                        'VALUE' => $article,
                    ];
                }
                $time = is_array($_REQUEST['time']) ? array_shift($_REQUEST['time']) : $_REQUEST['time'];

                if (!empty($time)) {
                    $addProps[] = [
                        'CODE' => 'TIME',
                        'NAME' => 'Время проведения',
                        'VALUE' => $time,
                    ];
                }
                $fond = is_array($_REQUEST['prize_fond']) ? array_shift($_REQUEST['prize_fond']) : $_REQUEST['prize_fond'];

                if (!empty($fond)) {
                    $addProps[] = [
                        'CODE' => 'PRIZE_FOND',
                        'NAME' => 'Призовой фонд',
                        'VALUE' => $fond,
                    ];
                }
                $desc = is_array($_REQUEST['desc']) ? array_shift($_REQUEST['desc']) : $_REQUEST['desc'];
                if (!empty($desc)){
                    $addProps[] = [
                        'CODE'  => 'DESCRIPTION',
                        'NAME'  => 'Описание',
                        'VALUE' => $desc,
                    ];
                }
            }
        } elseif ($type === 'trainings') {

            $offerId = $productId;

            $availableDates[$offerId] = [
                'DATES' => serialize(explode('-', $selectedDates[$i])),
            ];

            $desc = is_array($_REQUEST['desc']) ? array_shift($_REQUEST['desc']) : $_REQUEST['desc'];
            if (!empty($desc)){
                $addProps[] = [
                    'CODE'  => 'DESCRIPTION',
                    'NAME'  => 'Описание',
                    'VALUE' => $desc,
                ];
            }

        } elseif ($type === 'client-days'){

            $beginDate = trim(explode('-', $selectedDates[$i])[0]);
            $beginDate = DateTime::createFromFormat(getDateFormat(), $beginDate);

            if ($beginDate > new DateTime) {
                $offerId = getOfferByDate($beginDate, $availableDates, $periodDays);
            }
            $availableDates[$offerId] = [
                'DATES' => serialize(explode('-', $selectedDates[$i])),
            ];

            if(empty($_REQUEST['shops'][$i])){

                die(json_encode([
                    'errors' => 'Выберите магазин',
                    'html' => str_replace('#ERRORS#', 'Выберите магазин', $errorsTpl),
                ]));
            }

            if((empty($_REQUEST['discounts'][$i]) && $_REQUEST['discounts'][$i] === "") && $shops[$_REQUEST['shops'][$i]]['SHOW_DISCOUNT'] && !empty($productProps['DISCOUNTS']['VALUE'])){

                die(json_encode([
                    'errors' => 'Выберите размер скидки',
                    'html' => str_replace('#ERRORS#', 'Выберите размер скидки', $errorsTpl),
                ]));
            }
            $addProps[] = [
                'NAME'  => 'Магазин',
                'CODE'  => 'SHOP',
                'VALUE' => $_REQUEST['shops'][$i],
            ];

            if (!empty($productProps['DISCOUNTS']['VALUE']) && $shops[$_REQUEST['shops'][$i]]['SHOW_DISCOUNT']){
                $addProps[] = [
                    'NAME'  => 'Размер скидки',
                    'CODE'  => 'DISCOUNT',
                    'VALUE' => $_REQUEST['discounts'][$i],
                ];
            }
        } elseif ($type === 'birthday'){

            $storeId = $_REQUEST['stores'][$i];

            $discount = $_REQUEST['discounts'][$i];

            if (!empty($shops[$storeId]) && in_array($discount, $productProps['DISCOUNTS']['VALUE'])){

                $offerId = $productId;

                if (
                    !empty($shops[$storeId]['POST_DATE_FROM'])
                    && !empty($shops[$storeId]['POST_DATE_TO'])
                ) {
                    $availableDates[$offerId] = [
                        'FROM' => $shops[$storeId]['POST_DATE_FROM'],
                        'TO'   => $shops[$storeId]['POST_DATE_TO'],
                    ];
                } else {
                    $availableDates[$offerId] = [
                        'DATES' => $shops[$storeId]['BIRTHDAY'],
                    ];
                }

                $addProps[] = [
                    'NAME'  => 'Магазин',
                    'CODE'  => 'SHOP',
                    'VALUE' => $storeId,
                ];

                $addProps[] = [
                    'NAME'  => 'Размер скидки',
                    'CODE'  => 'DISCOUNT',
                    'VALUE' => $discount,
                ];
            }
        } elseif ($type === 'wedding') {
            $beginDate = trim(explode('-', $selectedDates[$i])[0]);
            $beginDate = DateTime::createFromFormat(getDateFormat(), $beginDate);

            if ($beginDate > new DateTime) {
                $offerId = getOfferByDate($beginDate, $availableDates, $periodDays);
            }
            $availableDates[$offerId] = [
                'DATES' => serialize(explode('-', $selectedDates[$i])),
            ];

            $actionTypes = [
                'discount' => 'Скидка',
                'gift' => 'Подарок',
            ];

            if(empty($_REQUEST['type'][$i]) || !isset($actionTypes[$_REQUEST['type'][$i]])){
                die(json_encode([
                    'errors' => 'Выберите тип',
                    'html' => str_replace('#ERRORS#', 'Выберите тип', $errorsTpl),
                ]));
            }

            $addProps[] = [
                'NAME'  => 'Акция',
                'CODE'  => 'TYPE',
                'VALUE' => $actionTypes[$_REQUEST['type'][$i]],
            ];


            if ($_REQUEST['type'][$i] == 'gift') {
                if (empty($_REQUEST['gift'][$i])) {
                    die(json_encode([
                        'errors' => 'Укажите подарок',
                        'html' => str_replace('#ERRORS#', 'Укажите подарок', $errorsTpl),
                    ]));
                } else {
                    $addProps[] = [
                        'NAME' => 'Подарок',
                        'CODE' => 'GIFT',
                        'VALUE' => $_REQUEST['gift'][$i],
                    ];
                }
            }

            if ($_REQUEST['type'][$i] == 'discount') {
                if (empty($_REQUEST['discounts'][$i])) {
                    die(json_encode([
                        'errors' => 'Укажите размер скидки',
                        'html' => str_replace('#ERRORS#', 'Укажите размер скидки', $errorsTpl),
                    ]));
                } else {
                    $addProps[] = [
                        'NAME' => 'Скидка',
                        'CODE' => 'DISCOUNT',
                        'VALUE' => $_REQUEST['discounts'][$i],
                    ];
                }
            }

            $desc = is_array($_REQUEST['desc']) ? array_shift($_REQUEST['desc']) : $_REQUEST['desc'];
            if (!empty($desc)){
                $addProps[] = [
                    'CODE'  => 'RULES',
                    'NAME'  => 'Условия выдачи',
                    'VALUE' => $desc,
                ];
            }
        } elseif ($type === 'pro') {
            $beginDate = trim(explode('-', $selectedDates[$i])[0]);
            $beginDate = DateTime::createFromFormat(getDateFormat(), $beginDate);

            if ($beginDate > new DateTime) {
                $offerId = getOfferByDate($beginDate, $availableDates, $periodDays);
            }
            $availableDates[$offerId] = [
                'DATES' => serialize(explode('-', $selectedDates[$i])),
            ];

            $actionTypes = [
                'bonus' => 'Бонус',
                'gift' => 'Подарок с покупкой',
            ];

            if(empty($_REQUEST['type'][$i]) || !isset($actionTypes[$_REQUEST['type'][$i]])){
                die(json_encode([
                    'errors' => 'Выберите тип',
                    'html' => str_replace('#ERRORS#', 'Выберите тип', $errorsTpl),
                ]));
            }

            $addProps[] = [
                'NAME'  => 'Акция',
                'CODE'  => 'TYPE',
                'VALUE' => $actionTypes[$_REQUEST['type'][$i]],
            ];

            if (empty($_REQUEST['visagiste'][$i])) {
                die(json_encode([
                    'errors' => 'Укажите визажиста',
                    'html' => str_replace('#ERRORS#', 'Укажите визажиста', $errorsTpl),
                ]));
            } else {
                $addProps[] = [
                    'NAME' => 'Визажист',
                    'CODE' => 'VISAGISTE',
                    'VALUE' => $_REQUEST['visagiste'][$i],
                ];
            }

            $desc = is_array($_REQUEST['desc']) ? array_shift($_REQUEST['desc']) : $_REQUEST['desc'];
            if (!empty($desc)){
                $addProps[] = [
                    'CODE'  => 'RULES',
                    'NAME'  => 'Условия акции',
                    'VALUE' => $desc,
                ];
            }
        }

        if (empty($offerId)){
            $offerId = $selectedDates[$i];
        }


        if(empty($availableDates[$offerId])){
            die(json_encode([
                'errors' => 'Пожалуйста, выберите дату больше текущей',
                'html' => str_replace('#ERRORS#', 'Пожалуйста, выберите дату больше текущей', $errorsTpl),
            ]));
        }

        if (
            empty($brands[$brand])
            || empty($availableDates[$offerId])
        ){
            continue;
        }

        if (!empty($availableDates[$offerId]['DATES'])){
            $period = $availableDates[$offerId]['DATES'];
        } else {
            $period = "{$availableDates[$offerId]['FROM']} - {$availableDates[$offerId]['TO']}";
        }

        if ($brands[$brand]['UF_DED_REQUIRED']) {
            if (empty($_REQUEST['ded-num'][$i])) {
                die(json_encode([
                    'errors' => 'Укажите номер ДЭД',
                    'html' => str_replace('#ERRORS#', 'Укажите номер ДЭД', $errorsTpl),
                ]));
            } else {
                $addProps[] = [
                    'CODE'  => 'DED',
                    'NAME'  => 'Номер ДЭД',
                    'VALUE' => $_REQUEST['ded-num'][$i],
                ];
            }
        }

        if ($product['CODE'] == 'sms-rassylka') {
            if (empty($_REQUEST['sms-count'][$i])) {
                die(json_encode([
                    'errors' => 'Укажите количество SMS',
                    'html' => str_replace('#ERRORS#', 'Укажите количество SMS', $errorsTpl),
                ]));
            } else {
                $addProps[] = [
                    'CODE'  => 'SMS_COUNT',
                    'NAME'  => 'Количество SMS',
                    'VALUE' => $_REQUEST['sms-count'][$i],
                ];
            }
        }

        $basketProps = array_merge([
            [
                'CODE' => 'BRAND',
                'NAME' => 'Бренд',
                'VALUE' => $brands[$brand]['UF_NAME']
            ],
            [
                'CODE' => 'PERIOD',
                'NAME' => 'Период проведения',
                'VALUE' => $period
            ],
        ], $addProps);
        
        if (array_sum($prices) > 0) {
            if (empty(array_filter($basketProps, function ($v) {return $v['CODE'] === "DESCRIPTION";}))) {
                $desc = is_array($_REQUEST['desc']) ? array_shift($_REQUEST['desc']) : $_REQUEST['desc'];
                if (!empty($desc)){
                    $basketProps[] = [
                        'CODE'  => 'DESCRIPTION',
                        'NAME'  => 'Комментарий',
                        'VALUE' => $desc,
                    ];
                }
            }
        }


        if ($productProps['CHECK_AVAILABLE']['VALUE'] === 'Y' && !empty($period)) {

            $tmp = unserialize($period);

            if (!$tmp && !is_string($tmp)) {
                list($from, $to) = explode('-', $period);
                $from = trim($from);
                $to = trim($to);

                if (!empty($from)) {
                    if (empty($to)) {
                        $to = $from;
                    }
                    $perDates = [
                        'FROM' => $from,
                        'TO' => $to,
                    ];
                }
            } elseif (is_array($tmp)) {
                foreach ($tmp as $d) {
                    $perDates = [
                        'FROM' => $d,
                        'TO' => $d,
                    ];
                }
            }
            $iter = ExtBasketItemTable::getRow([
                'filter' => [
                    [
                        'LOGIC' => 'OR',
                        [
                            '<=DATE_FROM' => $perDates['FROM'],
                            '>=DATE_TO' => $perDates['FROM'],
                        ],
                        [
                            '<=DATE_FROM' => $perDates['TO'],
                            '>=DATE_TO' => $perDates['TO'],
                        ]
                    ],
                    '=STATUS' => 'CONFIRMED',
                    "@BASKET.PRODUCT_ID" => array_column($offers, 'ID')
                ],
                'select' => ['ID','DATE_FROM', 'DATE_TO']
            ]);

            if ($iter !== null) {
                $dat = $iter["DATE_FROM"] == $iter["DATE_TO"] ?
                    'Дата <strong>' . $iter["DATE_FROM"] . '</strong> уже забронирована.' :
                    'Период с <strong>' . $iter["DATE_FROM"] . '</strong> по <strong>' . $iter["DATE_TO"] . '</strong> уже забронирован.';

                die(json_encode([
                    'errors' => 'Выбранный период уже забронирован. Пожалуйста выберите другой период',
                    'html' => str_replace('#ERRORS#', $dat . '</br>Пожалуйста, выберите другую дату проведения.', $errorsTpl),
                ]));
            }


        }

        if($offerId && $availableDates[$offerId]['AVAILABLE']==='N'){
            $dat = 'Период с <strong>' . $availableDates[$offerId]['FROM'] . '</strong> по <strong>' . $availableDates[$offerId]['TO'] . '</strong> уже забронирован.';

            die(json_encode([
                'errors' => 'Выбранный период уже забронирован. Пожалуйста выберите другой период',
                'html' => str_replace('#ERRORS#', $dat . '</br>Пожалуйста, выберите другую дату проведения.', $errorsTpl),
            ]));
        }


        $res = \Bitrix\Catalog\Product\Basket::addProductToBasket(
            $basket,
            [
                'QUANTITY' => 1 * intval($_REQUEST['sms-count'][$i] ?: 1),
                'PRODUCT_ID' => $offerId,
                'PRODUCT_PROVIDER_CLASS' => \Bitrix\Catalog\Product\Basket::getDefaultProviderName(),
                'CURRENCY' => BASE_PRICE,
                'PROPS' => $basketProps,
            ],
            ['SITE_ID' => SITE_ID],
            ['USER_MERGE' => 'N']
        );


        $basketChanged = $basketChanged || $res->isSuccess();
        if ($res->isSuccess()){
            $addedItems[] = $res->getData()['BASKET_ITEM'];
        }

    }
    if ($basketChanged){
        $res = $basket->save();
    } else {
        $res = (new \Bitrix\Sale\Result())->addError(new \Bitrix\Main\Error('Ничего не выбрано'));
    }

    if ($res->isSuccess()){
        ob_start();
        include 'inc/success.php';
        $html = ob_get_contents();
        ob_end_clean();

        die(json_encode([
            'html' => $html,
        ]));

    } else {

        $siteTemplatePath = SITE_TEMPLATE_PATH;

        $errorsTpl = <<<HTML
<div class="modal__overlay" tabindex="-1" data-micromodal-close="">
    <div class="modal-default">
        <div class="modal-close modal-close_callback" data-micromodal-close="">
            <img src="{$siteTemplatePath}/img/icons/remove.svg" alt="">
        </div>
        <div class="modal-default__content">
            <div class="to-cart-form">
                <div class="to-cart-form__header">Ошибка</div>
                <div class="to-cart-form__wrap">
                    <div class="to-cart-form__content">#ERRORS#</div>
                </div>
            </div>
        </div>
    </div>
</div>
HTML;

        die(json_encode([
            'errors' => $res->getErrorMessages(),
            'html' => str_replace('#ERRORS#', implode('<br>', $res->getErrorMessages()), $errorsTpl),
        ]));
    }
}

$data = [];

ob_start();
if ($type === null) {
    include 'inc/def.php';
} else {
    /** @noinspection PhpIncludeInspection */
    include "inc/{$type}.php";
}

$data['html'] = ob_get_contents();
ob_end_clean();

echo json_encode($data);
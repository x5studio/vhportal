<div class="modal__overlay" tabindex="-1" data-micromodal-close="">
    <div class="modal-default">
        <div class="modal-close modal-close_callback" data-micromodal-close="">
            <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/remove.svg" alt="">
        </div>
        <div class="modal-default__content">
            <div class="to-cart-form">
                <div class="to-cart-form__wrap">
                    <div class="to-cart-form__header">
                        <div class="to-cart-form__head">ВЫ ВЫБРАЛИ УСЛУГУ</div>
                        <div class="to-cart-form__type-wrap">
                            <div class="to-cart-form__img-wrap">
                                <img class="to-cart-form__img" src="<?= CFile::GetPath($product['PREVIEW_PICTURE'] ?? $product['DETAIL_PICTURE']);?>"
                                     alt="<?= $product['NAME'];?>">
                            </div>
                            <div class="to-cart-form__type-right">
                                <div class="to-cart-form__type"><?= $product['SECTION_NAME'];?></div>
                                <div class="to-cart-form__title">
                                    <div  class="to-cart-form__title-link"><?= $product['NAME'];?></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="to-cart-form__content">
                        <form name="basket-<?= $productId;?>">
                            <input type="hidden" name="product_id" value="<?= $productId?>" />

                            <div class="js-clone-wrap clone-wrap">
                                <?php

                                $totalPrice = 0;

                                foreach ($selectedBrands as $i => $brand):
                                    if (empty($brands)){
                                        break;
                                    }

                                    $selectedDate = $selectedDates[$i];

                                    $offerId = $selectedDate;

                                    if (!empty($offerId)){
                                        $price = $prices[$offerId] ?? $prices[0];
                                        $totalPrice += $price;
                                    } else {
                                        $price = null;
                                    }

                                    ?>

                                    <div class="to-cart-form__block js-clone-block">
                                        <div class="to-cart-form__block-wrap">
                                            <div class="to-cart-form__row">
                                                <label for="select-brand-<?= $i;?>-<?= $productId;?>" class="input__label-big">БРЕНД</label>
                                                <div class="select-custom">
                                                    <select name="brands[]" class="select-wrap__element js-custom-select" id="select-brand-<?= $i;?>-<?= $productId;?>">
                                                        <option value="0">Выберите бренд</option>
                                                        <?php foreach ($brands as $v):
                                                            if ($v['ID'] == $brand){
                                                                $selected = 'selected="selected"';
                                                            } else {
                                                                $selected = '';
                                                            }
                                                            ?>
                                                            <option value="<?= $v['ID'];?>" <?= $selected;?>><?= $v['UF_NAME'];?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>


                                            <?php if (!empty($dedBrands = array_filter($brands, function ($v) { return boolval($v['UF_DED_REQUIRED']); }))): ?>
                                                <div class="to-cart-form__row" id="basket-ded" style="display: none;">
                                                    <label for="basket-ded-<?= $productId;?>" class="input__label-big">Номер ДЭД </label>
                                                    <input id="basket-ded-<?= $productId;?>" class="input__field" name="ded-num[]" type="text" placeholder="Введите номер ДЭД">
                                                </div>
                                                <script>
                                                    brandsInput = document.querySelector('#select-brand-<?= $i;?>-<?= $productId;?>');
                                                    callback = function (e) {
                                                        document.querySelector('#basket-ded').style.display = <?= json_encode(array_column($dedBrands, 'ID')); ?>.indexOf(this.value) > -1 ? '' : 'none';
                                                    };
                                                    brandsInput.onchange = callback;
                                                    callback.call(brandsInput);
                                                </script>
                                            <?php endif; ?>


                                            <div class="to-cart-form__row">
                                                <label for="select-date-<?= $i;?>-<?= $productId;?>" class="input__label-big">дата размещения<span class="required">*</span></label>

                                                <div class="select-custom">
                                                    <select name="dates[]" class="select-wrap__element js-custom-select">
                                                        <option total="<?= CurrencyFormat($totalPrice, BASE_CURRENCY);?>" price="Выберите бренд и дату" value="0">Выберите дату</option>
                                                        <?php
                                                        $showedDates = [];
                                                        foreach ($availableDates as $id => $date):
                                                            if (in_array($id, $selectedDates)){
                                                                $selected = 'selected="selected"';
                                                            } else {
                                                                $selected = '';
                                                            }

                                                            if ($date['FROM']){
                                                                $period = $date['FROM'] . ' - ' . $date['TO'];
                                                            } else {
                                                                $period = 'Любой';
                                                            }

                                                            $fromDate = DateTime::createFromFormat(getDateFormat(), $date['FROM']);
                                                            if (!empty($fromDate) && $fromDate < new DateTime)
                                                                continue;

                                                            if (in_array($period, $showedDates)){
                                                                continue;
                                                            }
                                                            $showedDates[] = $period;
                                                            ?>
                                                            <option value="<?= $period; ?>" <?=$selected;?> >
                                                                <?= $period; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <?php
                                            if ($product['CODE'] == 'city-format'):
                                                $tmpProps = [
                                                    'CITY_FORMAT_SIDE',
                                                    'CITY_FORMAT_SLIDE_COUNT',
                                                ];
                                                foreach ($offersProps as $code => $prop){
                                                    if (!in_array($code, $tmpProps)){
                                                        continue;
                                                    }
                                                    ?>
                                                    <div class="to-cart-form__row">
                                                        <label for="select-<?= $productId.'-'.$code; ?>" class="input__label-big"><?= $prop['NAME'];?><span class="required">*</span></label>
                                                        <div class="select-custom">
                                                            <select name="props[<?= $code;?>]" class="select-wrap__element js-custom-select" id="select-<?= $productId.'-'.$code; ?>">
                                                                <?php foreach ($prop['VALUES'] as $v): ?>
                                                                <option value="<?= $v['VALUE']; ?>"><?= $v['VALUE'];?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            endif;
                                            ?>

                                            <div class="to-cart-form__row">
                                                <div class="form-note">
                                                    <span class="required">*</span>
                                                    <?php if ($product['CODE'] === 'promokolonna'): ?>
                                                    —  Печать и монтаж не входят в стоимость размещения
                                                    <?php else: ?>
                                                    —  Обратите внимание, редактирование полей может привеcти к изменению цены
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            
                                            <?php if (array_sum($prices) > 0): ?>
                                                <div class="to-cart-form__row">
                                                    <label for="desc-<?= $i;?>" class="input__label-big">Комментарий</label>
                                                    <div class="textarea-wrap">
                                                    <textarea class="textarea__element"
                                                              name="desc[]" id="desc-<?= $i;?>"
                                                              cols="30" rows="5"
                                                    ></textarea>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            
                                            <div class="to-cart-form__border"></div>

                                        </div>
                                    </div>

                                    <?php
                                    if (empty($selectedDates[$i])){
                                        break;
                                    }

                                    unset($brands[$brand]);
                                    unset($availableDates[$offerId]);
                                endforeach;

                                ?>
                            </div>

                            <div class="to-cart-form__row">
                                <label for="total-price-<?= $productId;?>" class="input__label-big">Итоговая ЦЕНА</label>
                                <div class="price price_big" id="total-price-<?= $productId;?>">
                                    <?//= CurrencyFormat($totalPrice, BASE_CURRENCY);?>
                                </div>
                            </div>
                            <div class="to-cart-form__btn">
                                <a href="" class="btn btn_type-1 js-to-cart-success" data-method="post" data-action="<?= $pHelper->getAddToBasketUrl();?>">
                                    <div class="btn__icon-wrap">
                                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/to-cart.svg" alt="" class="btn__icon">
                                    </div>
                                    в корзину
                                </a>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript">
(function(){

    let product_id = '<?= $productId;?>',
        formName = 'basket-' + product_id,
        formSelector = 'form[name="' + formName + '"]';

    $(formSelector).on('change', 'input,select', function(e){
        var el = $(e.currentTarget),
            form = el.closest(formSelector);

        $.get(
            '/local/ajax/price/',
            form.serialize(),
            function(data){
                if (data && data.success && data.result.price_formatted){
                    $('#total-price-' + product_id).html(data.result.price_formatted);
                } else if (!data.success){
                    $('#total-price-' + product_id).html(data.error);
                }
            },
            'json'
        );
    })
})();
</script>
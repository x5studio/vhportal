<div class="modal__overlay" tabindex="-1" data-micromodal-close="">
    <div class="modal-default">
        <div class="modal-close modal-close_callback" data-micromodal-close="">
            <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/remove.svg" alt="">
        </div>
        <div class="modal-default__content">
            <div class="to-cart-form">
                <div class="to-cart-form__wrap">
                    <div class="to-cart-form__header">
                        <div class="to-cart-form__head">ВЫ ВЫБРАЛИ УСЛУГУ</div>
                        <div class="to-cart-form__type-wrap">
                            <div class="to-cart-form__img-wrap">
                                <img class="to-cart-form__img" src="<?= CFile::GetPath($product['PREVIEW_PICTURE'] ?? $product['DETAIL_PICTURE']);?>"
                                     alt="<?= $product['NAME'];?>">
                            </div>
                            <div class="to-cart-form__type-right">
                                <div class="to-cart-form__type"><?= $product['SECTION_NAME'];?></div>
                                <div class="to-cart-form__title">
                                    <div  class="to-cart-form__title-link"><?= $product['NAME'];?></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="to-cart-form__content to-cart-form__content_inst-post">
                        <form name="basket-<?= $productId;?>">
                            <input type="hidden" name="product_id" value="<?= $productId?>" />

                            <div class="js-clone-wrap clone-wrap">
                                <?php

                                $totalPrice = 0;

                                foreach ($selectedBrands as $i => $brand):
                                    if (empty($brands) || $i > 0){
                                        break;
                                    }

                                    $selectedDate = $selectedDates[$i];
                                    if (empty($selectedDate)){
                                        $selectedDate = date(getDateFormat(), (new \Bitrix\Main\Type\Date)->add('P1D')->getTimestamp());
                                    }

                                    $beginDate = trim(explode('-', $selectedDates[$i])[0]);
                                    $beginDate = DateTime::createFromFormat(getDateFormat(), $beginDate ?: $selectedDate);

                                    $offerId = getOfferByDate($beginDate, $availableDates);

                                    if (!empty($offerId)){
                                        $price = $prices[$offerId] ?? $prices[0];
                                        $totalPrice += $price;
                                    } else {
                                        $price = null;
                                    }
                                    ?>
                                    <div class="to-cart-form__block js-clone-block">

                                        <div class="form-inst">
                                            <div class="form-inst__block-row">

                                                <div class="form-inst__block-td">
                                                    <?php if ($periodDays > 1): ?>
                                                    <label for="select-date-<?= $i;?>-<?= $productId;?>" class="input__label-big">
                                                        Дата начала <?= $periodDays?> дневного периода
                                                        <span class="required">*</span>
                                                    </label>
                                                    <?php else: ?>
                                                    <label for="select-date-<?= $i;?>-<?= $productId;?>" class="input__label-big">
                                                        Дата проведения
                                                        <span class="required">*</span>
                                                    </label>
                                                    <?php endif; ?>

                                                    <div class="cart-list__inst-form-label mb_10">дата</div>

                                                    <div class="flex flex_ac">
                                                        <?php if ($periodDays > 1): ?>
                                                            <input id="select-date-<?= $i;?>-<?= $productId;?>" name="dates[]" value="<?= $selectedDate;?>"
                                                                   class="select-wrap__element js-dates-single js-drop-calendar"
                                                                <?php if ($periodDays > 1) echo 'data-day-step="' . $periodDays . '"'; ?>
                                                            />
                                                        <?php else: ?>
                                                            <div class="dates-multiply dates-multiply_single js-dates-single">
                                                                <div class="dates-multiply__list">
                                                                    <?= $selectedDate;?>
                                                                </div>
                                                                <div class="dates-multiply__icon">
                                                                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/date.svg" alt="">
                                                                </div>
                                                                <input type="hidden" id="select-date-<?= $i;?>-<?= $productId;?>" name="dates[]" value="<?= $selectedDate;?>"
                                                                       class="js-dates-single-input" autocomplete="off"
                                                                    <?php if ($periodDays > 0) echo 'data-day-step="' . $periodDays . '"'; ?>
                                                                />
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>

                                                <div class="form-inst__block-td">
                                                    <label for="select-brand-<?= $i;?>-<?= $productId;?>" class="input__label-big mb_30">БРЕНД</label>
                                                    <div class="select-custom">
                                                        <select name="brands[]" class="select-wrap__element js-custom-select" id="select-brand-<?= $i;?>-<?= $productId;?>">
                                                            <?php foreach ($brands as $v):
                                                                if ($v['ID'] == $brand){
                                                                    $selected = 'selected="selected"';
                                                                } else {
                                                                    $selected = '';
                                                                }
                                                                ?>
                                                                <option value="<?= $v['ID'];?>" <?= $selected;?>><?= $v['UF_NAME'];?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <?php if (!empty($dedBrands = array_filter($brands, function ($v) { return boolval($v['UF_DED_REQUIRED']); }))): ?>
                                                <div class="to-cart-form__row" id="basket-ded" style="display: none;">
                                                    <label for="basket-ded-<?= $productId;?>" class="input__label-big">Номер ДЭД </label>
                                                    <input id="basket-ded-<?= $productId;?>" class="input__field" name="ded-num[]" type="text" placeholder="Введите номер ДЭД">
                                                </div>
                                                <script>
                                                    brandsInput = document.querySelector('#select-brand-<?= $i;?>-<?= $productId;?>');
                                                    callback = function (e) {
                                                        document.querySelector('#basket-ded').style.display = <?= json_encode(array_column($dedBrands, 'ID')); ?>.indexOf(this.value) > -1 ? '' : 'none';
                                                    };
                                                    brandsInput.onchange = callback;
                                                    callback.call(brandsInput);
                                                </script>
                                            <?php endif; ?>
    
                                            
                                            <?php if ($product['CODE'] == 'sms-rassylka'): ?>
                                                <div class="to-cart-form__row mt-20" id="sms-count" style="margin-top:20rem;width:200rem">
                                                    <label for="sms-count-<?= $productId;?>" class="input__label-big">Количество смс</label>
                                                    <input id="sms-count-<?= $productId;?>" class="input__field" name="sms-count[]" type="number" placeholder="Введите количество" value="1">
                                                </div>
                                            <?php endif; ?>


                                            <?php
                                            if ($product['CODE'] == 'city-format'):
                                                $tmpProps = [
                                                    'CITY_FORMAT_SIDE',
                                                    'CITY_FORMAT_SLIDE_COUNT',
                                                ];
                                                ?>
                                            <div class="to-cart-form__block-wrap" style="margin-top: 20rem;">
                                                <?php
                                                foreach ($offersProps as $code => $prop){
                                                    if (!in_array($code, $tmpProps)){
                                                        continue;
                                                    }
                                                    ?>
                                                    <div class="to-cart-form__row">
                                                        <label for="select-<?= $productId.'-'.$code; ?>" class="input__label-big mb_30"><?= $prop['NAME'];?></label>
                                                        <div class="select-custom">
                                                            <select name="props[<?= $code;?>]" class="select-wrap__element js-custom-select" id="select-<?= $productId.'-'.$code; ?>">
                                                                <?php foreach ($prop['VALUES'] as $v): ?>
                                                                <option value="<?= $v['VALUE']; ?>"><?= $v['VALUE'];?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php
                                                } ?>
                                                <?php if (array_sum($prices) > 0): ?>
                                                    <div class="to-cart-form__row">
                                                        <label for="desc-<?= $i;?>" class="input__label-big">Комментарий</label>
                                                        <div class="textarea-wrap">
                                                    <textarea class="textarea__element"
                                                              name="desc[]" id="desc-<?= $i;?>"
                                                              cols="30" rows="5"
                                                    ></textarea>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <?php
                                            else:
                                                ?>
                                                <? if ($showArticle) { ?>
                                                <div class="to-cart-form__block-wrap" style="margin-top: 20rem;">
                                                    <div class="to-cart-form__row">
                                                        <label for="select-article-<?= $i; ?>-<?= $productId; ?>"
                                                               class="input__label-big">Продукт</label>
                                                        <input name="article" placeholder="Артикул или штрихкод"
                                                               class="select-wrap__element js-custom-select">
                                                    </div>
                                                </div>
                                            <? } ?>
                                                <? if ($showTime) { ?>
                                                <div class="to-cart-form__block-wrap" style="margin-top: 20rem;">
                                                    <div class="to-cart-form__row">
                                                        <label for="select-time-<?= $i; ?>-<?= $productId; ?>"
                                                               class="input__label-big">Время проведения</label>
                                                        <input type="time" name="time" data-inputmask="'alias': 'datetime', 'inputFormat': 'HH:MM', 'placeholder': '0'" placeholder="00:00">
                                                        <script type="text/javascript">
                                                            $(document).ready(function(){
                                                                $(":input").inputmask();
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                            <? } ?>
                                                <? if ($showPrizeFond) { ?>
                                                <div class="to-cart-form__block-wrap" style="margin-top: 20rem;">
                                                    <div class="to-cart-form__row">
                                                        <label for="select-article-<?= $i; ?>-<?= $productId; ?>"
                                                               class="input__label-big">Призовой фонд</label>
                                                        <input name="prize_fond" placeholder=""
                                                               class="select-wrap__element js-custom-select">
                                                    </div>
                                                </div>
                                            <? } ?>
                                                <div class="form-inst__block form-inst__block_w-a">
                                                    <div class="cart-list__inst-form">
                                                        <div class="cart-list__label">Описание</div>
                                                        <div class="">
                                                            <div class="cart-list__inst-form-list">
                                                                <div class="form-inst__textarea">
                                                                    <div class="textarea-wrap">
																	<textarea name="desc[]"
                                                                              class="textarea__element textarea__element_inst"
                                                                              placeholder="Описание..."
                                                                    ></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>

                                        <div class="to-cart-form__block-wrap">

                                            <div class="to-cart-form__row">
                                                <div class="form-note">
                                                    <span class="required">*</span>  —  Обратите внимание, редактирование полей может привеcти к изменению цены
                                                </div>
                                            </div>
                                            <div class="to-cart-form__border"></div>

                                        </div>
                                    </div>

                                    <?php
                                    if (empty($selectedDates[$i])){
                                        break;
                                    }

                                    unset($brands[$brand]);
                                    unset($availableDates[$offerId]);
                                endforeach;

                                ?>
                            </div>

                            <div class="to-cart-form__row">
                                <label for="total-price-<?= $productId;?>" class="input__label-big"> итоговая ЦЕНА</label>
                                <div class="price price_big" id="total-price-<?= $productId;?>">
                                    <?= CurrencyFormat($totalPrice, BASE_CURRENCY);?>
                                </div>
                            </div>
                            <div class="to-cart-form__btn">
                                <a href="javascript:;" class="btn btn_type-1 js-to-cart-success" data-method="post" data-action="<?= $pHelper->getAddToBasketUrl();?>">
                                    <div class="btn__icon-wrap">
                                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/to-cart.svg" alt="" class="btn__icon">
                                    </div>
                                    в корзину
                                </a>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript">
(function () {
    var product_id = '<?= $productId;?>';
    var date = null;
    var dates = null;
    var quantity = 1;
    
    $('.js-dates-single').each(function(){
        dates = this.value || (this.querySelector('.js-dates-single-input') || {value:1}).value;
        this.addEventListener('pickmeup-change', function(e){
            var el = $(e.target);
            if (Array.isArray(e.detail.formatted_date)){
                date=e.detail.formatted_date[0];
            }else{
                date=e.detail.formatted_date;
            }
            if (el.closest('form').attr('name') === "basket-" + product_id){
                $.get(
                    '/local/ajax/price/',
                    {
                        product_id: product_id,
                        date: date,
                        "dates[]": dates,
                        quantity: quantity
                    },
                    function(data){
                        if (data && data.success && data.result.price_formatted){
                            $('#total-price-' + product_id).html(data.result.price_formatted);
                        }
                    },
                    'json'
                );
            }
        });
    });
    $('#sms-count-'+product_id).each(function(){
        this.addEventListener('change', function(e){
            var el = $(e.target);
            quantity = e.target.value;
            if (el.closest('form').attr('name') === "basket-" + product_id){
                $.get(
                    '/local/ajax/price/',
                    {
                        product_id: product_id,
                        date: date,
                        "dates[]": dates,
                        quantity: quantity
                    },
                    function(data){
                        if (data && data.success && data.result.price_formatted){
                            $('#total-price-' + product_id).html(data.result.price_formatted);
                        }
                    },
                    'json'
                );
            }
        });
    });
    
})();
</script>
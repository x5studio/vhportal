<div class="modal__overlay" tabindex="-1" data-micromodal-close="">
    <div class="modal-default">
        <div class="modal-close modal-close_callback" data-micromodal-close="">
            <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/remove.svg" alt="">
        </div>
        <div class="modal-default__content">
            <div class="to-cart-form">
                <div class="to-cart-form__wrap">
                    <div class="to-cart-form__header">
                        <div class="to-cart-form__type-wrap">
                            <div class="to-cart-form__img-wrap">
                                <img class="to-cart-form__img" src="<?= $product['IMAGE_SRC'];?>"
                                     alt="<?= $product['NAME'];?>">
                            </div>
                            <div class="to-cart-form__type-right">
                                <div class="to-cart-form__type"><?= $product['SECTION_NAME'];?></div>
                                <div class="to-cart-form__title">
                                    <div class="to-cart-form__title-link"><?= $product['NAME'];?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="to-cart-form__content">
                        <div class="cart-info">
                            <div class="cart-info__wrap">
                                <?php
                                /** @var \Bitrix\Sale\BasketItem[] $addedItems */
                                $price = 0;
                                foreach ($addedItems as $item):
                                    $price += $item->getFinalPrice();
                                    $props = $item->getPropertyCollection()->getPropertyValues();

                                    foreach ($props as $code => &$propValue){
                                        $tmp = unserialize($propValue['VALUE']);
                                        if (!empty($tmp)){
                                            $propValue['VALUE'] = is_array($tmp) ? implode("<br>", $tmp) : $tmp;
                                            $propValue['~VALUE'] = $tmp;
                                        }
                                    }

                                    ?>
                                <div class="cart-info__block">
                                    <div class="cart-info__row">
                                        <div class="input__label-big mb_0">Бренд</div>
                                        <div class="cart-info__val"><?= $props['BRAND']['VALUE'];?></div>
                                    </div>
                                    <div class="cart-info__row">
                                        <div class="input__label-big mb_0">период проведения</div>
                                        <div class="cart-info__val"><?= $props['PERIOD']['VALUE'];?></div>
                                    </div>
                                    <?php if ($item->getPrice() > 0): ?>
                                    <div class="cart-info__row">
                                        <div class="input__label-big mb_0">ЦЕНА</div>
                                        <div class="cart-info__val"><?= CurrencyFormat($item->getPrice(), BASE_CURRENCY);?></div>
                                    </div>
                                    <div class="to-cart-form__border to-cart-form__border_info"></div>
                                    <?php endif; ?>
                                </div>
                                <?php endforeach; ?>

                            </div>
                            <?php if ($price > 0): ?>
                            <div class="to-cart-form__row">
                                <label class="input__label-big">итоговая ЦЕНА</label>
                                <div class="price price_big"><?= CurrencyFormat($price, BASE_CURRENCY);?></div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="cart-success-text">
                        <div class="cart-success-text__wrap">
                            <div class="cart-success-text__img-wrap">
                                <img class="cart-success-text__img" src="<?= SITE_TEMPLATE_PATH;?>/img/success-icon.svg" alt="">
                            </div>
                            <div class="cart-success-text__text">успешно добавлен<br>в корзину</div>
                        </div>
                    </div>
                    <div class="to-cart-form__bottom">
                        <div class="to-cart-form__success-btn">
                            <div class="to-cart-form__btn">
                                <a href="" class="btn btn_type-1 js-close-all-modal">вернуться к покупкам</a>
                            </div>
                            <div class="to-cart-form__btn mt_30">
                                <a href="/personal/cart/" class="btn btn_type-2">
                                    <div class="btn__icon-wrap">
                                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/cart.svg" alt="" class="btn__icon">
                                    </div>
                                    перейти в корзину
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

ob_start();

?>
<div class="modal__overlay" tabindex="-1" data-micromodal-close="">
    <div class="modal-default">
        <div class="modal-close modal-close_callback" data-micromodal-close="">
            <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/remove.svg" alt="">
        </div>
        <div class="modal-default__content">
            <div class="to-cart-form">
                <div class="to-cart-form__wrap">
                    <div class="to-cart-form__header">
                        <div class="to-cart-form__head">ВЫ ВЫБРАЛИ УСЛУГУ</div>
                        <div class="to-cart-form__type-wrap">
                            <div class="to-cart-form__img-wrap">
                                <img class="to-cart-form__img" src="<?= CFile::GetPath($product['PREVIEW_PICTURE'] ?? $product['DETAIL_PICTURE']);?>"
                                     alt="<?= $product['NAME'];?>">
                            </div>
                            <div class="to-cart-form__type-right">
                                <div class="to-cart-form__type"><?= $product['SECTION_NAME'];?></div>
                                <div class="to-cart-form__title">
                                    <div  class="to-cart-form__title-link"><?= $product['NAME'];?></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="to-cart-form__content">
                        <form>
                            <input type="hidden" name="product_id" value="<?= $productId?>" />

                            #NEED_MORE_BRAND#

                            <div class="js-clone-wrap clone-wrap">
                                <?php

                                foreach ($selectedBrands as $i => $brand):
                                    if (empty($brands)){
                                        break;
                                    }

                                    $selectedStore = $_REQUEST['stores'][$i];

                                    $offerId = $productId;

                                    ?>
                                    <div class="to-cart-form__block js-clone-block" id="birthday-<?= $i;?>">
                                        <div class="to-cart-form__block-wrap">


                                            <div class="to-cart-form__row">
                                                <label for="select-brand-<?= $i;?>-<?= $productId;?>" class="input__label-big">БРЕНД</label>
                                                <div class="select-custom">
                                                    <style type="text/css">
                                                        ul#select2-select-brand-<?= $i;?>-<?= $productId;?>-results > li:first-child {
                                                            display: none;
                                                        }
                                                    </style>
                                                    <select name="brands[]" class="select-wrap__element js-custom-select" id="select-brand-<?= $i;?>-<?= $productId;?>">
                                                        <option value="0">Выберите бренд</option>
                                                        <?php foreach ($brands as $v):
                                                            if ($v['ID'] == $brand){
                                                                $selected = 'selected="selected"';
                                                            } else {
                                                                $selected = '';
                                                            }
                                                            ?>
                                                            <option value="<?= $v['ID'];?>" <?= $selected;?>><?= $v['UF_NAME'];?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>


                                            <?php if (!empty($dedBrands = array_filter($brands, function ($v) { return boolval($v['UF_DED_REQUIRED']); }))): ?>
                                                <div class="to-cart-form__row" id="basket-ded" style="display: none;">
                                                    <label for="basket-ded-<?= $productId;?>" class="input__label-big">Номер ДЭД </label>
                                                    <input id="basket-ded-<?= $productId;?>" class="input__field" name="ded-num[]" type="text" placeholder="Введите номер ДЭД">
                                                </div>
                                                <script>
                                                    brandsInput = document.querySelector('#select-brand-<?= $i;?>-<?= $productId;?>');
                                                    callback = function (e) {
                                                        document.querySelector('#basket-ded').style.display = <?= json_encode(array_column($dedBrands, 'ID')); ?>.indexOf(this.value) > -1 ? '' : 'none';
                                                    };
                                                    brandsInput.onchange = callback;
                                                    callback.call(brandsInput);
                                                </script>
                                            <?php endif; ?>


                                            <?php if (!empty($productProps['DISCOUNTS']['VALUE'])): ?>
                                                <div class="to-cart-form__row">
                                                    <label for="basket-discount-<?= $productId;?>" class="input__label-big">размер скидки </label>
                                                    <div class="select-custom">
                                                        <style type="text/css">
                                                            ul#select2-basket-discount-<?= $productId;?>-results > li:first-child {
                                                                display: none;
                                                            }
                                                        </style>
                                                        <select name="discounts[]" class="select-wrap__element js-custom-select" id="basket-discount-<?= $productId;?>">
                                                            <option value="0">Выберите размер скидки</option>
                                                            <?php foreach ($productProps['DISCOUNTS']['VALUE'] as $percent): ?>
                                                                <option value="<?= $percent;?>"><?= $percent;?>%</option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php endif; ?>


                                            <div class="to-cart-form__row">
                                                <label for="select-store-<?= $i;?>-<?= $productId;?>" class="input__label-big">Магазин</label>
                                                <div class="select-custom">
                                                    <style type="text/css">
                                                        ul#select2-select-store-<?= $i;?>-<?= $productId;?>-results > li:first-child {
                                                            display: none;
                                                        }
                                                    </style>
                                                    <select name="stores[]" class="select-wrap__element js-custom-select" id="select-store-<?= $i;?>-<?= $productId;?>">
                                                        <option value="0">Выберите магазин</option>
                                                        <?php foreach ($shops as $v):
                                                            if ($v['ID'] == $selectedStore){
                                                                $selectedStore = $v;
                                                                $selected = 'selected="selected"';
                                                            } else {
                                                                $selected = '';
                                                            }

                                                            $birthDay = \Bitrix\Main\Type\Date::createFromText($v['BIRTHDAY']);
                                                            list($day, $month) = explode(':', FormatDate('j:F', $birthDay->getTimestamp()));
                                                            list($dayEnd, $monthEnd) = explode(':', FormatDate('j:F', $birthDay->add("2 day")->getTimestamp()));

                                                            ?>
                                                            <option value="<?= $v['ID'];?>" <?= $selected;?> data-day="<?= $day;?>" data-dayend="<?= $dayEnd;?>" data-month="<?= $month;?>" data-monthend="<?= $monthEnd;?>"> <?= "{$v['NAME']} ";?> <?= $v['ADDRESS'] ? "({$v['ADDRESS']})" : "";?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <?php
                                            if (!empty($selectedStore)){

                                                $birthDay = \Bitrix\Main\Type\Date::createFromText($selectedStore['BIRTHDAY']);

                                                list($dayT, $monthT) = explode(':', FormatDate('j:F', $birthDay->getTimestamp()));
                                                list($dayEndT, $monthEndT) = explode(':', FormatDate('j:F', $birthDay->add("2 day")->getTimestamp()));
                                            } else {
                                                list($dayT, $monthT) = ['', ''];
                                                list($dayEndT, $monthEndT) = ['', ''];
                                            }

                                            ?>
                                            <div class="to-cart-form__row birthday">
                                                <label for="" class="input__label-big"> дата дня рождения </label>
                                                <div class="to-cart-form__row-day">
                                                    <div>
                                                        <div class="to-cart-form__day"><?= $dayT; ?></div>
                                                        <div class="input__label-big"><?= $monthT; ?></div>
                                                    </div>
                                                    <div>
                                                        <div class="to-cart-form__defice">-</div>
                                                    </div>
                                                    <div>
                                                        <div class="to-cart-form__day js-dayend"><?= $dayEndT; ?></div>
                                                        <div class="input__label-big js-monthend"><?= $monthEndT; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <?php if (array_sum($prices) > 0): ?>
                                                <div class="to-cart-form__row">
                                                    <label for="desc-<?= $i;?>" class="input__label-big">Комментарий</label>
                                                    <div class="textarea-wrap">
                                                    <textarea class="textarea__element"
                                                              name="desc[]" id="desc-<?= $i;?>"
                                                              cols="30" rows="5"
                                                    ></textarea>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <div class="to-cart-form__border"></div>

                                        </div>
                                    </div>

                                    <?php
                                    if (empty($_REQUEST['stores'][$i])){
                                        break;
                                    }

                                    unset($brands[$brand]);
                                endforeach;

                                ?>
                            </div>

                            <div class="to-cart-form__btn">
                                <a href="" class="btn btn_type-1 js-to-cart-success" data-method="post" data-action="<?= $pHelper->getAddToBasketUrl();?>">
                                    <div class="btn__icon-wrap">
                                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/to-cart.svg" alt="" class="btn__icon">
                                    </div>
                                    в корзину
                                </a>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript">
    $(document).on('change', '[name="stores[]"]', function(e){

        var el = $(e.currentTarget),
            day = el.find('option:selected').data('day'),
            month = el.find('option:selected').data('month'),
            dayend=el.find('option:selected').data('dayend'),
            monthend=el.find('option:selected').data('monthend');
        el = el.closest('.to-cart-form__block-wrap').find('div.birthday');
        el.find('.to-cart-form__day').text(day);
        el.find('.input__label-big').text(month);
        el.find('.to-cart-form__day.js-dayend').text(dayend);
        el.find('.input__label-big.js-monthend').text(monthend);
    })
</script>
<?php


$content = ob_get_contents();ob_end_clean();

if (count($brands) > 1){
    $templatePath = SITE_TEMPLATE_PATH;
    $needMoreBrandsButtonHtml = <<<HTML
<div class="to-cart-form__add-btn">
    <a data-action="{$pHelper->getAddToBasketUrl()}?add_item" class="btn btn_type-2 btn_type-2-small js-to-cart">
        <div class="btn__icon-wrap">
            <img src="{$templatePath}/img/icons/add.svg" alt="" class="btn__icon">
        </div>
        <span>ДОБАВИТЬ <br>БРЕНД</span>
    </a>
</div>
HTML;
    $content = str_replace('#NEED_MORE_BRAND#', $needMoreBrandsButtonHtml, $content);
} else {
    $content = str_replace('#NEED_MORE_BRAND#', '', $content);
}

echo $content;
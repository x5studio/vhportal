<div class="modal__overlay" tabindex="-1" data-micromodal-close="">
    <div class="modal-default">
        <div class="modal-close modal-close_callback" data-micromodal-close="">
            <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/remove.svg" alt="">
        </div>
        <div class="modal-default__content">
            <div class="to-cart-form">


                <div class="to-cart-form__wrap">
                    <div class="to-cart-form__header">
                        <div class="to-cart-form__head">ВЫ ВЫБРАЛИ УСЛУГУ</div>
                        <div class="to-cart-form__type-wrap">
                            <div class="to-cart-form__img-wrap">
                                <img class="to-cart-form__img" src="<?= CFile::GetPath($product['PREVIEW_PICTURE'] ?? $product['DETAIL_PICTURE']);?>"
                                     alt="<?= $product['NAME'];?>">
                            </div>
                            <div class="to-cart-form__type-right">
                                <div class="to-cart-form__type"><?= $product['SECTION_NAME'];?></div>
                                <div class="to-cart-form__title">
                                    <div  class="to-cart-form__title-link"><?= $product['NAME'];?></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="to-cart-form__content to-cart-form__content_inst-post">
                        <form name="basket-<?= $productId;?>">
                            <input type="hidden" name="product_id" value="<?= $productId?>" />

                            <div class="js-clone-wrap clone-wrap">

                                <?php

                                $totalPrice = 0;

                                foreach ($selectedBrands as $i => $brand):
                                    if ($i > 0){
                                        break;
                                    }

                                    $selectedDate = $selectedDates[$i];
                                    if (empty($selectedDate)){
                                        $selectedDate = date(getDateFormat(), (new \Bitrix\Main\Type\Date)->add('P1D')->getTimestamp());
                                    }


                                    $beginDate = trim(explode('-', $selectedDates[$i])[0]);
                                    $beginDate = DateTime::createFromFormat(getDateFormat(), $beginDate ?: $selectedDate);

                                    $offerId = getOfferByDate($beginDate, $availableDates);

                                    if (!empty($offerId)){
                                        $price = $prices[$offerId] ?? $prices[0];
                                        $totalPrice += $price;
                                    } else {
                                        $price = null;
                                    }



                                    ?>
                                    <div class="to-cart-form__block js-clone-block">


                                        <div class="to-cart-form__block-wrap">


                                            <div class="to-cart-form__row">
                                                <label for="select-brand-<?= $i;?>-<?= $productId;?>" class="input__label-big mb_30">БРЕНД</label>
                                                <div class="select-custom">
                                                    <select name="brands[]" class="select-wrap__element js-custom-select" id="select-brand-<?= $i;?>-<?= $productId;?>">
                                                        <option value="0">Выберите бренд</option>
                                                        <?php foreach ($brands as $v):
                                                            if ($v['ID'] == $brand){
                                                                $selected = 'selected="selected"';
                                                            } else {
                                                                $selected = '';
                                                            }
                                                            ?>
                                                            <option value="<?= $v['ID'];?>" <?= $selected;?>><?= $v['UF_NAME'];?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>


                                            <?php if (!empty($dedBrands = array_filter($brands, function ($v) { return boolval($v['UF_DED_REQUIRED']); }))): ?>
                                                <div class="to-cart-form__row" id="basket-ded" style="display: none;">
                                                    <label for="basket-ded-<?= $productId;?>" class="input__label-big">Номер ДЭД </label>
                                                    <input id="basket-ded-<?= $productId;?>" class="input__field" name="ded-num[]" type="text" placeholder="Введите номер ДЭД">
                                                </div>
                                                <script>
                                                    brandsInput = document.querySelector('#select-brand-<?= $i;?>-<?= $productId;?>');
                                                    callback = function (e) {
                                                        document.querySelector('#basket-ded').style.display = <?= json_encode(array_column($dedBrands, 'ID')); ?>.indexOf(this.value) > -1 ? '' : 'none';
                                                    };
                                                    brandsInput.onchange = callback;
                                                    callback.call(brandsInput);
                                                </script>
                                            <?php endif; ?>


                                            <div class="to-cart-form__row">
                                                <label for="basket-type-<?= $productId;?>" class="input__label-big">Акция</label>
                                                <div class="select-custom">
                                                    <select id="basket-type-<?= $productId;?>" name="type[]" class="select-wrap__element js-custom-select">
                                                        <option value="">Выберите тип</option>
                                                        <option value="discount">Скидка</option>
                                                        <option value="gift">Подарок</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="to-cart-form__row" id="basket-gift" style="display: none;">
                                                <label for="basket-gift-<?= $productId;?>" class="input__label-big">Подарок </label>
                                                <input id="basket-gift-<?= $productId;?>" class="input__field" name="gift" type="text" placeholder="Артикул или штрихкод">
                                            </div>


                                            <?php if (!empty($productProps['DISCOUNTS']['VALUE'])): ?>
                                                <div class="to-cart-form__row" id="basket-discounts" style="display: none;">
                                                    <label for="basket-discount-<?= $productId;?>" class="input__label-big">Размер скидки </label>
                                                    <div class="select-custom">
                                                        <select name="discounts[]" class="select-wrap__element js-custom-select" id="basket-discount-<?= $productId;?>">
                                                            <option value="">Выберите размер скидки</option>
                                                            <?php foreach ($productProps['DISCOUNTS']['VALUE'] as $percent): ?>
                                                                <option value="<?= $percent;?>"><?= $percent;?>%</option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php endif; ?>


                                            <div class="to-cart-form__row">
                                                <label for="select-date-<?= $i;?>-<?= $productId;?>" class="input__label-big"> даты проведения </label>
                                                <div class="dates-multiply dates-multiply_small js-dates-single <?//js-dates-multiply?>" id="select-date-<?= $i;?>-<?= $productId;?>">
                                                    <div class="dates-multiply__list"><?= $selectedDate;?></div>
                                                    <div class="dates-multiply__icon">
                                                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/date.svg" alt="">
                                                    </div>
                                                    <input type="hidden" name="dates[]" class="js-dates-single-input <?//js-dates-multiply-input?>" value="<?= $selectedDate;?>">
                                                </div>
                                            </div>


                                            <div class="to-cart-form__row">
                                                <label for="desc-<?= $i;?>" class="input__label-big">Условия выдачи</label>
                                                <div class="textarea-wrap">
                                                    <textarea class="textarea__element"
                                                              placeholder="Условия выдачи..."
                                                              name="desc[]" id="desc-<?= $i;?>"
                                                              cols="30" rows="5"
                                                    ></textarea>
                                                </div>
                                            </div>


                                            <script>
                                                document.querySelector("#basket-type-<?= $productId;?>").onchange = function (e) {
                                                    document.querySelector('#basket-discounts').style.display = this.value === 'discount' ? '' : 'none';
                                                    document.querySelector('#basket-gift').style.display = this.value === 'gift' ? '' : 'none';
                                                    document.querySelector('#basket-desc').style.display = this.value === 'gift' ? '' : 'none';
                                                };
                                            </script>


                                        </div>

                                    </div>
                                    <?php

                                    if (empty($selectedDates[$i])){
                                        break;
                                    }

                                    unset($brands[$brand]);
                                    unset($availableDates[$offerId]);

                                endforeach;

                                ?>
                            </div>


                            <?php /*
                            <div class="to-cart-form__row">
                                <label for="total-price-<?= $productId;?>" class="input__label-big"> итоговая ЦЕНА</label>
                                <div class="price price_big" id="total-price-<?= $productId;?>">
                                    <?= CurrencyFormat($totalPrice, BASE_CURRENCY);?>
                                </div>
                            </div>
*/?>


                            <div class="to-cart-form__btn">
                                <a href="javascript:;" class="btn btn_type-1 js-to-cart-success" data-method="post" data-action="<?= $pHelper->getAddToBasketUrl();?>">
                                    <div class="btn__icon-wrap">
                                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/to-cart.svg" alt="" class="btn__icon">
                                    </div>
                                    <?= !empty($productProps['BASKET_BTN_TITLE']['VALUE']) ? $productProps['BASKET_BTN_TITLE']['VALUE'] : 'в корзину';?>
                                </a>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#basket-shops-<?= $productId;?>.js-custom-select').on('select2:select', function (e) {
        var id = e.params.data.id;
        console.log(e.params.data);
        if(id==2){
            $('#basket-discounts').hide();
        }else{
            $('#basket-discounts').show();
        }
    });
</script>
<?php

define('NEED_AUTH', false);

include_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

$ctx = \Bitrix\Main\Context::getCurrent();
$rsp = $ctx->getResponse();
$rsp->addHeader('Content-Type', 'application/json');

global $USER;

if (!$ctx->getRequest()->isPost()) {
    $rsp->addHeader('Allow', 'POST')->setContent(json_encode([
        'success' => false,
        'error' => [
            'code' => 'method_not_allowed',
            'message' => 'This request method not allowed. Valid method: POST'
        ],
    ]))->setStatus(405)->send();
    die;
}

if ($_REQUEST['access_token'] !== 'megatestkey') {
    $USER->Logout();
    $rsp->setContent(json_encode([
        'success' => false,
        'error' => [
            'code' => 'Access denied',
            'message' => 'Token not allowed'
        ]
    ]))->setStatus(401)->send();
    die;
} else {
    $USER->Authorize(1);
}

$data = \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getInput();
$json = json_decode($data, true);
if (empty($json) || json_last_error() > 0) {
    $rsp->setContent('{"success": false, "error": "' . (json_last_error_msg() ?: 'Empty data') . '"}')->send();
    die;
}

file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/local/import/import.json', $data);

$execPath = $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/classes/reports/import.php';
$logPath = $_SERVER['DOCUMENT_ROOT'] . '/local/import/error.log';

$command = "php {$execPath} > {$logPath} 2>&1 & echo $!; ";

$pid = exec($command, $out);

$rsp->setContent('{"success": true, "result": ' . $pid . '}')->send();
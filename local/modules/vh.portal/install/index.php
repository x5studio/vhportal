<?php

class VH_Portal extends CModule {

    function __construct(){
        $this->MODULE_ID            = 'vh.portal';
        $this->MODULE_NAME          = 'Портал VisageHall';
        $this->MODULE_VERSION       = '1.0.0';
        $this->MODULE_VERSION_DATE  = '31.03.2020';
        $this->MODULE_DESCRIPTION   = '';
        $this->PARTNER_NAME         = 'cerber';
        $this->PARTNER_URI          = "https://vhportal.ru";
    }

    public function DoInstall(){
        Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/vh_portal_orders_export.php',
            <<<PHP
<?php
\$basePath = "/modules/vh.portal/export/orders.php";
\$prefix = \$_SERVER['DOCUMENT_ROOT'];
\$prefix .= file_exists(\$prefix . '/local' . \$basePath) ? '/local' : '/bitrix';
require(\$prefix . \$basePath);
?>
PHP
        );


    }

    public function DoUninstall(){
        Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

        unlink($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/vh_portal_orders_export.php');
    }
}
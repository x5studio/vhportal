<?php

use Bitrix\Main\Entity\Query\Join,
    Bitrix\Main\Entity\ReferenceField,
    Bitrix\Main\Entity\ExpressionField,
    Bitrix\Sale\Internals\StatusTable,
    Bitrix\Iblock\SectionTable,
    Bitrix\Sale\Internals\BasketPropertyTable;

define('NEED_AUTH', true);
define('ADMIN_SECTION', true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog.php");

$sTableID = 'marketing_plan';

$oSort = new CAdminUiSorting($sTableID, 'ID', 'DESC');
$lAdmin = new CAdminUiList($sTableID, $oSort);

/*
 * Filter
 */
$filterFields = array(
    array(
        "id" => "EXT_ITEM.STATUS",
        "name" => 'Статус',
        "type" => "list",
        "items" => array(
            'PROCESS'   => 'Рассматривается',
            'CONFIRMED' => 'Подтверждено',
            'COMPLETED' => 'Выполнено',
            'CANCELED'  => 'Отменено',
        ),
        "filterable" => "=",
        "default" => true
    ),
    array(
        "id" => "SECTION_ID",
        "name" => 'Категория',
        "type" => "list",
        "items" => array_reduce(
            SectionTable::getList([
                'filter' => ['@IBLOCK_ID' => [CATALOG_MARKETING, CATALOG_PERSONAL]],
                'select' => ['ID', 'NAME', 'DEPTH_LEVEL'],
                'order' => ['LEFT_MARGIN' => 'ASC'],
            ])->fetchAll(),
            function($r, $i){
                $r[$i['ID']] = (str_repeat('. ', --$i['DEPTH_LEVEL'])) . $i['NAME'];
                return $r;
            },
            []
        ),
        "filterable" => "",
        "default" => true,
        "params" => [
            "multiple" => true,
        ],
    ),
    array(
        "id" => "BASKET_ID",
        "name" => 'Бренд',
        "type" => "list",
        "items" => array_reduce(
            BasketPropertyTable::getList([
                'filter' => ['=CODE' => 'BRAND', '!BASKET.ORDER_ID' => null],
                'select' => ['VALUE', new ExpressionField('BASKET_IDS', "CONCAT('[', GROUP_CONCAT(BASKET_ID), ']')")],
                'order' => ['VALUE' => 'ASC'],
                'group' => ['VALUE'],
            ])->fetchAll(),
            function($r, $i){
                $r[$i['BASKET_IDS']] = $i['VALUE'];
                return $r;
            },
            []
        ),
        "filterable" => "",
        "default" => true,
        "params" => [
            "multiple" => true,
        ],
    ),
    array(
        "id" => "DATE_INSERT",
        "name" => 'Дата заявки',
        "type" => "date",
        "filterable" => "",
        "default" => true,
        "params" => [
            "multiple" => true,
        ],
    ),
    array(
        "id" => "EXT_ITEM.DATE_FROM",
        "name" => 'Дата проведения',
        "type" => "date",
        "filterable" => "",
        "default" => true,
        "params" => [
            "multiple" => true,
        ],
    ),
);
$arFilter = [];
$lAdmin->AddFilter($filterFields, $arFilter);

if (!empty($arFilter['SECTION_ID']) && is_array($arFilter['SECTION_ID'])) {
    $iter = SectionTable::getList([
        'select' => ['ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'LEFT_MARGIN', 'RIGHT_MARGIN'],
        'filter' => ['@ID' => $arFilter['SECTION_ID']],
    ]);
    $filters = [];
    while ($s = $iter->fetch()) {
        $filters[] = [
            '>=ELEMENT.IBLOCK_SECTION.LEFT_MARGIN' => $s['LEFT_MARGIN'],
            '<=ELEMENT.IBLOCK_SECTION.RIGHT_MARGIN' => $s['RIGHT_MARGIN'],
            '=ELEMENT.IBLOCK_ID' => $s['IBLOCK_ID'],
        ];
        $filters[] = [
            '>=PARENT.IBLOCK_SECTION.LEFT_MARGIN' => $s['LEFT_MARGIN'],
            '<=PARENT.IBLOCK_SECTION.RIGHT_MARGIN' => $s['RIGHT_MARGIN'],
            '=PARENT.IBLOCK_ID' => $s['IBLOCK_ID'],
        ];
        $filters['@ELEMENT.IBLOCK_SECTION_ID'][] = $s['ID'];
        $filters['@PARENT.IBLOCK_SECTION_ID'][] = $s['ID'];
    }
    $arFilter[] = [
        'LOGIC'=>'OR',
    ] + $filters;
    unset($arFilter['SECTION_ID']);
}

if (!empty($arFilter['BASKET_ID']) && is_array($arFilter['BASKET_ID'])) {
    $filter = [];
    foreach ($arFilter['BASKET_ID'] as $i) {
        array_push($filter, ...json_decode($i));
    }
    $arFilter['BASKET_ID'] = $filter;
}
/*
 * END Filter
 */


/*
 * Headers
 */
$arHeaders = array(
    array(
        "id" => "ID",
        "content" => "Номер заявки",
        "sort" => "ID",
        "default" => false,
    ),
    array(
        "id" => "PERIOD",
        "content" => "Дата проведения",
        "sort" => "EXT_ITEM.DATE_FROM",
        "default" => true,
    ),
    array(
        "id" => "WEEKDAY",
        "content" => "День недели",
        "sort" => "WEEKDAY",
        "default" => true,
    ),
    array(
        "id" => "USER_NAME",
        "content" => "Заказчик",
        "sort" => "USER_NAME",
        "default" => false,
    ),
    array(
        "id" => "NAME",
        "content" => "Название",
        "sort" => "NAME",
        "default" => true,
    ),
    array(
        "id" => "BRAND",
        "content" => "Бренд",
        "sort" => "BRAND",
        "default" => true,
    ),
    array(
        "id" => "DISCOUNT",
        "content" => "Скидка",
        "sort" => "DISCOUNT",
        "default" => true,
    ),
    array(
        "id" => "STORE",
        "content" => "Магазин",
        "sort" => "STORE",
        "default" => true,
    ),
    array(
        "id" => "DESCRIPTION",
        "content" => "Описание",
        "sort" => false,
        "default" => true,
    ),
);
$lAdmin->AddHeaders($arHeaders);
/*
 * END Headers
 */


/*
 * Data
 */
$pagination = $lAdmin->getPageNavigation($sTableID);

$iter = \Bitrix\Sale\OrderTable::getList([

    'select' => [
        'ID',
        'BASKET_ID' => 'BASKET.ID',
        'PRODUCT_ID' => 'BASKET.PRODUCT_ID',
        'NAME' => 'BASKET.NAME',
        'BRAND' => 'PROP_BRAND.VALUE',
        'PERIOD' => 'PROP_PERIOD.VALUE',
        'DISCOUNT' => 'PROP_DISCOUNT.VALUE',
        'DESCRIPTION' => 'PROP_DESCRIPTION.VALUE',

        new ExpressionField('STORE', "CONCAT_WS(' ', CONCAT(UPPER(%s), ':'), %s, %s)", ['SHOP.CODE', 'SHOP.IBLOCK_SECTION.NAME', 'SHOP_ADDRESS.VALUE']),
        new ExpressionField('USER_NAME', "CONCAT_WS(' ', %s, %s, %s, CONCAT('(', %s, ')'))", ['USER.LAST_NAME', 'USER.NAME', 'USER.SECOND_NAME', 'USER.LOGIN']),
    ],

    'filter' => $arFilter,

    'group' => ['BASKET.ID'],

    'order' => [$oSort->getField() => $oSort->getOrder()],

    'runtime' => [
        (new ReferenceField(
            'ELEMENT',
            \Bitrix\Iblock\ElementTable::class,
            ['=this.PRODUCT_ID' => 'ref.ID']
        ))->configureJoinType('LEFT'),

        (new ReferenceField(
            'CML2',
            \Bitrix\Iblock\ElementPropertyTable::class,
            Join::on('this.PRODUCT_ID', 'ref.IBLOCK_ELEMENT_ID')->where('ref.IBLOCK_PROPERTY_ID', '9')
        ))->configureJoinType('LEFT'),

        (new ReferenceField(
            'PARENT',
            \Bitrix\Iblock\ElementTable::class,
            ['=this.CML2.VALUE' => 'ref.ID']
        ))->configureJoinType('LEFT'),

        (new ReferenceField(
            'EXT_ITEM',
            ExtBasketItemTable::class,
            ['=this.BASKET_ID' => 'ref.BASKET_ID']
        ))->configureJoinType('LEFT'),

        (new ReferenceField(
            'PROP_PERIOD',
            BasketPropertyTable::class,
            Join::on('this.BASKET_ID', 'ref.BASKET_ID')->where('ref.CODE', 'PERIOD')
        ))->configureJoinType('LEFT'),

        (new ReferenceField(
            'PROP_BRAND',
            BasketPropertyTable::class,
            Join::on('this.BASKET_ID', 'ref.BASKET_ID')->where('ref.CODE', 'BRAND')
        ))->configureJoinType('LEFT'),

        (new ReferenceField(
            'PROP_DISCOUNT',
            BasketPropertyTable::class,
            Join::on('this.BASKET_ID', 'ref.BASKET_ID')->where('ref.CODE', 'DISCOUNT')
        ))->configureJoinType('LEFT'),

        (new ReferenceField(
            'PROP_SHOP',
            BasketPropertyTable::class,
            Join::on('this.BASKET_ID', 'ref.BASKET_ID')->where('ref.CODE', 'SHOP')
        ))->configureJoinType('LEFT'),

        (new ReferenceField(
            'PROP_DESCRIPTION',
            BasketPropertyTable::class,
            Join::on('this.BASKET_ID', 'ref.BASKET_ID')->where('ref.CODE', 'DESCRIPTION')
        ))->configureJoinType('LEFT'),

        (new ReferenceField(
            'SHOP',
            \Bitrix\Iblock\ElementTable::class,
            ['=this.PROP_SHOP.VALUE' => 'ref.ID']
        ))->configureJoinType('LEFT'),

        (new ReferenceField(
            'SHOP_ADDRESS',
            \Bitrix\Iblock\ElementPropertyTable::class,
            Join::on('this.PROP_SHOP.VALUE', 'ref.IBLOCK_ELEMENT_ID')->where('ref.IBLOCK_PROPERTY_ID', '3')
        ))->configureJoinType('LEFT'),
    ],

    'limit' => $_REQUEST["mode"] != "excel" ? $pagination->getLimit() : null,

    'offset' => $pagination->getOffset() ?: null,

    'count_total' => true
]);

$_REQUEST["mode"] != "excel" && $lAdmin->setNavigation($pagination->setRecordCount($iter->getCount()), 'Навигация');

$iter->addFetchDataModifier(function(&$d){
    foreach($d as &$v)
        $v = unserialize($v)[0] ?: $v;

    if (!empty($d['PERIOD'])) {
        if (count($dates = explode('-', $d['PERIOD'])) == 2) {
            $d['PERIOD'] = FormatDate('d.m', strtotime($dates[0])) . '-' . FormatDate('d.m', strtotime($dates[1]));
        } elseif (count($dates) == 1) {
            $d['PERIOD'] = FormatDate('d F Y', strtotime($dates[0]));
            $d['WEEKDAY'] = FormatDate('l', strtotime($dates[0]));
        }
    }
});

while ($i = $iter->fetch()) {
    $lAdmin->AddRow(false, $i);
}
/*
 * END Data
 */


/*
 * Output
 */
$lAdmin->AddAdminContextMenu([["TEXT" => "Выгрузить", "TITLE" => "Excel", "LINK" => $lAdmin->getCurPageParam('mode=excel'), "GLOBAL_ICON"=>"adm-menu-excel",]], false);
$lAdmin->CheckListMode();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$lAdmin->DisplayFilter($filterFields);
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");

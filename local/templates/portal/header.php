<?php

global $USER, $APPLICATION;

$portalParams = \VH\Portal\Helper::getInstance();

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script><![endif]-->
    <title><?php $APPLICATION->ShowTitle();?></title>

    <?php/*
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    */?>

    <link href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet">
    <link href="<?= SITE_TEMPLATE_PATH;?>/js/lightgallery/css/lightgallery.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet/less" type="text/css" href="<?= SITE_TEMPLATE_PATH;?>/js/datepicker/daterangepicker.css" />

    <link rel="stylesheet/less" type="text/css" href="<?= SITE_TEMPLATE_PATH;?>/css/static.css?h=<?= md5_file($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/css/static.css');?>" />
    <link rel="stylesheet/less" type="text/css" href="<?= SITE_TEMPLATE_PATH;?>/css/style.less?h=<?= md5_file($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/css/style.less');?>" />

    <link href="<?= SITE_TEMPLATE_PATH;?>/js/modal/micromodal.css?h=<?= md5_file($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/js/modal/micromodal.css');?>" rel="stylesheet">
    <script src="<?= SITE_TEMPLATE_PATH;?>/js/less-1.7.5.min.js" type="text/javascript"></script>

    <?php $APPLICATION->ShowHead();?>
</head>

<body>

<?php
$APPLICATION->ShowPanel();
?>

<div class="wrapper">

    <!-- .header-->
<?php if ($USER->IsAuthorized() || NEED_AUTH === false): ?>
    <header class="header">
        <div class="header__center center">
            <div class="logo">
                <a href="/" class="logo__link">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/logo.svg" class="logo__img" alt="">
                </a>
            </div>

            <div class="header__menu">
                <div class="menu">

                    <?php
                    // Каталог
                    $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "catalog",
                        array(
                            "ROOT_MENU_TYPE" => "left",
                            "MAX_LEVEL" => "3",
                            "CHILD_MENU_TYPE" => "left",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(),
                            "COMPONENT_TEMPLATE" => "catalog"
                        ),
                        false
                    );
                    ?>
                </div>
            </div>

            <div class="header__right">
                <div class="header__right-top">
                    <div class="times"><?= $portalParams->getWorkTime(); ?></div>
                    <div class="phone js-modal-callback"><?= $portalParams->getPhone(); ?></div>
                </div>

                <div class="header__user">
                    <?php if ($USER->IsAuthorized()): ?>
                        <div class="header__user-price"><?= CurrencyFormat($portalParams->getUserAccountValue(), 'RUB'); ?></div>
                        <div class="header__user-cart">
                            <a href="/personal/cart/" class="cart-btn">
                                <div class="cart-btn__icon-wrap">
                                    <img class="cart-btn__icon" src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/cart.svg"
                                         alt="">
                                </div>
                                <?php
                                if (($positions = $portalParams->getUserBasketPositions()) > 0):
                                    ?>
                                    <div class="cart-btn__count">+<?= $positions; ?></div>
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="header__user-nav">
                            <a href="" class="header__user-link">отчеты</a>
                        </div>
                    <?php endif;
                    $APPLICATION->IncludeComponent("bitrix:menu","user", [
                        "ROOT_MENU_TYPE" => "user",
                        "POSITION" => "top",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "user",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "Y",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => ""
                    ]);
                    ?>
                </div>
            </div>

        </div>
    </header>
<?php endif; ?>

    <main class="content">

<?php if (!$USER->IsAuthorized() && NEED_AUTH === true): ?>
    <div class="enter">

    <div class="enter__wrap">
    <div class="enter__top">
        <div class="enter__top-right">
            <div class="times"><?= $portalParams->getWorkTime(); ?></div>
            <div class="phone js-modal-callback"><?= $portalParams->getPhone(); ?></div>
        </div>
    </div>
<?php endif; ?>
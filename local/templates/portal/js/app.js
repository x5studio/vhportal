var app = {
    init: function(){
        app.formField.init();
        app.resizeView();
        app.shopSlide();

        app.isMobile = $(window).width() < 600 ? true : false
        $(window).on('resize', function () {
            app.isMobile = $(window).width() < 600 ? true : false
        });


        var inputs = document.querySelectorAll( '.inputfile' );
        Array.prototype.forEach.call( inputs, function( input )
        {
            var label	 = input.nextElementSibling,
                labelVal = label.innerHTML;

            input.addEventListener( 'change', function( e )
            {
                var fileName = '';
                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    label.querySelector( 'span' ).innerHTML = fileName;
                else
                    label.innerHTML = labelVal;
            });

            // Firefox bug fix
            input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
            input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
        });

        setTimeout(() => {
            setImgSize()
        }, 100)

        setTimeout(() => {
            setImgSize()
        }, 500)

        function setImgSize() {
            let notRemArr = [
                'pack__img',
                'goods__img',
                'contest__icon',
                'festival-card__img',
                'shop__img'
            ]
            $('img').each(function(){
                let cond = notRemArr.find(item => $(this).hasClass(item))
                if(!cond){
                    $(this).css('width', this.naturalWidth + 'rem')
                }
            })
        }

        $("body").on('mouseover',".rating__brand-element", function(event) {
            let id = $(this).data('id');

            $('.rating__brand-element[data-id='+id+']').addClass('active')
        });

        $("body").on('mouseleave',".rating__brand-element", function(event) {
            $('.rating__brand-element').removeClass('active')
        });




        $("body").on('click',".quantity-arrow-minus", function(event) {
            var qua = $(this).parent().find('.quantity-num');
            if (qua.val() > 1) {
                qua.val(+qua.val() - 1);
                qua.trigger('change');
            }
        });
        $("body").on('click',".quantity-arrow-plus", function(event) {
            var qua = $(this).parent().find('.quantity-num');
            qua.val(+qua.val() + 1);
            qua.trigger('change');
        });


        $('body').on('click', '.js-not-toggle', function(e){
            e.preventDefault()
            $('.js-drop-not').toggleClass('active')
        })

        $('body').on('click', '.js-change-form', function(e){
            e.preventDefault()

            MicroModal.show("modal-change-form");
        })

        $('body').on('click', '.js-change-form-success', function(e){
            e.preventDefault()

            MicroModal.close("modal-change-form");
            MicroModal.show("modal-change-success");
        })



        $('body').on('click', '.js-pop-default', function(e){
            e.preventDefault()
            console.log(12321321)

            let action = $(this).data('action')
            let modal = $(this).data('pop-id')
            let method = $(this).data('method')

            if(!method){
                method = 'GET'
            }
            $.ajax({
                url: '/data/clone.json',
                method: method,
                dataType: 'json',
                data: $(this).closest('form').serialize()
            }).done(function(data) {
                console.log(data)

                let template = data.html;  //в template хранится верстка полученная с сервера
                console.log(template)

                $('.js-default-wrap').empty().html(template)


                MicroModal.show(modal,{
                    onShow: function(){
                    }
                });

            });
            return false
        })



        $('body').on('click', '.js-slide-prom', function(e){
            e.preventDefault()
            $(this).closest('.promzone__element').toggleClass('active')
            $(this).toggleClass('active')
            if($(this).hasClass('active')){
                $(this).find('span').text('свернуть')
            }else{
                $(this).find('span').text('подробнее')
            }

            return false
        })

        $('body').on('click', '.js-shop-album', function(e) {
            e.preventDefault()
            var idShop = $(this).data('id-shop')
            var album = visageData.shopsGallery.filter(function(item){
                console.log(item.shopId, idShop)
                return item.shopId == idShop
            })


            var photos = album.map(function(item){
                var photo = {
                    "src": item.photo,
                    'thumb': item.photo,
                    'subHtml': "<h4>"+item.title+"</h4><p>"+item.short+"</p>"
                }

                return photo
            })



            var config = {
                dynamic: true,
                dynamicEl: photos
            }

            $(this).lightGallery(config)
        })

       /* MicroModal.init({
            onShow: function(modal){
                console.log(12321321312321)
                $('.js-custom-select').select2()
            }, // [1]
            //onClose: modal => console.info(`${modal.id} is hidden`), // [2]
        });*/

        $('body').on('click', '.js-to-cart', function(e){
            e.preventDefault()

            let name = 'modal-to-cart';

            if($(this).parent().data('type') === 'default'){
                name = 'modal-to-cart2'
            }

            if($(this).parent().hasClass('contest__to-cart')){
                name = 'modal-to-cart3'
            }

            if($(this).parent().data('type') === 'target'){
                name = 'modal-to-cart4'
            }

            if($(this).parent().data('type') === 'pro'){
                name = 'modal-to-cart2'
            }

            //console.log(name)

            if($('#modal-main').length){
                let id = $(this).data('id')
                let action = $(this).data('action')
                let method = $(this).data('method')

                if(!method){
                    method = 'GET'
                }
                $.ajax({
                    url: action,
                    method: method,
                    dataType: 'json',
                    data: $(this).closest('form').serialize()
                }).done(function(data) {
                    console.log(data)

                    let template = data.html;  //в template хранится верстка полученная с сервера
                    console.log(template)

                     $('#modal-main').empty().html(template)


                    MicroModal.show('modal-main',{
                        onShow: function(){
                            app.refreshPlugin(data)
                        }
                    });

                });


            }else{
                MicroModal.show(name,{
                    onShow: function(){
                        app.refreshPlugin()
                    }
                });
            }

        })

        $('body').on('click', '.js-to-cart-success', function(e){
            e.preventDefault()

            let name = 'modal-success-cart';

            if($(this).parent().data('type') === 'inst-post'){
                name = 'modal-success-cart2'
            }

            if($(this).parent().data('type') === 'inst-contest'){
                name = 'modal-success-cart3'
            }

            if($(this).parent().data('type') === 'inst-target'){
                name = 'modal-success-cart4'
            }


            if($('#modal-main').length){
                let id = $(this).data('id')
                let action = $(this).data('action')
                let method = $(this).data('method')

                if(!method){
                    method = 'GET'
                }
                $.ajax({
                    url: action,
                    method: method,
                    dataType: 'json',
                    data: $(this).closest('form').serialize()
                }).done(function(data) {
                    console.log(data)

                    let template = data.html;  //в template хранится верстка полученная с сервера
                    console.log(template)

                    $('#modal-main').empty().html(template)


                    MicroModal.show('modal-main',{
                        onShow: function(){
                            app.refreshPlugin(data)
                        }
                    });

                });


            }else{
                MicroModal.show(name,{
                    onShow: function(){
                        //app.refreshPlugin()
                    }
                });
            }

        })

        $('body').on('click', '.js-drop-cond', function(e){
            e.preventDefault()

            let name = 'modal-condition';



            MicroModal.show(name);

        })

        $('body').on('click', '.js-add-clone-form', function(e){
            e.preventDefault()
            $('select').select2('destroy');


            let id = $(this).data('id')
            let action = $(this).data('action')
            let method = $(this).data('method')

            if(!method){
                method = 'GET'
            }
            $.ajax({
                url: action,
                method: method,
                dataType: 'json',
                data: $(this).closest('form').serialize()
            }).done(function(data) {
                console.log(data.html)
                $( ".js-clone-wrap" ).append(data.html);
                app.refreshPlugin(data)
            });

        })

        $('body').on('click', '.js-drop-dates', function(e){
            e.preventDefault()
            let wrap = $(this).closest('.select-wrap').find('.dates')
            $('.js-dates-wrap').empty()
            wrap.clone().appendTo( ".js-dates-wrap");
            MicroModal.show("modal-dates");
        })

        $('body').on('change', '.dates-list .dates-list__checkbox', function(e){
            e.preventDefault()
            let val = $(this).val();
            $('.js-drop-dates').val(val)
            MicroModal.close("modal-dates");
        })





        $('body').on('click', '.js-close-all-modal', function(e){
            e.preventDefault()
            $('.modal-close').click();
            return false
        })


        /*$('.js-drop-calendar').dateRangePicker({
            singleMonth: true,
            showShortcuts: false,
            showTopbar: false
        });*/



        app.refreshPlugin()


       /* // Использование
        var myEfficientFn = app.debounce(app.resizeView, 200);
        window.addEventListener('resize', myEfficientFn);*/


        function debounce(func, wait, immediate) {
            var timeout;
            return function() {
                var context = this, args = arguments;
                var later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        };

// Использование
        var myEfficientFn = debounce(function() {
             app.resizeView();
        }, 500);
        window.addEventListener('resize', myEfficientFn);

    },
    refreshPlugin: function(data){
        console.log(data)
        if($('.select2').length){
            $('.select2-hidden-accessible').select2('destroy');
        }
        if($('select').select2){
            $('select').select2({dropdownAutoWidth : true});
        }

        $('.js-drop-calendar').each(function(){
            let self = this;

            pickmeup(self, {
                date: $(self).val(),
                position       : 'right',
                hide_on_select : false,
                mode      : 'range',
                calendars : 1,
                format	: 'd.m.Y',
                render : function (date) {
                    if(data && data.date_list){
                        let dt =
                            ("0" + date.getDate()).slice(-2)+ '.' +
                            ("0" + (date.getMonth() + 1)).slice(-2) + '.' +
                            date.getFullYear()
                        let dtNote = data.date_list.find(function(item){
                            //console.log(item.date)
                            //console.log(dt)

                            return item.date === dt
                        })

                        console.log(dt)
                        console.log(dtNote)
                        if (dt === (dtNote && dtNote.date)) {

                            return {class_name : 'date-in-past'};
                        }

                    }
                    return {};
                },
                /*instance_content_template: function (x, y) {
                    console.log(x,y)
                    var year_element                 = document.createElement('div');
                    return year_element
                    //return '<div>wed</div>'
                }*/
            });

            let dayStep = $(self).data('day-step')
            self.addEventListener('pickmeup-change', function (e) {
                let date = [e.detail.formatted_date[1], app.snipets.calcDate(e.detail.formatted_date[1], dayStep - 1)]
                pickmeup(self).set_date(date);
            })
        })

        $('.js-dates-multiply').each(function(){
            pickmeup(this, {
                position       : 'right',
                hide_on_select : false,
                mode      : 'multiple',
                calendars : 1,
                format	: 'd.m.Y',
                render : function (date) {
                    /*console.log(date)
                    if (date < now) {
                        return {disabled : true, class_name : 'date-in-past'};
                    }*/
                    return {};
                }
            });

            this.addEventListener('pickmeup-change', function (e) {
                var dates = e.detail.formatted_date.join(' ')
                var datesForInput = e.detail.formatted_date.join('-')

                $(this).find('.dates-multiply__list').text(dates)
                $(this).find('.js-dates-multiply-input').val(datesForInput)
            })
        })


        $('.js-dates-single').each(function(){
            pickmeup(this, {
                date: $(this).val(),
                position       : 'right',
                hide_on_select : false,
                mode      : 'single',
                calendars : 1,
                format	: 'd.m.Y',
                render : function (date) {
                    /*console.log(date)
                    if (date < now) {
                        return {disabled : true, class_name : 'date-in-past'};
                    }*/
                    return {};
                }
            });

            this.addEventListener('pickmeup-change', function (e) {
                var date = e.detail.formatted_date

                console.log(e.detail.formatted_date)

                $(this).find('.dates-multiply__list').text(date)
                $(this).find('.js-dates-single-input').val(date)
            })
        })

    },

    resizeView: function(){
        var width = $(window).width();
        var widthContent=1920;
        var fontSize = width/widthContent;
        /*if(width < 1400 && width > 1100){
            fontSize = fontSize * 1.2;
        }
        if(width < 1100 && width > 769){
            fontSize = fontSize * 1.4;
        }
        if(width < 769){
            fontSize = fontSize * 1.8;
        }*/
        $("html").css("fontSize",fontSize);
    },

    formField: {
        init: function(){
            var self = this;

            self.refresh();
            $('body').on('focus', '.field__input', function(){
                var wrap = $(this).closest('.field');
                wrap.find('.field__label').addClass('active')
                $(this).addClass('active')
            })
            $('body').on('blur', '.field__input', function(){
                var wrap = $(this).closest('.field');
                if($(this).val().length == 0){
                    $(this).removeClass('active')
                    wrap.find('.field__label').removeClass('active')
                }
            })
        },

        refresh: function(){
            $('.field__input').each(function(){
                if($(this).val()){
                    var wrap = $(this).closest('.field');
                    wrap.find('.field__label').addClass('active')
                    $(this).addClass('active')
                }
            })
        }
    },

    windowLoad: function(){
        $('html').addClass('loaded');
    },

    shopSlide: function(){


        $(".shop-open-slide__small-img-element").eq(0).addClass('active')
        $("body").on('click',".shop-open-slide__small-img-element", function(event) {
            $(".shop-open-slide__small-img-element").removeClass('active')
            $(this).addClass('active')
            var img = $(this).find('img');
            $('.shop-open-slide__big-img').html($(this).html()).find('img, video').attr('src', img.data('image')).attr('style', '').attr('controls', '');
        });

    },
    snipets: {
        calcDate: function get(data, day)
        {
            data = data.split('.');
            data = new Date(data[2], data[1] - 1, +data[0]);
            data.setDate(data.getDate() + day);
            data = [data.getDate(),data.getMonth()+1,data.getFullYear()];
            data = data.join('.').replace(/(^|\/)(\d)(?=\/)/g,"$10$2");
            return data
        },
        numberFormat: function(number, decimals, dec_point, thousands_sep) {
            if(!decimals){
                decimals = 0;
            }
            if(!dec_point){
                dec_point = '';
            }
            if(!thousands_sep){
                thousands_sep = ' ';
            }
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + (Math.round(n * k) / k)
                        .toFixed(prec);
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
                .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
                .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
                    .join('0');
            }
            return s.join(dec);
        },
        parseHref: function() {
            var path = location.search;

            var query = decodeURI(path.substr(1)),
                keys = {};

            query.split('&').forEach(function(item) {
                item = item.split('=');
                keys[item[0]] = item[1];
            });

            return keys
        }


    },
 }

$(function(){
    console.log("app", app.init);
    app.init();
})
$(document).ready(function(){app.windowLoad()});


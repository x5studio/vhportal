<div class="office__content office__content_cart">
    <div  class="office-head__link office-head__link_border mb_0">
        <div class="office-head__icon-wrap">
            <img class="office-head__icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/user-menu-2-dark.svg" alt="">
        </div>

        <div class="office-head__hint">
            корзина
        </div>

        <?php /*
            <div class="office-head__btn">
                <a href="" class="btn btn_type-1">
                    <div class="btn__icon-wrap">
                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/download-light.svg" alt="" class="btn__icon">
                    </div>
                    скачать excel
                </a>
            </div>
*/?>
    </div>



    <div class="cart-list">


        <?php foreach ($arResult['GRID']['ROWS'] as $item): ?>
            <div class="cart-list__element cart-list__element_type-1">
                <div class="cart-list__wrap">
                    <div class="cart-list__img-wrap">
                        <img class="cart-list__img" src="/img/prom-luxe.svg" alt="">
                    </div>

                    <div class="cart-list__descrip">
                        <div class="cart-list__type"><?= $arResult['sections'][$item['PRODUCT_ID']];?></div>
                        <div class="cart-list__title"><?= $item['NAME'];?></div>
                    </div>
                    <div class="cart-list__center">
                        <div class="cart-list__form">

                            <?php
                            foreach ($item['PROPS'] as $prop):
                                $tmp = unserialize($prop['~VALUE']);
                                $type = 'text';
                                if (!empty($tmp)){
                                    $prop['VALUE'] = is_array($tmp) ? implode("<br>", $tmp) : $tmp;
                                    $rows = is_array($tmp) ? count($tmp) : 1;
                                    $cols = is_array($tmp) ? strlen(max($tmp)) + 3 : 10;
                                    $prop['~VALUE'] = $tmp;
                                    $type = 'area';
                                }

                                if ($prop['CODE'] === 'SHOP'){
                                    $tmp = CIBlockElement::GetList(
                                        ['sort' => 'asc'],
                                        ['=IBLOCK_ID' => CATALOG_SHOPS, 'ACTIVE' => 'Y', '=ID' => $prop['VALUE']],
                                        false,
                                        false,
                                        ['ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_ADDRESS']
                                    )->Fetch();
                                    if ($tmp){
                                        $prop['VALUE'] = "{$tmp['NAME']}, {$tmp['PROPERTY_ADDRESS_VALUE']}";
                                        $item['PROPS_ALL']['SHOP']['VALUE'] = $prop['VALUE'];
                                    }
                                } elseif ($prop['CODE'] === 'DISCOUNT'){
                                    $prop['VALUE'] .= '%';
                                }
                                ?>
                                <div class="cart-list__form-element <?php if ($type != 'area') echo 'cart-list__form-element_brand'; ?>">
                                    <div class="cart-list__label">
                                        <?= $prop['NAME'];//<span class="required">*</span>?>
                                    </div>

                                    <?php/*
                                <div class="cart-list__select">
                                    <div class="select-custom">
                                        <select name="" class="select-wrap__element" disabled="disabled">
                                            <option selected="selected"><?= $prop['VALUE'];?></option>
                                        </select>
                                    </div>
                                </div>
                                */?>

                                    <?php if (true): ?>
                                        <div style="border-top: 1px solid #d7ccc8; padding-top: 4rem; margin-top: -10rem"><?= $prop['VALUE'];?></div>
                                    <?php elseif ($type == 'area'): ?>
                                        <textarea rows="<?= $rows;?>" cols="<?= $cols;?>" disabled="disabled"><?= $prop['VALUE'];?></textarea>
                                    <?php else: ?>
                                        <input class="cart-list__form-element_brand" type="text" value="<?= $prop['VALUE'];?>" disabled="disabled" />
                                    <?php endif; ?>

                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>


                    <div class="cart-list__right">
                        <?php if ($item['PRICE'] > 0): ?>
                            <div class="cart-list__price">
                                <div class="cart-list__label">ЦЕНА </div>
                                <div class="cart-list__val"><?= $item['SUM_FULL_PRICE_FORMATED'];?></div>
                            </div>
                        <?php endif; ?>

                        <div class="cart-list__nav">
                            <div class="cart-list__remove">
                                <form method="post" action="<?= $GLOBALS['sDocPath'];?>" name="delete_<?= $item['ID'];?>">
                                    <input type="hidden" name="DELETE_<?= $item['ID'];?>" value="Y" />

                                    <input type="hidden" name="id" value="<?= $item['ID'];?>" />
                                    <input type="hidden" name="<?= $arParams['ACTION_VARIABLE'];?>" value="delete" />

                                    <input type="submit" style="display: none;" />

                                    <a href="javascript: document.delete_<?= $item['ID'];?>.submit();" class="cart-list__remove-btn">
                                        <div class="cart-list__remove-icon-wrap">
                                            <img class="cart-list__remove-icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/remove.svg" alt="">
                                        </div>
                                    </a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?if(!empty($item['CROSSING']) && $USER->IsAdmin()) {?>
                    <div class="cart-list__wrap _crossing">
                        <div class="cart-list__crossing-store-and-description">
                            <div class="cart-list__crossing-store"><?=$item['PROPS_ALL']['SHOP']['VALUE']?></div>
                            <span class="cart-list__crossing-description">Клиентские дни  в выбранные даты</span>
                        </div>
                        <div class="cart-list__center">
                            <div class="cart-list__crossing-date"><?=unserialize($item['PROPS_ALL']['PERIOD']['VALUE'])[0]?></div>
                            <ul class="cart-list__crossing-brands">
                                <? foreach ($item['CROSSING'] as $brand) {?>
                                    <li class="cart-list__crossing-brand"><?=$brand?></li>
                                <?}?>
                            </ul>
                        </div>
                    </div>
                <?}?>
            </div>
        <?php endforeach; ?>


    </div>


    <div class="cart-itog">
        <div class="cart-itog__wrap">
            <div class="cart-itog__content" style="text-align: center">
                <div class="cart-itog__label">итоговая цена</div>
                <div class="cart-itog__price"><?= $arResult['allSum_FORMATED'];?></div>

                <?php if (count($arResult['GRID']['ROWS']) > 0): ?>
                <form class="cart-itog__btn" action="/personal/cart/make/" method="post">
                    <div class="input__row t_left enter__form-center">
                        <div class="checkbox__row">
                            <div class="checkbox-type checkbox-type_type-3">
                                <input type="hidden" name="confirm" value="N">
                                <input style="margin-right: 2rem;" type="checkbox" id="order-confirm" name="confirm" value="Y">
                                <label for="order-confirm" class="field-filter__label-type4" style="font-size: 12rem;line-height: 16rem;">
                                    Я ознакомлен с <a target="_blank" href="https://docs.google.com/document/d/11BOcoeMOA4p8cMNIhdbvuaCHmOJzI6l2oGHR8M_mvGI/edit?usp=sharing" style="text-decoration: underline">правилами оформления</a> в VisageHall
                                </label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn_type-1">Забронировать</button>
                </form>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
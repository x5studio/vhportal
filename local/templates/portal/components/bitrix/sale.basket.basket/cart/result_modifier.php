<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Type\Date;
/**
 * @var $arResult ;
 */

$products = array_map(function($i){
    return $i['PRODUCT_ID'];
}, $arResult['GRID']['ROWS']);

$arResult['sections'] = [];
if (!empty($products)) {
    $iter = \Bitrix\Iblock\ElementTable::getList(
        [
            'select' => ['ID', 'SECTION_NAME' => 'IBLOCK_SECTION.NAME'],
            'filter' => ['=ID' => $products]
        ]
    );
    while ($s = $iter->fetch()) {
        $arResult['sections'][$s['ID']] = $s['SECTION_NAME'];
    }
}

$brands = [];
$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
    [
        'filter' => ['TABLE_NAME' => 'vh_brand_reference'],
        'cache' => ['ttl' => 3600]
    ]
)->fetch();
if (isset($hlblock['ID'])) {
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    $res = $entity_data_class::getList();
    while ($item = $res->fetch()) {
        $brands[$item['UF_NAME']] = $item['UF_GROUP'];
        unset($item);
    }
    unset($res);
}
unset($hlblock);


$cartProducts = [];

foreach ($arResult['GRID']['ROWS'] as &$item) {
    $item['PROPS_ALL']['BRAND']['GROUP'] = $brands[$item['PROPS_ALL']['BRAND']['VALUE']];
    $cartProducts[$item['PRODUCT_ID']][$item['ID']] = &$item;
    unset($item);
}

if(count($cartProducts)) {

    $rsElements = CIBlockElement::GetList(
        [],
        ['IBLOCK_ID' => CATALOG_MARKETING, 'ID' => array_keys($cartProducts)],
        false,
        false,
        ['IBLOCK_ID', 'ID', 'NAME', 'PROPERTY_SHOW_CROSSING']
    );

    while($arProduct = $rsElements -> GetNext()) {
        if($arProduct['PROPERTY_SHOW_CROSSING_VALUE'] === 'Да') {

            $crossing = [];
            $cartItems = $cartProducts[$arProduct['ID']];
            foreach ($cartItems as $cartItem) {
                $strDate = unserialize($cartItem['PROPS_ALL']['PERIOD']['VALUE'])[0];
                $date = new Date($strDate);

                $iter = ExtBasketItemTable::getList(
                    [
                        'filter' => [
                            '=BASKET_PROP.CODE' => 'BRAND',
                            '=STATUS' => ExtBasketItemTable::STATUS_CONFIRMED,
                            '=DATE_FROM' => $date,
                            '=NAME' => $arProduct['NAME'],
                        ],
                        'select' => [
                            '*',
                            'BRAND' => 'BASKET_PROP.VALUE',
                            'NAME' => 'BASKET.NAME',
                            'EMAIL' => 'ORDER.USER.EMAIL',
                            'USER_ID' => 'ORDER.USER.ID',
                            'SITE_ID' => 'ORDER.LID',
                            'SHOP_ID' => 'SHOP.VALUE',
                        ],
                        'runtime' => [
                            new Bitrix\Main\ORM\Fields\Relations\Reference(
                                'SHOP',
                                Bitrix\Sale\Internals\BasketPropertyTable::class,
                                array(
                                    '=this.BASKET_ID' => 'ref.BASKET_ID',
                                    '=ref.CODE' => new Bitrix\Main\DB\SqlExpression("'SHOP'"),
                                )
                            ),
                        ]
                    ]
                );
                while ($item = $iter->fetch()){
                    if($cartProducts[$arProduct['ID']][$cartItem['ID']]['PROPS_ALL']['SHOP']['VALUE'] != $item['SHOP_ID']) continue;
                    if(
                        empty($cartProducts[$arProduct['ID']][$cartItem['ID']]['PROPS_ALL']['BRAND']['GROUP']) ||
                        $cartProducts[$arProduct['ID']][$cartItem['ID']]['PROPS_ALL']['BRAND']['GROUP'] == $brands[$item['BRAND']]
                    ) {
                        $cartProducts[$arProduct['ID']][$cartItem['ID']]['CROSSING'][] = $item['BRAND'];
                    }
                }
            }
        }
    }
}



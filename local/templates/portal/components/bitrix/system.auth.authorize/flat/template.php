<?php

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */

$login = !empty($arResult["LAST_LOGIN"]) ? $arResult["LAST_LOGIN"] : $_POST['USER_LOGIN'];

?>

<form class="enter__content" name="form_auth" method="post" target="_top" action="/">

    <input type="hidden" name="AUTH_FORM" value="Y" />
    <input type="hidden" name="TYPE" value="AUTH" />

    <?php if (strlen($arResult["BACKURL"]) > 0):?>
        <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
    <?php endif; ?>

    <?php foreach ($arResult["POST"] as $key => $value):?>
        <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
    <?php endforeach; ?>


    <div class="enter__logo">
        <div class="enter__logo-wrap">
            <img src="<?= SITE_TEMPLATE_PATH;?>/img/logo-mid.svg" class="enter__logo-img" alt="">
        </div>
    </div>


    <div class="enter__hint">
        добро пожаловать
    </div>
    <div class="enter__form">
        <div class="enter__form-center">


            <div class="input__row">
                <label for="USER_LOGIN" class="input__label t_center"><?=GetMessage("AUTH_LOGIN")?></label>
                <div class="input__wrap">
                    <input id="USER_LOGIN" type="text" class="input__field input__field_type-1" name="USER_LOGIN" maxlength="255" value="<?= $login;?>" />
                </div>
            </div>

            <div class="input__row">
                <label for="USER_PASSWORD" class="input__label t_center"><?=GetMessage("AUTH_PASSWORD")?></label>
                <div class="input__wrap">


                    <?php if($arResult["SECURE_AUTH"]): ?>
                        <div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none">
                            <div class="bx-authform-psw-protected-desc">
                                <span></span>
                                <?echo GetMessage("AUTH_SECURE_NOTE")?>
                            </div>
                        </div>

                        <script type="text/javascript">
                            document.getElementById('bx_auth_secure').style.display = '';
                        </script>
                    <?php endif; ?>

                    <input type="password" class="input__field input__field_type-1" id="USER_PASSWORD" name="USER_PASSWORD" maxlength="255" autocomplete="off" />
                </div>
            </div>


            <?if($arResult["CAPTCHA_CODE"]):?>
                <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />

                <div class="input__row">
                    <div class="bx-captcha">
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                    </div>
                    <label for="f_captcha" class="input__label t_center"><?echo GetMessage("AUTH_CAPTCHA_PROMT")?></label>
                    <div class="input__wrap">
                        <input id="f_captcha" class="input__field input__field_type-1" type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
                    </div>
                </div>
            <?endif;?>


            <?if ($arResult["STORE_PASSWORD"] == "Y"):?>
                <div class="input__row t_center mb_50">
                    <div class="checkbox__row">
                        <div class="checkbox-type checkbox-type_type-3">
                            <input type="checkbox" id="STORE_PASSWORD" name="USER_REMEMBER" value="Y">
                            <label for="STORE_PASSWORD" class="field-filter__label-type4">
                                <?=GetMessage("AUTH_REMEMBER_ME")?>
                            </label>
                        </div>
                    </div>
                </div>
            <?endif?>


        </div>

        <?php

        $messages = [];

        if(!empty($arParams["~AUTH_RESULT"]))
            $messages[] = $arParams["~AUTH_RESULT"]["MESSAGE"];
        if($arResult['ERROR_MESSAGE'] <> '')
            $messages[] = $arResult['ERROR_MESSAGE'];

        foreach ($messages as $msg):
            $text = str_replace(array("<br>", "<br />"), "\n", $msg);
            ?>
            <div style="color:red;margin-bottom: 15rem;margin-top: -45rem;"><?=nl2br(htmlspecialcharsbx($text))?></div>
        <?php endforeach; ?>

        <div class="input__row enter__btn">
            <button type="submit" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" class="btn btn_type-1"><?=GetMessage("AUTH_AUTHORIZE")?></button>
        </div>

        <?php
        if ($arParams["NOT_SHOW_LINKS"] != "Y"): ?>
            <div class="enter__remember-pass">
                <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow" class="enter__remember-pass-link"><?=GetMessage("AUTH_FORGOT_PASSWORD")?></a>
            </div>
        <?php endif; ?>

    </div>
</form>

<script type="text/javascript">
    <?if (strlen($login)>0):?>
    try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
    <?else:?>
    try{document.form_auth.USER_LOGIN.focus();}catch(e){}
    <?endif?>
</script>
<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

$pages = &$arResult['PAGINATION'];

if (1 >= $pages['COUNT']) return;

$pageParam = $pages['PAGE_PARAM'];

$uri = (new \Bitrix\Main\Web\Uri($uri))->addParams([$pageParam => $pages['CURRENT'] + 1]);

if ($pages['CURRENT'] >= $pages['COUNT']) return;
?>
<div class="more-btn">
    <div class="more-btn__wrap">
        <button class="btn btn_type-1" data-href="<?= $uri;?>">
            показать еще
        </button>
    </div>
</div>
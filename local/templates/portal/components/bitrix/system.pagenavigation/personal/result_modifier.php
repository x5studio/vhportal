<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


$arResult['PAGINATION'] = [
    'COUNT'         => $arResult['NavPageCount'],
    'PAGE_PARAM'    => 'PAGEN_' . $arResult['NavNum'],
    'CURRENT'       => $arResult['NavPageNomer'],
    'URI'           => $arResult['sUrlPathParams'],
];
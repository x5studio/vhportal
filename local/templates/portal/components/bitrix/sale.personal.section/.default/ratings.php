<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>
<style>
    .rating {
        width: 100%;
        display: grid;
        grid-template-columns: 1fr 1fr 1fr 1fr;
        grid-gap: 20rem;
    }

    .rating__item {
        min-height: 300rem;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        align-items: flex-start;
        position: relative;
        padding: 20rem;
        background: linear-gradient(96.76deg, #FEF7EE 0%, #FFFBDC 100%);
    }

    .rating__type {
        font-weight: bold;
        font-size: 20rem;
        line-height: 26rem;
        letter-spacing: 0.1em;
        text-transform: uppercase;
        color: #000000;
    }

    .rating__date {
        font-weight: 500;
        font-size: 14rem;
        line-height: 26rem;
        letter-spacing: 0.1em;
        text-transform: uppercase;
        color: #000000;
    }

    .rating__show {
        margin-right: 20rem;
    }
</style>
<div class="rating">
    <?php
    global $USER;
    if (true) {

        $pHelper = \VH\Portal\Helper::getInstance();
        $brands = $pHelper->getUserBrands();

        $rights = array_reduce($brands, function ($rights, $brand) {
            if ($brand['UF_GROUP'] > 0)
                $rights[$brand['UF_GROUP']] = true;
            return $rights;
        }, []);

        $months = [
            [1 => "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            [1 => "Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"],
        ];

        $hl = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity('RatingFiles');
        $ratingTable = $hl->getDataClass();

        $iter = $ratingTable::getList([
            'select' => [
                'ID',
                'UF_MIDDLE_FILE',
                'UF_LUXE_FILE',
                'UF_DATE',
                'PERIOD' => 'UF_PERIOD_REF.XML_ID',
            ],
            'order' => ['UF_DATE' => 'DESC'],
        ]);

        $ratings = [];
        while ($i = $iter->fetch()) {

            $date = '';
            switch ($i['PERIOD']) {
                case 'D':
                    $date = $i['UF_DATE']->format('j') . " " .$months[1][$i['UF_DATE']->format('n')] . " " . $i['UF_DATE']->format('Y');
                    break;
                case 'Y':
                    $date = $i['UF_DATE']->format('Y') . ' год';
                    break;
                case 'M':
                default:
                    $date = $months[0][$i['UF_DATE']->format('n')] . " " . $i['UF_DATE']->format('Y');
                    break;
            }

            $files = [
                'MIDDLE' => $i['UF_MIDDLE_FILE'],
                'LUXE' => $i['UF_LUXE_FILE'],
            ];

            if (count(array_filter($files)) > 1) {
                if ($files['MIDDLE'] > 0 && $rights[6]) {
                    $ratings[] = [
                        'TYPE' => "MIDDLE",
                        'DATE' => $date,
                        'FILE_URL' => \CFile::GetPath($files['MIDDLE']),
                    ];
                }

                if ($files['LUXE'] > 0 && $rights[7]) {
                    $ratings[] = [
                        'TYPE' => "LUXE",
                        'DATE' => $date,
                        'FILE_URL' => \CFile::GetPath($files['LUXE']),
                    ];
                }
            } else {
                $ratings[] = [
                    'TYPE' => "Рейтинг",
                    'DATE' => $date,
                    'FILE_URL' => \CFile::GetPath($files['MIDDLE'] ?: $files['LUXE']),
                ];
            }
        }

        foreach ($ratings as $rating):
            ?>
            <div class="rating__item" id="rating_<?= $i['ID']; ?>">
                <div class="rating__item-body">
                    <h3 class="rating__type"><?= $rating['TYPE']; ?></h3>
                    <p class="rating__date"><?= $rating['DATE']; ?></p>
                </div>
                <div class="rating__item-nav">
                    <a target="_blank" href="https://view.officeapps.live.com/op/view.aspx?src=<?= $_SERVER['SERVER_NAME'] . $rating['FILE_URL']; ?>" class="rating__show btn btn_type-4">
                        <div class="btn__icon-wrap">
                            <img src="/local/templates/portal/img/icons/view-dark.svg" alt="" class="btn__icon"
                                 style="width: 15rem;">
                        </div>
                        Смотреть</a>
                    <a target="_blank" href="<?= $rating['FILE_URL']; ?>" class="rating__download btn btn_type-4" download>
                        <div class="btn__icon-wrap">
                            <img src="/local/templates/portal/img/icons/download.svg" alt="" class="btn__icon"
                                 style="width: 15rem;">
                        </div>
                        Скачать</a>
                </div>
            </div>
        <?php
        endforeach;
    }
    ?>
</div>

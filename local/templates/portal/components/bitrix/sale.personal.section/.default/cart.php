<?php

$APPLICATION->IncludeComponent(
    "bitrix:sale.basket.basket",
    "cart",
    array(
        "COMPONENT_TEMPLATE" => "cart",
        "COLUMNS_LIST_EXT" => array(
            0 => "PREVIEW_PICTURE",
            1 => "DETAIL_PICTURE",
            2 => "PREVIEW_TEXT",
            3 => "PROPS",
            4 => "DELETE",
            5 => "SUM",
            6 => "PROPERTY_BRAND",
            7 => "PROPERTY_STORES",
            8 => "PROPERTY_POST_DATE_TO",
            9 => "PROPERTY_POST_DATE_FROM",
        ),
        "PATH_TO_ORDER" => "/personal/order/make/",
        "HIDE_COUPON" => "N",
        "PRICE_VAT_SHOW_VALUE" => "N",
        "USE_PREPAYMENT" => "N",
        "QUANTITY_FLOAT" => "N",
        "CORRECT_RATIO" => "N",
        "AUTO_CALCULATION" => "Y",
        "SET_TITLE" => "Y",
        "ACTION_VARIABLE" => "basketAction",
        "COMPATIBLE_MODE" => "Y",
        "ADDITIONAL_PICT_PROP_1" => "-",
        "ADDITIONAL_PICT_PROP_4" => "-",
        "BASKET_IMAGES_SCALING" => "adaptive",
        "USE_GIFTS" => "N"
    ),
    false
);
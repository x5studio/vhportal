<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$vHelper = \VH\Portal\Helper::getInstance();

$userBrands = $vHelper->getUserBrands();

if ($request->isPost()){
    global $APPLICATION;

    $success = false;
    switch ($request->getQuery('type')){
        case 'stocks':
            $date = $request->getQuery('date');

            $brandReport = $request->getQuery('brand');
            $brandName   = '';

            if ($date){
                $date = new \Bitrix\Main\Type\Date($date);

                $brands = [];
                foreach ($userBrands as $brand){
                    if ($brandReport === 'all' || $brandReport == $brand['ID']){
                        $brands[] = $brand['UF_XML_ID'];
                        $brandName = $brandReport === 'all'
                            ? 'all'
                            : CUtil::translit($brand['UF_NAME'], 'ru');
                    }
                }
            }

            if (!empty($brands)){
                $APPLICATION->RestartBuffer();

                $output = fopen("php://output",'w');

                $iter = \VH\Portal\Helper::getReportDataForStocks(
                    $date,
                    $brands,
                    true,
                    true
                );

                \VH\Portal\Helper::writeCsvDataFromIter(
                    $output,
                    $iter,
                    ['Номер', 'Бренд', 'Наименование', 'Артикул',   'Штрих-код', 'Остаток'],
                    ['i',     'BRAND', 'NAME',         'ARTNUMBER', 'BARCODE',   'AMOUNT']
                );

                $fileName = "stocks_report_{$brandName}_{$date->toString()}.csv";

                header("Content-Type: application/csv");
                header("Content-Disposition: attachment; filename={$fileName}");

                fclose($output);

                $success = true;
            }
    }
    if (!$success){
        LocalRedirect($request->getRequestedPage());
    } else {
        die();
    }
}

/**
 * Получение данных
 */

$pageSize = 20;
$pageParam = 'page';


$nav = new Bitrix\Main\UI\PageNavigation($pageParam);
$nav->allowAllRecords(false)
    ->setPageSize($pageSize)
    ->initFromUri();

$offset = $nav->getOffset();
$limit = $nav->getLimit();

$iter = \VH\Portal\ReportDatesTable::getList([
    'select'        => ['DATE'],
    'order'         => ['DATE' => 'DESC'],
    'offset'        => $offset,
    'limit'         => $limit,
    'count_total'   => true,
]);

$nav->setRecordCount($iter->getCount());

$uri = new \Bitrix\Main\Web\Uri($request->getRequestedPage());

?>
<div class="office__content office__content_cart">
    <div class="office-head__link office-head__link_border mb_0 mb_20">
        <div class="office-head__icon-wrap">
            <img class="office-head__icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/user-menu-5-dark.svg" alt="">
        </div>

        <div class="office-head__hint">
            отчеты
        </div>

        <?php/*
        <div class="office-head__right">
            <div class="office-head__select">
                <div class="office-head__select-element">
                    <div class="select-wrap">
                        <select name="" class="select-wrap__element">
                            <option value="">все типы</option>
                            <option value="">не все типы</option>
                        </select>
                    </div>
                </div>

                <div class="office-head__select-element">
                    <div class="select-wrap">
                        <select name="" class="select-wrap__element">
                            <option value="">все бренды</option>
                            <option value="">не все бренды</option>
                        </select>
                    </div>
                </div>
                <div class="office-head__select-element">
                    <div class="select-wrap">
                        <select name="" class="select-wrap__element">
                            <option value="">по полугодиям</option>
                            <option value="">не по полугодиям</option>
                        </select>
                    </div>
                </div>
                <div class="office-head__select-element">
                    <div class="select-wrap">
                        <select name="" class="select-wrap__element">
                            <option value="">за все время</option>
                            <option value="">не за все время</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        */?>
    </div>

    <div class="marketing-content">
        <div class="office-card office-card_raports">

            <?php
            while ($date = $iter->fetch()):
                $date = $date['DATE'];

                /** @var \Bitrix\Main\Type\Date $date */

                foreach ($userBrands as $brand):
                    $uri->addParams([
                        'type'  => 'stocks',
                        'date'  => $date->toString(),
                        'brand' => $brand['ID'],
                    ]);
                ?>
            <div class="office-card__element office-card__element_status-3">
                <div class="office-card__wrap">
                    <div class="office-card__brand-hint"><?= $brand['UF_NAME'];?></div>
                    <div class="office-card__head mb_0">ведомость по товарам</div>
                    <div class="office-card__date mb_0"><?= FormatDate('f Y', $date->getTimestamp());//$date->format('f Y');?></div>

                    <div class="office-card__nav">
                        <?php/*
                        <div class="office-card__nav-element">
                            <a href="" class="btn btn_type-4">
                                <div class="btn__icon-wrap">
                                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/view-dark.svg" alt="" class="btn__icon">
                                </div>
                                смотреть
                            </a>
                        </div>
                        */?>

                        <div class="office-card__nav-element">
                            <form action="<?= $uri->getUri();?>" method="post">
                                <button data-href="<?= $uri->getUri();?>" class="btn btn_type-4">
                                    <div class="btn__icon-wrap">
                                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/download.svg" alt="" class="btn__icon">
                                    </div>
                                    скачать
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
                <?php
                endforeach;
            endwhile;
            ?>

        </div>
        <?php

        $APPLICATION->IncludeComponent('bitrix:main.pagenavigation', 'reports', [
            'NAV_OBJECT'  => $nav,
            'SHOW_ALWAYS' => false,
            'BASE_LINK'   => $request->getRequestedPage(),
        ]);

        ?>
    </div>

</div>

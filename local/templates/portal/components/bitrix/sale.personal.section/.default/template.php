<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
    $APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}

$availablePages = array();

if ($arParams['SHOW_PRIVATE_PAGE'] === 'Y')
{
    $availablePages[] = array(
        "path" => $arResult['PATH_TO_PRIVATE'],
        "name" => Loc::getMessage("SPS_PERSONAL_PAGE_NAME"),
    );
}

if ($arParams['SHOW_ORDER_PAGE'] === 'Y')
{
    $availablePages[] = array(
        "path" => $arResult['PATH_TO_ORDERS'],
        "name" => Loc::getMessage("SPS_ORDER_PAGE_NAME"),
    );

    /*
    $delimeter = ($arParams['SEF_MODE'] === 'Y') ? "?" : "&";
    $availablePages[] = array(
        "path" => $arResult['PATH_TO_ORDERS'].$delimeter."filter_history=Y",
        "name" => Loc::getMessage("SPS_ORDER_PAGE_HISTORY"),
    );
    */
}

if ($arParams['SHOW_ACCOUNT_PAGE'] === 'Y')
{
    $availablePages[] = array(
        "path" => $arResult['PATH_TO_ACCOUNT'],
        "name" => Loc::getMessage("SPS_ACCOUNT_PAGE_NAME"),
    );
}

if ($arParams['SHOW_PROFILE_PAGE'] === 'Y')
{
    $availablePages[] = array(
        "path" => $arResult['PATH_TO_PROFILE'],
        "name" => Loc::getMessage("SPS_PROFILE_PAGE_NAME"),
    );
}

if ($arParams['SHOW_BASKET_PAGE'] === 'Y')
{
    $arResult['PATH_TO_BASKET'] = $arParams['PATH_TO_BASKET'];
    $availablePages[] = array(
        "path" => $arParams['PATH_TO_BASKET'],
        "name" => Loc::getMessage("SPS_BASKET_PAGE_NAME"),
    );
}

if ($arParams['SHOW_SUBSCRIBE_PAGE'] === 'Y')
{
    $availablePages[] = array(
        "path" => $arResult['PATH_TO_SUBSCRIBE'],
        "name" => Loc::getMessage("SPS_SUBSCRIBE_PAGE_NAME"),
    );
}

if ($arParams['SHOW_CONTACT_PAGE'] === 'Y')
{
    $availablePages[] = array(
        "path" => $arParams['PATH_TO_CONTACT'],
        "name" => Loc::getMessage("SPS_CONTACT_PAGE_NAME"),
    );
}

if (!empty($arParams['PATH_TO_RATINGS']))
{
    $arResult['PATH_TO_RATINGS'] = $arParams['PATH_TO_RATINGS'];
}

if (!empty($arParams['PATH_TO_REPORTS']))
{
    $arResult['PATH_TO_REPORTS'] = $arParams['PATH_TO_REPORTS'];
}

if (!empty($arParams['PATH_TO_VACANCIES']))
{
    $arResult['PATH_TO_VACANCIES'] = $arParams['PATH_TO_VACANCIES'];
}

$customPagesList = CUtil::JsObjectToPhp($arParams['~CUSTOM_PAGES']);
if ($customPagesList)
{
    foreach ($customPagesList as $page)
    {
        $availablePages[] = array(
            "path" => $page[0],
            "name" => $page[1],
        );
    }
}

if (!isset($arResult['PAGE'])) {
    $arResult['PAGE'] = $GLOBALS['sDocPath'];
}
?>
<div class="marketing-top">
    <div class="marketing__left">
        <div class="back-btn">
            <div class="back-btn__icon-wrap">
				<a class="back-btn__link" href="/">
                    <img class="back-btn__icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/arr-back.svg" alt="">
                </a>
            </div>
        </div>
        <div class="marketing__head">Личный кабинет <span class="office-name"><?= $USER->GetFullName();?></span></div>
    </div>
    <div class="marketing__right">
        <a href="/logout=yes" class="head-page__nav-link">
            <div class="head-page__nav-icon-wrap">
                <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/exit.svg" class="head-page__nav-icon" alt="">
            </div>
            <div class="head-page__nav-hint">выйти из аккаунта</div>
        </a>
    </div>
</div>
<div class="office padding_bottom">
    <?php
    $APPLICATION->IncludeComponent("bitrix:menu","user_office", [
        "ROOT_MENU_TYPE" => "user",
        "POSITION" => "top",
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "user",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "Y",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => ""
    ]);

    switch ($arResult['PAGE']){
        case $arResult['PATH_TO_BASKET']:
            include('cart.php');
            break;
        case $arResult['PATH_TO_ORDERS']:
            include('orders.php');
            break;
        case $arResult['PATH_TO_ORDER_DETAIL']:
            include('order_detail.php');
            break;
        case $arResult['PATH_TO_RATINGS']:
            include('ratings.php');
            break;
        case $arResult['PATH_TO_REPORTS']:
            include('reports.php');
            break;
        case $arResult['PATH_TO_VACANCIES']:
            include('vacancies.php');
            break;
        default:
            define('ERROR_404', 'Y');
            show404();
    }
    ?>
</div>
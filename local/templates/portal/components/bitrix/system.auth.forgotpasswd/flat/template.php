<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

?>
<form class="enter__content" name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

    <?if($arResult["BACKURL"] <> ''):?>
        <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
    <?endif?>
    <input type="hidden" name="AUTH_FORM" value="Y">
    <input type="hidden" name="TYPE" value="SEND_PWD">

    <div class="enter__logo">
        <div class="enter__logo-wrap">
            <img src="/img/logo-mid.svg" class="enter__logo-img" alt="">
        </div>
    </div>
    <div class="enter__hint">
        <?=GetMessage("AUTH_FORGOT_PASSWORD")?>
    </div>
    <div class="enter__form">
        <div class="enter__form-center">

            <div class="input__row">
                <label class="input__label t_center"><?echo GetMessage("AUTH_LOGIN_EMAIL")?></label>
                <div class="input__wrap">
                    <input class="input__field input__field_type-1" type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" />
                    <input type="hidden" name="USER_EMAIL" />
                </div>
            </div>

            <?if($arResult["PHONE_REGISTRATION"]):?>
                <div class="input__row">
                    <label class="input__label t_center"><?echo GetMessage("forgot_pass_phone_number")?></label>
                    <div class="input__wrap">
                        <input class="input__field input__field_type-1" type="text" name="USER_PHONE_NUMBER" maxlength="255"/>
                    </div>
                </div>
            <?endif?>

            <?if ($arResult["USE_CAPTCHA"]):?>
                <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />

                <div class="input__row">
                    <div class="bx-captcha">
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                    </div>
                    <label for="f_captcha" class="input__label t_center"><?echo GetMessage("system_auth_captcha")?></label>
                    <div class="input__wrap">
                        <input id="f_captcha" class="input__field input__field_type-1" type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
                    </div>
                </div>
            <?endif?>

        </div>
        <?php
        if(!empty($arParams["~AUTH_RESULT"])):
            $text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
            $color = $arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "green":"red";
            ?>
            <div style="color:<?=$color;?>;margin-bottom: -45rem;"><?=nl2br(htmlspecialcharsbx($text))?></div>
        <?php endif; ?>

        <div class="input__row enter__btn mt_85">
            <button class="btn btn_type-1" type="submit" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>"><?=GetMessage("AUTH_SEND")?></button>
        </div>

        <div class="enter__remember-pass">
            <a href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow" class="enter__remember-pass-link"><?=GetMessage("AUTH_AUTH")?></a>
        </div>
    </div>
</form>

<script type="text/javascript">
    document.bform.onsubmit = function(){document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;};
    document.bform.USER_LOGIN.focus();
</script>
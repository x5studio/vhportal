<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult['ITEMS'])){
    return;
}



?>
<div class="event-client__day-last">
    <div class="event-client__head">прошедшие клиентские дни</div>
    <div class="event-day">

        <?php
        $shopsGallery = [];
        foreach ($arResult['ITEMS'] as $i => $item):
            $img = $item['DETAIL_PICTURE']['SRC'] ?? $item['PREVIEW_PICTURE']['SRC'];

            $attrs = "class=\"event-day__element";
            if (!empty($item['PROPERTIES']['MORE_PHOTO']['VALUE'])){
                foreach ($item['PROPERTIES']['MORE_PHOTO']['VALUE'] as $imgId){
                    $shopsGallery[$imgId][] = $i;
                }
                $attrs .=  " js-shop-album\" data-id-shop=\"{$item['IBLOCK_ID']}_{$item['ID']}\"";
            } else {
                $attrs .= '"';
            }

            ?>
        <div <?= $attrs;?>>
            <a href="" class="event-day__link">
				<div class="event-day__img-wrap" style="background:black">
					<img class="event-day__img" src="<?= $img;?>" alt="<?= $item['NAME'];?>" style="max-width:655rem;max-height:191rem;object-fit:cover;opacity:0.5">
                </div>
                <div class="event-day__content">
                    <div class="event-day__date"><?= $item['PROPERTIES']['DATE']['VALUE'];?></div>
                    <div class="event-day__bottom">
                        <div class="event-day__title"><?= $item['NAME'];?></div>
                        <div class="event-day__btn">смотреть фото</div>
                    </div>
                </div>
            </a>
        </div>
        <?php
        endforeach;

        if (!empty($shopsGallery)){
            $iter = \Bitrix\Main\FileTable::getList([
                'filter' => [
                    '@ID' => array_keys($shopsGallery),
                ],
            ]);
            $tmp = [];
            while ($img = $iter->fetch()){
                $path = CFile::GetFileSRC($img);
                foreach ($shopsGallery[$img['ID']] as $i){
                    $tmp[] = [
                        'shopId' => $arResult['ITEMS'][$i]['IBLOCK_ID'] . '_' . $arResult['ITEMS'][$i]['ID'],
                        'title'  => $arResult['ITEMS'][$i]['NAME'],
                        'short'  => $arResult['ITEMS'][$i]['NAME'],
                        'photo'  => $path,
                    ];
                }
            }
            $shopsGallery = $tmp;
        }

        $arResult['SHOPS_GALLERY'] = $shopsGallery;
        $this->getComponent()->setResultCacheKeys(['SHOPS_GALLERY']);

        ?>
    </div>
</div>
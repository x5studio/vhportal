<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var PageNavigationComponent $component */
$component = $this->getComponent();

/** @var \Bitrix\Main\UI\PageNavigation $nav */
$nav = $arParams['~NAV_OBJECT'];

$pages = [];

if ($nav->getPageCount() <= 5){
    for ($i = 1; $i <= $nav->getPageCount(); $i++){
        $pages[] = $i;
    }
} else {
    $pages[] = 1;
    $pages[] = $nav->getCurrentPage() - 1;
    $pages[] = $nav->getCurrentPage();
    $pages[] = $nav->getCurrentPage() + 1;

    $pages[] = $nav->getPageCount() - 1;
    $pages[] = $nav->getPageCount();

    $pages = array_unique(array_filter($pages, function ($i){
        return $i > 0;
    }));
}

?>
<div class="yiiPager-center">
    <ul class="yiiPager pager">
        <?php if ($nav->getCurrentPage() == 1): ?>
            <li class="prev disabled"><span></span></li>
        <?php else: ?>
            <li class="prev"><a href="<?= $component->replaceUrlTemplate($nav->getCurrentPage() - 1)?>"></a></li>
        <?php
        endif;


        $prev = null;
        foreach ($pages as $page){
            if ($prev != null && $prev + 1 != $page){
                echo '<li>...</li>';
            }
            ?>
            <li class="<?= $page == $nav->getCurrentPage() ? 'selected disabled' : '';?>"><a href="<?= $component->replaceUrlTemplate($page)?>" data-page="<?= $page-1;?>"><?= $page;?></a></li>
        <?php
            $prev = $page;
        }

        if ($nav->getPageCount() == $nav->getCurrentPage()): ?>
            <li class="next disabled"><span></span></li>
        <?php else: ?>
            <li class="next"><a href="<?= $component->replaceUrlTemplate($nav->getCurrentPage() + 1)?>"></a></li>
        <?php endif; ?>
    </ul>
</div>

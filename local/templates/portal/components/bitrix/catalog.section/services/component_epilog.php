<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

// Вывод в footer.php
addItemsToShopsGallery($arResult['SHOPS_GALLERY']);

if (!empty($arResult['DYNAMICS'])){
    $searches = [];
    $replaces = [];

    ob_start();
    foreach ($arResult['DYNAMICS'] as $key => $dynamic){
        if ($dynamic['TYPE'] === 'COMPONENT'){

            $r = $APPLICATION->IncludeComponent(...$dynamic['ARGS']);

            $searches[] = $key;
            $replaces[] = ob_get_contents();
        }
        ob_clean();
    }
    ob_end_clean();

    $arResult['CACHED_TPL'] = str_replace($searches, $replaces, $arResult['CACHED_TPL']);
}


echo $arResult['CACHED_TPL'];

if (!empty($arResult['~UF_RULES'])){
    ob_start();
    ?>

<div class="modal micromodal-slide modal-rules" id="rule-section-<?= $arResult['ID'];?>" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close="">
        <div class="modal-default">
            <div class="modal-close modal-close_callback" data-micromodal-close="">
                <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/remove.svg" alt="">
            </div>
            <div class="modal-default__content">
                <div class="rules rules__wrap js-default-wrap">
                    <?= $arResult['~UF_RULES'];?>
                </div>
            </div>
        </div>
    </div>
</div>

    <?php

    $APPLICATION->AddViewContent('footer', ob_get_contents());
    ob_end_clean();
} else {

}
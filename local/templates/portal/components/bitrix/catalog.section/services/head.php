<div class="marketing-top">
    <div class="marketing__left">
        <div class="back-btn">
            <div class="back-btn__icon-wrap">
                <a class="back-btn__link" href="<?= $arResult['BACK_URL'];?>">
                    <img class="back-btn__icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/arr-back.svg" alt="">
                </a>
            </div>
        </div>
        <div class="marketing__head"><?= $arResult['NAME'];?></div>
    </div><?php
    if ($hideHead){
        echo '</div>';
        return;
    }
    ?>
    <div class="marketing__right">
        <div class="marketing__nav">
            <?php
            if ($arResult['UF_TECH_DOC'] > 0):
                $path = CFile::GetPath($arResult['UF_TECH_DOC']);
                ?>
            <a href="<?= $path;?>" target="_blank" class="marketing__nav-link">
                <div class="marketing__nav-icon-wrap">
                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/download.svg" class="marketing__nav-icon" alt="">
                </div>
                <div class="marketing__nav-hint">скачать техническое задание</div>
            </a>
            <?php

            endif;

            if ($arResult['UF_RULES']):

                $rule = [
                    'id' => 'rule-section-'.$arResult['ID'],
                ];
                $rule['js'] = "MicroModal.show('{$rule['id']}',{})";
                ?>
            <a href="javascript: <?= $rule['js'];?>" aria-hidden="true" class="marketing__nav-link js-pop-rules" data-pop-id="<?= $rule['id'];?>">
                <div class="marketing__nav-icon-wrap">
                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/rules.svg" class="marketing__nav-icon" alt="">
                </div>
                <div class="marketing__nav-hint">Правила оформления промозон</div>
            </a>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="content-fon <?= $arResult['UF_HEAD_CLASS'];?> mb_50">
    <div class="event-client__top">
        <div class="event-client__top-left">
            <div class="event-client__top-content">
                <div class="event-client__top-head js-modal-head"><?= $arResult['NAME'];?></div>
            </div>
        </div>
        <div class="event-client__top-right">
            <div class="event-client__top-img-wrap">
                <img class="event-client__top-img" src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?? $arResult['PICTURE']['SRC'];?>" alt="<?= $arResult['NAME'];?>">
            </div>
        </div>
    </div>
</div>
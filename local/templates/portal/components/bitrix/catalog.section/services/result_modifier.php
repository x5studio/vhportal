<?php

/**
 * Stores
 */

$iter = CIBlockSection::GetList([], ['=IBLOCK_ID' => CATALOG_SHOPS, '=ACTIVE' => 'Y'], false, ['ID', 'NAME']);

$storeSections = [];
while ($s = $iter->Fetch()){
    $storeSections[$s['ID']] = $s['NAME'];
}


$arResult['STORES'] = [];
$iter = CIBlockElement::GetList([], ['=IBLOCK_ID' => CATALOG_SHOPS, '=ACTIVE' => 'Y']);
while ($store = $iter->GetNext()){
    $arResult['STORES'][$store['ID']] = $store;
    $arResult['STORES'][$store['ID']]['SECTION_NAME'] = $storeSections[$store['IBLOCK_SECTION_ID']];
}

$result = [];
CIBlockElement::GetPropertyValuesArray(
    $result,
    CATALOG_SHOPS,
    array('ID' => array_keys($arResult['STORES'])),
    ['CODE' => ['CARRIERS', 'ADDRESS', 'MORE_PHOTO', 'BIRTHDAY', 'POST_DATE_FROM', 'POST_DATE_TO']]
);

$images = [];
$ids    = [];
foreach ($result as $storeId => $v){
    $arResult['STORES'][$storeId]['ADDRESS'] = $v['ADDRESS']['VALUE'];

    $arResult['STORES'][$storeId]['POST_DATE_FROM'] = $v['POST_DATE_FROM']['VALUE'];
    $arResult['STORES'][$storeId]['POST_DATE_TO'] = $v['POST_DATE_TO']['VALUE'];

    $arResult['STORES'][$storeId]['BIRTHDAY'] = $v['BIRTHDAY']['VALUE'];

    $arResult['STORES'][$storeId]['BIRTHDAY_DATE'] = \Bitrix\Main\Type\Date::createFromText($v['BIRTHDAY']['VALUE']);


    $arResult['STORES'][$storeId]['CARRIERS'] = [];
    if (!empty($v['CARRIERS']['VALUE'])){
        foreach ($v['CARRIERS']['VALUE'] as $i => $n){
            $ids[$n][] = $storeId;
        }
    }

    if (!empty($v['MORE_PHOTO']['VALUE'])){
        foreach ($v['MORE_PHOTO']['VALUE'] as $imgId){
            $images[$imgId][] = $storeId;
        }
    }
}

$arResult['PROMOZONES'] = [];
if (!empty($ids)){
    $classHL = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity(PROMOZONES_HL_BLOCK)->getDataClass();
    $iter = $classHL::getList([
        'filter' => [
            '=UF_XML_ID' => array_keys($ids),
        ]
    ]);
    while ($el = $iter->fetch()){

        $arResult['PROMOZONES'][$el['UF_XML_ID']] = $el;

        foreach ($ids[$el['UF_XML_ID']] as $storeId){
            $fPhotos = false;
            foreach ($el['UF_PHOTOS'] as $imgId){
                $images[$imgId][] = [
                    $storeId, $el['UF_XML_ID']
                ];
                $fPhotos = true;
            }
            $arResult['STORES'][$storeId]['CARRIERS'][] = [
                'NAME'   => $el['UF_NAME'],
                'VALUE'  => $el['UF_DESCRIPTION'],
                'XML_ID' => $el['UF_XML_ID'],

                'SHOW_PHOTOS' => $fPhotos
            ];
        }
    }
}

$shopsGallery = [];

if (!empty($images)){
    $iter = \Bitrix\Main\FileTable::getList([
        'filter' => array_keys($images),
    ]);
    while ($img = $iter->fetch()){
        $path = CFile::GetFileSRC($img);

        foreach ($images[$img['ID']] as $key){
            if (is_array($key)){
                $storeId = $key[0];
                $shopId  = $key[1];
                $keyObj  = 'PROMOZONES';
                $keyName = 'UF_NAME';
            } else {
                $storeId = $key;
                $shopId  = $key;
                $keyObj  = 'STORES';
                $keyName = 'NAME';
            }

            $arResult[$keyObj][$shopId]['IMAGES'][] = $path;

            $shopsGallery[] = [
                'shopId' => $shopId,
                'title'  => $arResult[$keyObj][$shopId][$keyName],
                'short'  => '', //$arResult[$keyObj][$shopId][$keyName],
                'photo'  => $path,
            ];
        }
    }
}


// Сохранание в кэше
// SHOPS_GALLERY - Картинки галереи
// DYNAMICS - динамический контент
// CACHED_TPL - результат работы шаблона

$this->getComponent()->setResultCacheKeys(['SHOPS_GALLERY', 'DYNAMICS', 'CACHED_TPL', 'UF_RULES', '~UF_RULES']);

$arResult['SHOPS_GALLERY'] = $shopsGallery;
/**
 * Stores: End
 */

$iter = CIBlockSection::GetList(['LEFT_MARGIN' => 'ASC'], ['=SECTION_ID' => $arResult['ID'], 'IBLOCK_ID' => $arResult['IBLOCK_ID'], 'ACTIVE' => 'Y'], false, ['UF_*']);

$arResult['SUBSECTIONS'] = [];

while ($s = $iter->Fetch()){
    $arResult['SUBSECTIONS'][] = $s;
}

$arResult['PARENT'] = CIBlockSection::GetList(
    [],
    ['=ID' => $arResult['IBLOCK_SECTION_ID'], 'IBLOCK_ID' => $arResult['IBLOCK_ID']],
    false,
    ['*', 'UF_*']
)->GetNext();

if ($arResult['UF_TECH_DOC'] <= 0 && $arResult['PARENT']['UF_TECH_DOC'] > 0) {
    $arResult['UF_TECH_DOC'] = $arResult['PARENT']['UF_TECH_DOC'];
}

if (!empty($arResult['PARENT']['SECTION_PAGE_URL'])){
    $arResult['BACK_URL'] = $arResult['PARENT']['SECTION_PAGE_URL'];
} else {
    $arResult['BACK_URL'] = $arResult['LIST_PAGE_URL'];
}
<?php

$items = $arResult['ITEMS'];

include "list.php";


if (!empty($arResult['UF_RULES'])):
    $rules = explode("\r\n", $arResult['UF_RULES']);
    ?>
    <div class="rules center-content">
        <div class="rules__wrap">
            <div class="rules__head">правила размещения в приложении</div>
            <div class="rules__list">
                <?php
                foreach ($rules as $rule):
                    $rule = trim($rule);
                    if (empty($rule)) {
                        continue;
                    }
                    ?>
                    <div class="rules__element"><?= $rule; ?></div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="pack pack_mb-110">
    <?php
    foreach ($items as $i => $item):
        $classType = $i % 3 + 1;

        $img = !empty($item['DETAIL_PICTURE'])
            ? $item['DETAIL_PICTURE']['SRC']
            : (
            !empty($item['PREVIEW_PICTURE'])
                ? $item['PREVIEW_PICTURE']['SRC']
                : SITE_TEMPLATE_PATH . '/img/pack-1.svg'
            );

        $minPrice = null;
        foreach ($item['OFFERS'] as $offer){
            if ($minPrice === null || $minPrice['VALUE'] > $offer['MIN_PRICE']['VALUE']){
                $minPrice = $offer['MIN_PRICE'];
            }
        }

        $priceStr = '';
        if (strlen($minPrice['PRINT_VALUE']) > 0){
            $priceStrPrefix = count($item['OFFERS']) > 1 ? 'От ' : '';
            $priceStr = "{$priceStrPrefix}{$minPrice['PRINT_VALUE']}";
            if (!empty($item['PROPERTIES']['PERIOD_TEXT']['VALUE'])){
                $priceStr .= " / {$item['PROPERTIES']['PERIOD_TEXT']['VALUE']}";
            }
        }

        $description = !empty($item['PREVIEW_TEXT']) ? $item['PREVIEW_TEXT'] : $item['DETAIL_TEXT'];

        $description = str_replace(['<br>', '</br>', '<br/>', '<br />'], "\n", $description);

        ?>
    <div class="pack__element  pack__element_type-<?= $classType;?>">
        <div class="pack__element-wrap  js-block">
            <div class="pack__img-wrap">
                <img class="pack__img js-modal-img" src="<?= $img;?>" alt="">
            </div>
            <div class="pack__content">
                <div class="pack__title js-modal-title"><?= $item['NAME'];?></div>
                <div class="pack__note-list">
                    <div class="pack__note"><?= implode('</div><div class="pack__note">', explode("\n", $description));?></div>
                </div>
                <div class="pack__bottom">
                    <div class="pack__price"><?= $priceStr;?></div>

                    <div class="pack__to-cart">
                        <a href="<?= $pHelper->getAddToBasketUrl(['product_id' => $item['ID']]);?>" class="js-to-cart btn btn_type-1">
                            <div class="btn__icon-wrap">
                                <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/to-cart.svg" alt="" class="btn__icon">
                            </div>
                            в корзину
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php endforeach; ?>
</div>

<div class="center-content">
    <?php
    foreach ($items as $item):
        $img = !empty($item['DETAIL_PICTURE'])
            ? $item['DETAIL_PICTURE']['SRC']
            : (
            !empty($item['PREVIEW_PICTURE'])
                ? $item['PREVIEW_PICTURE']['SRC']
                : SITE_TEMPLATE_PATH . '/img/icons/contest.svg'
            );

        $minPrice = null;
        foreach ($item['OFFERS'] as $offer){
            if ($minPrice === null || $minPrice['VALUE'] > $offer['MIN_PRICE']['VALUE']){
                $minPrice = $offer['MIN_PRICE'];
            }
        }

        $priceStr = '';
        if (strlen($minPrice['PRINT_VALUE']) > 0){
            $priceStrPrefix = count($item['OFFERS']) > 1 ? 'От ' : '';
            $priceStr = "{$priceStrPrefix}{$minPrice['PRINT_VALUE']}";
            if (!empty($item['PROPERTIES']['PERIOD_TEXT']['VALUE'])){
                $priceStr .= " / {$item['PROPERTIES']['PERIOD_TEXT']['VALUE']}";
            }
        }
        ?>
    <div class="contest contest_type-1 mb_75">
        <div class="contest__wrap js-block <?= $listItemClass ?>">
            <div class="contest__top">
                <div class="contest__top-left">
                    <div class="contest__icon-wrap">
                        <img class="contest__icon js-modal-img" src="<?= $img?>" alt="<?= $item['NAME'];?>">
                    </div>
                    <div class="contest__title js-modal-title"><?= $item['NAME'];?></div>
                </div>

                <div class="contest__center">
                    <div class="contest__condition"><?= $item['PREVIEW_TEXT'];?></div>
                </div>

                <div class="contest__top-right">
                    <div class="price mb_25"><?= $priceStr;?></div>
                    <div class="contest__to-cart">
                        <a href="javascript:;" class="btn btn_type-1 js-to-cart" data-action="<?= $pHelper->getAddToBasketUrl(['product_id' => $item['ID']]);?>">
                            <div class="btn__icon-wrap">
                                <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/to-cart.svg" alt="" class="btn__icon">
                            </div>
                            в корзину
                        </a>
                    </div>
                </div>
            </div>
            <div class="contest__bottom">
                <?= $item['DETAIL_TEXT'];?>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
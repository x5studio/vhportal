<?php

foreach ($arResult['SUBSECTIONS'] as $section) {

    $items = array_filter($arResult['ITEMS'], function ($v) use (&$section) {
        return $section['ID'] == $v['~IBLOCK_SECTION_ID'];
    });

    if (empty($items)) {
        continue;
    }

    $items = array_values($items);

    $listItemClass = 'goods__wrap_digital-site';

    ?>
    <div class="center-content">
        <div class="client__head-2"><?= $section['NAME']; ?></div>
    </div>
    <?php
    /** @noinspection PhpIncludeInspection */
    include "{$section['UF_TEMPLATE']}.php";
}

if (!empty($arResult['UF_RULES'])):
    $rules = explode("\r\n", $arResult['UF_RULES']);
    ?>
    <div class="rules center-content">
        <div class="rules__wrap">
            <div class="rules__head">правила размещения на сайте</div>
            <div class="rules__list">
                <?php
                foreach ($rules as $rule):
                    $rule = trim($rule);
                    if (empty($rule)) {
                        continue;
                    }
                    ?>
                    <div class="rules__element"><?= $rule; ?></div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php

/** @var array $arResult */
/** @var array $items */

?>
<style type="text/css">
.promzone__element.active .hide-active {
    display: none;
}
.promzone__element .hide-active {
    width: 640rem;
}
.price_after_basket_btn {
    display: inline-block;
    margin-left: 27rem;
    padding: 6rem 6rem 6rem 0;
    font-size: 20rem;
}
</style>
<div class="promzone">
    <div class="promzone__wrap center">

        <?php
        foreach ($items as $i => $item):
            $j = $i % 3 + 1;

            $img = !empty($item['DETAIL_PICTURE'])
                ? $item['DETAIL_PICTURE']['SRC']
                : (
                    !empty($item['PREVIEW_PICTURE'])
                        ? $item['PREVIEW_PICTURE']['SRC']
                        : SITE_TEMPLATE_PATH . '/img/prom-luxe.svg'
                );

            $minPrice = null;
            $previousPrice = null;
            $priceStrPrefix = '';
            if (!empty($item['OFFERS'])) {
                foreach ($item['OFFERS'] as $offer) {
                    if ($minPrice === null || $minPrice['VALUE'] > $offer['MIN_PRICE']['VALUE']) {
                        $minPrice = $offer['MIN_PRICE'];
                    }
                    if ($previousPrice == null) {
                        $previousPrice = $offer['MIN_PRICE']['VALUE'];
                    } elseif ($previousPrice != $offer['MIN_PRICE']['VALUE']) {
                        $priceStrPrefix = 'От ';
                    }
                }
            } else {
                $minPrice = $item['MIN_PRICE'];
            }
            $priceStr = '';
            if (strlen($minPrice['PRINT_VALUE']) > 0){
                $priceStr = "{$priceStrPrefix}{$minPrice['PRINT_VALUE']}";
                if (!empty($item['PROPERTIES']['PERIOD_TEXT']['VALUE'])){
                    $priceStr .= " / {$item['PROPERTIES']['PERIOD_TEXT']['VALUE']}";
                }
            }
            ?>
            <div class="js-block promzone__element promzone__element_type-<?= $j;?>">

                <div class="promzone__element-wrap">

                    <div class="promzone__left">
                        <div class="promzone__img-wrap">
                            <img class="promzone__img js-modal-img" src="<?= $img; ?>" alt="<?= $item['NAME'];?>">
                        </div>
                        <div class="promzone__head">
                            <div class="promzone__title">
                                <a class="promzone__title-link js-modal-title"><?= $item['NAME'];?></a>
                                <?php if (!empty($item['~DETAIL_TEXT'])): ?>
                                    <div class="promad__note"><?= $item['~DETAIL_TEXT']; ?></div>
                                <?php endif; ?>
                            </div>

                            <div class="promzone__cart-wrap">
                                <a href="javascript:;" class="btn btn_type-1 js-to-cart" data-method="get" data-action="<?= $pHelper->getAddToBasketUrl(['product_id' => $item['ID']]);?>">
                                    <div class="btn__icon-wrap">
                                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/to-cart.svg" alt="" class="btn__icon">
                                    </div>
                                    в корзину
                                </a>
                                <?php if (!empty($minPrice)): ?>
                                <div class="price_after_basket_btn price price_big"><?= $priceStr;?></div>
                                <?php endif; ?>
                            </div>
                            <div class="hide-active promad__note"><?= !empty($item['~PREVIEW_TEXT']) ? '<br>'. $item['~PREVIEW_TEXT'] : '';?></div>
                        </div>
                    </div>

                    <div class="promzone__center">
                        <div class="promzone__shops">
                            <?php
                            $stores = '';

                            $isCityFormat = $item['CODE'] === 'city-format';

                            ob_start();
                            foreach ($item['PROPERTIES']['STORES']['VALUE'] as $storeId):
                                $store = $arResult['STORES'][$storeId];
                                ?>
                                <a href="<?= $store['DETAIL_PAGE_URL'];?>" class="promzone__shops-element">
                                    <div class="shop-info shop-info_mini">
                                        <div class="shop-info__city"><?= $store['SECTION_NAME'];?></div>
                                        <div class="shop-info__title"><?= $store['NAME'];?></div>
                                        <div class="shop-info__adds"><?= $store['ADDRESS'];?></div>
                                    </div>
                                </a>
                                <?php
                                ob_flush();

                                if ($isCityFormat){
                                    ob_clean();
                                    continue;
                                }

                                ?>

                                <div class="promzone__drop-block">
                                    <a href="<?= $store['DETAIL_PAGE_URL'];?>" class="promzone__drop-top">
                                        <div class="shop-info shop-info_mini">
                                            <div class="shop-info__city"><?= $store['SECTION_NAME'];?></div>
                                            <div class="shop-info__title"><?= $store['NAME'];?></div>
                                            <div class="shop-info__adds"><?= $store['ADDRESS'];?></div>
                                        </div>
                                    </a>
                                    <?php if (!empty($store['CARRIERS']) && !empty($item['PROPERTIES']['PROMOZONES']['VALUE'])): ?>
                                        <div class="promzone__drop-content">
                                            <div class="shop-info__attr">
                                                <?php
                                                uasort($store['CARRIERS'], function ($x, $y) {return strnatcasecmp($x['NAME'], $y['NAME']);});
                                                foreach ($store['CARRIERS'] as $v):
                                                    if (!in_array($v['XML_ID'], $item['PROPERTIES']['PROMOZONES']['VALUE'])) continue;

                                                    $attrs = '';
                                                    $class = '';

                                                    if ($v['SHOW_PHOTOS']){
                                                        $class = ' js-shop-album';
                                                        $attrs = "style=\"cursor: pointer\" data-id-shop=\"{$v['XML_ID']}\"";
                                                    }
                                                    ?>
                                                    <div class="shop-info__attr-element">
                                                        <div class="shop-info__attr-label<?= $class;?>" <?= $attrs;?> ><?= $v['NAME'];?></div>
                                                        <div class="shop-info__attr-val"><?= $v['VALUE'];?></div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                </div>
                                <?php
                                $stores .= ob_get_contents(); ob_clean();
                            endforeach;

                            ob_end_clean();
                            ?>
                        </div>
                    </div>

                    <?php

                    if ($isCityFormat):
                        ob_start();


                        $treeProps = [];
                        foreach ($item['OFFERS'] as $offer){

                            $prevValue = null;
                            foreach ($offer['DISPLAY_PROPERTIES'] as $prop){
                                if (empty($prop['VALUE'])){
                                    continue;
                                }
                                $key = $prop['VALUE'];

                                if ($prevValue === null){
                                    if (!isset($treeProps[$key])){
                                        $treeProps[$key] = [];
                                    }
                                    $prevValue = &$treeProps[$key];
                                } else {
                                    if (!isset($prevValue[$key])){
                                        $prevValue[$key] = [];
                                    }
                                    $prevValue = &$prevValue[$key];
                                }
                            }
                            if (empty($prevValue['MIN_PRICE']) || $prevValue['MIN_PRICE'] > $offer['MIN_PRICE']['VALUE']){
                                $prevValue['MIN_PRICE']     = $offer['MIN_PRICE']['VALUE'];
                                $prevValue['MIN_PRICE_FMT'] = $offer['MIN_PRICE']['PRINT_VALUE'];
                            }
                            if ($offer['~DETAIL_PICTURE'] > 0){
                                $prevValue['IMAGE'] = $offer['DETAIL_PICTURE']['SRC'];
                            }
                            unset($prevValue);
                        }

                        ?>
                    <div class="promad">
                        <div class="promad__wrap">
                            <a href="<?= $store['DETAIL_PAGE_URL'];?>" class="promad__left">
                                <div class="shop-info shop-info_mini">
                                    <div class="shop-info__city"><?= $store['SECTION_NAME'];?></div>
                                    <div class="shop-info__title"><?= $store['NAME'];?></div>
                                    <div class="shop-info__adds"><?= $store['ADDRESS'];?></div>
                                </div>
                            </a>
                            <div class="promad__center">
                                <div class="promad__note"><?= $item['~PREVIEW_TEXT'];?></div>
                                <div class="promad__list">


                                    <?php foreach ($treeProps as $value => $nextValue): ?>
                                    <div class="promad__block">
                                        <div class="promzone__drop-top">
                                            <div class="promad__top"><?= $value;?></div>
                                        </div>
                                        <div class="promzone__drop-content">
                                            <div class="shop-info__attr">
                                                <?php
                                                foreach ($nextValue as $nValue => $res):

                                                    $shopId = null;
                                                    $addClass = '';
                                                    $addAttrs = '';
                                                    if (!empty($res['IMAGE'])){
                                                        $shopId = md5($value . $nValue);
                                                        $addClass = ' js-shop-album';
                                                        $addAttrs = " data-id-shop=\"{$shopId}\" style=\"cursor: pointer;\"";

                                                        $arResult['SHOPS_GALLERY'][] = [
                                                            'shopId' => $shopId,
                                                            'title'  => $item['NAME'],
                                                            'short'  => "{$value} {$nValue}",
                                                            'photo'  => $res['IMAGE'],
                                                        ];
                                                    }
                                                    ?>
                                                <div class="shop-info__attr-element">
                                                    <div class="shop-info__attr-label<?= $addClass;?>"<?= $addAttrs;?>><?= $nValue;?></div>
                                                    <div class="shop-info__attr-val"><?= $res['MIN_PRICE_FMT'];?></div>
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>

                                    </div>
                                    <?php endforeach; ?>


                                </div>
                            </div>
                        </div>
                        <div><?= $item['DETAIL_TEXT'];?></div>
                    </div>

                    <?php

                        $stores = ob_get_contents();
                        ob_end_clean();

                    else:

                        $stores = !empty($stores)
                            ? "<div class=\"promzone__drop-wrap\">{$stores}</div>"
                            : '';

                    endif;

                    if (!empty($stores)): ?>
                    <div class="promzone__right">
                        <div class="promzone__more-btn">
                            <a href="" class="btn btn_type-2 js-slide-prom">
                                <div class="btn__icon-wrap">
                                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/arr-bottom.svg" alt="" class="btn__icon">
                                </div>
                                <span>подробнее</span>
                            </a>
                        </div>
                    </div>
                    <div class="promzone__drop"><?= $stores;?></div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
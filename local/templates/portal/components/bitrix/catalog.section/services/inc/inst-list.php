<?php
foreach ($items as $item):
    $img = !empty($item['DETAIL_PICTURE'])
        ? $item['DETAIL_PICTURE']['SRC']
        : (
        !empty($item['PREVIEW_PICTURE'])
            ? $item['PREVIEW_PICTURE']['SRC']
            : SITE_TEMPLATE_PATH . '/img/digital-item-1.png'
        );

    $minPrice = null;
    $priceStrPrefix = count($item['OFFERS']) > 1
        || (!empty($item['PROPERTIES']['PERIOD_TEXT']['VALUE']) && $item['PROPERTIES']['PERIOD_TEXT']['VALUE'] === 'От')
        ? 'От ' : '';
    foreach ($item['OFFERS'] as $offer){
        if ($minPrice === null || $minPrice['VALUE'] > $offer['MIN_PRICE']['VALUE']){
            $minPrice = $offer['MIN_PRICE'];
        }
    }

    ?>
    <div class="goods__element">
        <div class="js-block goods__wrap <?= $listItemClass;?>">
            <div class="goods__img-wrap"><img src="<?= $img;?>" alt="" class="js-modal-img goods__img"></div>
            <div class="goods__content">
                <div class="goods__content-title js-modal-title"><?= $item['NAME'];?></div>
                <div class="goods__content-price"><?php
                    $priceStr = '';
                    if (strlen($minPrice['PRINT_VALUE']) > 0){
                        $priceStr = "{$priceStrPrefix}{$minPrice['PRINT_VALUE']}";
                        if (!empty($item['PROPERTIES']['PERIOD_TEXT']['VALUE']) && $item['PROPERTIES']['PERIOD_TEXT']['VALUE'] !== 'От'){
                            $priceStr .= " / {$item['PROPERTIES']['PERIOD_TEXT']['VALUE']}";
                        }
                    }
                    echo $priceStr;
                    ?></div>
                <?php if ($minPrice['VALUE'] > 0): ?>
                    <div class="goods__content-btn">
                        <a href="javascript:;" class="btn btn_type-1 js-to-cart" data-id="<?= $item['ID'];?>" data-action="<?= $pHelper->getAddToBasketUrl(['product_id' => $item['ID']]);?>">
                            <div class="btn__icon-wrap">
                                <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/to-cart.svg" alt="<?= $item['NAME'];?>" class="btn__icon">
                            </div>
                            в корзину
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
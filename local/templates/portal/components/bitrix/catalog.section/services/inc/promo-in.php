<?php

foreach ($arResult['SUBSECTIONS'] as $i => $section){
    ?>
    <div class="center-content">
        <div class="head-page">
            <div class="head-page__left">
                <div class="head-page__head"><?= $section['NAME'];?></div>
            </div>

            <?php/*
            <div class="head-page__right">
                <div class="marketing__nav">
                    <a href="#" class="marketing__nav-link">
                        <div class="marketing__nav-icon-wrap">
                            <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/download.svg" class="marketing__nav-icon" alt="">
                        </div>
                        <div class="marketing__nav-hint">скачать техническую детализацию</div>
                    </a>
                    <a href="#" class="marketing__nav-link js-pop-rules">
                        <div class="marketing__nav-icon-wrap">
                            <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/rules.svg" class="marketing__nav-icon" alt="">
                        </div>
                        <div class="marketing__nav-hint">Правила оформления промозон</div>
                    </a>
                </div>
            </div>
            */?>

        </div>
    </div>
    <?php

    $items = array_filter($arResult['ITEMS'], function ($v) use (&$section){
        return $section['ID'] == $v['~IBLOCK_SECTION_ID'];
    });

    if ($i == 0){
        include 'list.php';
    } else { ?>
        <div class="prom-other">
        <?php
        $f = false;
        foreach ($items as $item):
            $img = !empty($item['DETAIL_PICTURE'])
                ? $item['DETAIL_PICTURE']['SRC']
                : (
                !empty($item['PREVIEW_PICTURE'])
                    ? $item['PREVIEW_PICTURE']['SRC']
                    : SITE_TEMPLATE_PATH . '/img/prom-other-column.svg'
                );

            $f = !$f;

            $minPrice = null;
            $previousPrice = null;
            $priceStrPrefix = '';
            foreach ($item['OFFERS'] as $offer){
                if ($minPrice === null || $minPrice['VALUE'] > $offer['MIN_PRICE']['VALUE']){
                    $minPrice = $offer['MIN_PRICE'];
                }
                if ($previousPrice == null) {
                    $previousPrice = $offer['MIN_PRICE']['VALUE'];
                } elseif ($previousPrice != $offer['MIN_PRICE']['VALUE']) {
                    $priceStrPrefix = 'От ';
                }
            }


            ?>
            <div class="js-block prom-other__element promzone__element_type-<?= $f ? '7' : '8';?>">
                <div class="prom-other__element-wrap">
                    <div class="prom-other__img-wrap">
                        <img class="prom-other__img js-modal" src="<?= $img;?>" alt="<?= $item['NAME'];?>">
                    </div>

                    <div class="prom-other__content">
                        <div class="prom-other__title">
                            <a href="" class="prom-other__title-link js-modal-title"><?= $item['NAME'];?></a>
                        </div>

                        <?php
                        if (!empty($item['PROPERTIES']['STORES']['VALUE'])){
                            ?>
                            <div class="prom-other__note"><?= $item['DETAIL_TEXT'] ?? $item['PREVIEW_TEXT'];?></div>
                            <div class="prom-other__shop">
                                <?php
                                foreach ($item['PROPERTIES']['STORES']['VALUE'] as $storeId):
                                    $store = $arResult['STORES'][$storeId]; ?>
                                    <div class="shop-info shop-info_mini">
                                    <div class="shop-info__city"><?= $store['SECTION_NAME']; ?></div>
                                    <div class="shop-info__title"><?= $store['NAME']; ?></div>
                                    <div class="shop-info__adds"><?= $store['ADDRESS']; ?></div>
                                    </div><?php
                                endforeach;
                                ?>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="prom-other__note prom-other__note_mt">
                                <?php
                                echo $item['~DETAIL_TEXT'];
                                foreach ($item['PROPERTIES']['STORES']['VALUE'] as $xmlId){
                                    echo "{$arResult['PROMOZONES'][$xmlId]['UF_NAME']} — {$arResult['PROMOZONES'][$xmlId]['UF_DESCRIPTION']} <br>";
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="prom-other__bottom">
                            <div class="prom-other__price">
                                <div class="price"><?php
                                    $priceStr = '';
                                    if (strlen($minPrice['PRINT_VALUE']) > 0){
                                        $priceStr = "{$priceStrPrefix}{$minPrice['PRINT_VALUE']}";
                                        if (!empty($item['PROPERTIES']['PERIOD_TEXT']['VALUE'])){
                                            $priceStr .= " / {$item['PROPERTIES']['PERIOD_TEXT']['VALUE']}";
                                        }
                                    }
                                    echo $priceStr;
                                    ?></div>
                            </div>
                            <div class="prom-other__btn">
                                <a href="javascript:;" class="btn btn_type-1 js-to-cart" data-action="<?= $pHelper->getAddToBasketUrl(['product_id' => $item['ID']]);?>">
                                    <div class="btn__icon-wrap">
                                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/to-cart.svg" alt="" class="btn__icon">
                                    </div>
                                    в корзину
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <?php
        endforeach;
        ?>

        </div>

        <?php
    }
}
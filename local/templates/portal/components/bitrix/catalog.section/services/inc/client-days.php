<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

?>

    <div class="js-block content-fon <?= $arResult['UF_HEAD_CLASS']; ?>">
        <div class="event-client__top">
            <div class="event-client__top-left">
                <div class="event-client__top-content">
                    <?php
                    foreach ($arResult['ITEMS'] as $item):

                        $minPrice = null;
                        $priceStrPrefix = count($item['OFFERS']) > 1
                        || (!empty($item['PROPERTIES']['PERIOD_TEXT']['VALUE']) && $item['PROPERTIES']['PERIOD_TEXT']['VALUE'] === 'От')
                            ? 'От ' : '';
                        foreach ($item['OFFERS'] as $offer){
                            if ($minPrice === null || $minPrice['VALUE'] > $offer['MIN_PRICE']['VALUE']){
                                $minPrice = $offer['MIN_PRICE'];
                            }
                        }

                        $description = !empty($item['PREVIEW_TEXT'])
                            ? $item['PREVIEW_TEXT']
                            : $item['DETAIL_TEXT'];

                        $description = str_replace('#PRICE#', $minPrice['PRINT_VALUE'], $description);

                        ?>
                        <div class="event-client__top-head js-modal-head js-modal-title">
                            <?= $item['NAME']; ?>
                        </div>
                        <div class="event-client__top-text">
                            <?= $description; ?>
                        </div>
                        <div class="event-client__top-btn-wrap">
                            <a href="javascript:;" class="btn btn_type-1 js-to-cart" data-method="get"
                               data-action="<?= $pHelper->getAddToBasketUrl(['product_id' => $item['ID']]); ?>">
                                <div class="btn__icon-wrap">
                                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/to-cart.svg" alt=""
                                         class="btn__icon">
                                </div>
                                <?= !empty($item['PROPERTIES']['BASKET_BTN_TITLE']['VALUE']) ? $item['PROPERTIES']['BASKET_BTN_TITLE']['VALUE'] : 'в корзину'; ?>
                            </a>
                        </div>
                        <div class="event-client__note mt_20"><?= $item['PROPERTIES']['NOTE']['VALUE']; ?></div>
                        <?php
                        break;
                    endforeach;
                    ?>
                </div>
            </div>
            <div class="event-client__top-right">
                <div class="event-client__top-img-wrap">
                    <img class="event-client__top-img js-modal-img" src="<?= $arResult['DETAIL_PICTURE']['SRC']; ?>"
                         alt="<?= $arResult['NAME']; ?>">
                </div>
            </div>
        </div>
    </div>
<?php

if ($arResult['CODE'] === 'client-days') {

    $description = $arResult['DESCRIPTION'];
    ?>

    <div class="event-client__wrap">
        <div class="event-client__left">
            <div class="event-client__content">
                <div class="event-client__text"><?= $description; ?></div>
                <div class="event-client__head">условия проведения:</div>
                <div class="event-client__list"><?= $arResult['~UF_RULES']; ?></div>
            </div>
        </div>


        <div class="event-client__right">
            <?php


            echo '#NEWS_LIST#';

            $arResult['DYNAMICS']['#NEWS_LIST#'] = [
                'TYPE' => 'COMPONENT',
                'ARGS' => [

                    'bitrix:news.list',
                    'last_client_days',
                    [
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",

                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "content",
                        "IBLOCK_ID" => "5",
                        "NEWS_COUNT" => "4",
                        "SORT_BY1" => "ID",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => Array("*"),
                        "PROPERTY_CODE" => Array(
                            "DATE",
                            "MORE_PHOTO",
                        ),

                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["CACHE_TIME"],
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "SET_STATUS_404" => "N",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => "",
                        "PAGER_BASE_LINK" => "",
                        "PAGER_PARAMS_NAME" => "arrPager",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => ""
                    ],

                ],
            ];


            ?>
        </div>

    </div>
    <?php

} elseif ($arResult['CODE'] === 'birthday-store') {

    ?>
    <div class="client-content">
        <div class="client__head-2">график дней рождения</div>
        <div class="client-calendar">
            <div class="client-calendar__scroll">

                <?php
                // Сортировка по дате рождения без учета года
                uasort($arResult['STORES'], function ($a, $b) {

                    /** @var \Bitrix\Main\Type\Date */
                    $date1 = $a['BIRTHDAY_DATE'];
                    /** @var \Bitrix\Main\Type\Date */
                    $date2 = $b['BIRTHDAY_DATE'];

                    list($month1, $day1) = explode(':', $date1->format('n:j'));
                    list($month2, $day2) = explode(':', $date2->format('n:j'));

                    $month1 = (int)$month1;
                    $month2 = (int)$month2;

                    $day1 = (int)$day1;
                    $day2 = (int)$day2;

                    return (((int)$month1 * 1000) + (int)$day1) > (((int)$month2 * 1000) + (int)$day2);
                });

                $i = 0;
                foreach ($arResult['STORES'] as $i => $store):
                    $i++;

                    /** @var \Bitrix\Main\Type\Date */
                    $birthDay = $store['BIRTHDAY_DATE'];

                    list($day, $month) = explode(':', FormatDate('j:F', $birthDay->getTimestamp()));
                    ?>
                    <div class="client-calendar__element client-calendar__element_shop-<?= $i; ?>">
                        <div class="client-calendar__date">
                            <div class="client-calendar__date-day"><?= $day; ?></div>
                            <div class="client-calendar__date-month"><?= $month; ?></div>
                        </div>
                        <div class="client-calendar__content">
                            <div class="client-calendar__top">
                                <div class="client-calendar__city"><?= $store['SECTION_NAME']; ?></div>
                                <div class="client-calendar__shop"><?= $store['NAME']; ?></div>
                                <div class="client-calendar__addss"><?= $store['ADDRESS']; ?></div>
                            </div>

                            <div class="client-calendar__btn-wrap">
                                <a href="javascript:;"
                                   class="btn btn_type-1 js-to-cart"
                                   data-method="get"
                                   data-action="<?= $pHelper->getAddToBasketUrl(['product_id' => $item['ID'], 'stores[]' => $store['ID']]); ?>"
                                >принять участие</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>

    </div>

    <?php
} elseif ($arResult['CODE'] === 'beauty-studio' || $arResult['CODE'] === 'wedding' || $arResult['CODE'] === 'brand-days') {

    $description = $arResult['DESCRIPTION'];
    ?>

    <div class="event-client__wrap">
        <div class="event-client__left">
            <div class="event-client__content">
                <div class="event-client__text"><?= $description; ?></div>
                <div class="event-client__head">условия проведения:</div>
                <div class="event-client__list"><?= $arResult['~UF_RULES']; ?></div>
            </div>
        </div>
    </div>
    <?
}
?>
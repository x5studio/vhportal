<div class="festival">
    <div class="festival__wrap list">
        <?php
        $f = false;
        $items = $arResult['ITEMS'];

        foreach ($items as $item):
            $img = !empty($item['DETAIL_PICTURE'])
                ? $item['DETAIL_PICTURE']['SRC']
                : (
                !empty($item['PREVIEW_PICTURE'])
                    ? $item['PREVIEW_PICTURE']['SRC']
                    : SITE_TEMPLATE_PATH . '/img/prom-luxe2.svg'
                );


            $minPrice = null;
            $previousPrice = null;
            $priceStrPrefix = '';
            foreach ($item['OFFERS'] as $offer){
                if ($minPrice === null || $minPrice['VALUE'] > $offer['MIN_PRICE']['VALUE']){
                    $minPrice = $offer['MIN_PRICE'];
                }
                if ($previousPrice == null) {
                    $previousPrice = $offer['MIN_PRICE']['VALUE'];
                } elseif ($previousPrice != $offer['MIN_PRICE']['VALUE']) {
                    $priceStrPrefix = 'От ';
                }
            }
            $priceStr = '';
            if (strlen($minPrice['PRINT_VALUE']) > 0){
                $priceStr = "{$priceStrPrefix}{$minPrice['PRINT_VALUE']}";
                if (!empty($item['PROPERTIES']['PERIOD_TEXT']['VALUE'])){
                    $priceStr .= " / {$item['PROPERTIES']['PERIOD_TEXT']['VALUE']}";
                }
            }

            $description = str_replace(['<br>', '</br>', '<br />', '<br/>'] , "\n", $item['DETAIL_TEXT'] ?? $item['PREVIEW_TEXT']);

            $description = trim($description);

            $description = explode("\n", $description);

            $f = !$f;

            ?>
            <div class="festival__element festival__element_small festival__element_type-<?= $f ? '2' : '3' ;?>">
                <div class="festival__element-wrap js-block">
                    <div class="festival-card__img-wrap">
                        <img class="js-modal-img festival-card__img" src="<?= $img;?>" alt="<?= $item['NAME'];?>">
                    </div>
                    <div class="festival-card__content">
                        <div class="js-modal-title festival-card__title"><?= $item['NAME'];?></div>
                        <?php if (!empty($description)):?>
                            <div class="festival-card__note-list">
                            <?php foreach ($description as $str):?>
                                <div class="festival-card__note"><?= $str;?></div>
                            <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        <div class="price"><?= $priceStr;?></div><br>
                        <div class="festival-card__to-cart">
                            <a href="javascript:;" class="btn btn_type-1 js-to-cart" data-method="get" data-action="<?= $pHelper->getAddToBasketUrl(['product_id' => $item['ID']]);?>">
                                <div class="btn__icon-wrap">
                                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/to-cart.svg" alt="" class="btn__icon">
                                </div>
                                в корзину
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        <?php endforeach; ?>

        <div class="festival__element festival__element_cond">
            <div class="festival__cond-wrap"><?= $arResult['DESCRIPTION'];?></div>
        </div>

    </div>
</div>
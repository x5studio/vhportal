<?php

ob_start();

/**
 * @var string 'list'
 */
$template = 'list';

if (!empty($arResult['UF_TEMPLATE'])){
    $template = $arResult['UF_TEMPLATE'];
}

if ($template === 'list'){
    $items = $arResult['ITEMS'];
}

$pHelper = \VH\Portal\Helper::getInstance();

$hideCodes = [
    'beauty-studio',
    'client-days',
    'staff-motivation',
    'birthday-store',
    'ramadan',
    'wedding',
    'pro',
    'brand-days',
];


$arResult['UF_HEAD_CLASS'] = !empty($arResult['UF_HEAD_CLASS'])
    ? $arResult['UF_HEAD_CLASS']
    : 'content-fon_prom-open';

$hideHead = in_array($arResult['CODE'], $hideCodes);

include "head.php";

/** @noinspection PhpIncludeInspection */
include "inc/{$template}.php";


// Вывод в component_epilog.php
$arResult['CACHED_TPL'] = ob_get_contents(); ob_end_clean();
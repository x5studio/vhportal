<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$pHelper = VH\Portal\Helper::getInstance();

$imgSrc = !empty($arResult['PICTURE']['SRC'])
    ? $arResult['PICTURE']['SRC']
    : (
        !empty($arResult['DETAIL_PICTURE']['SRC'])
            ? $arResult['DETAIL_PICTURE']['SRC']
            : SITE_TEMPLATE_PATH . '/img/training-img.png'
    );
?>
<div class="marketing-top">
    <div class="marketing__left">
        <div class="back-btn">
            <div class="back-btn__icon-wrap">
                <a class="back-btn__link" href="<?= $arResult['LIST_PAGE_URL'];?>">
                    <img class="back-btn__icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/arr-back.svg" alt="">
                </a>
            </div>
        </div>
        <div class="marketing__head"><?= $arResult['NAME'];?></div>
    </div>
</div>
<div class="content-fon content-fon_training mb_10">
    <div class="event-client__top">
        <div class="event-client__top-left">
            <div class="event-client__top-content">
                <div class="event-client__top-head js-modal-head">
                    тренинги<br>
                    в visagehall
                </div>
                <div class="event-client__text">
                    Правильно подготовленный персонал помогает увеличить продажи.
                </div>

            </div>
        </div>
        <div class="event-client__top-right">
            <div class="event-client__top-img-wrap">
                <img class="event-client__top-img" src="<?= $imgSrc;?>" alt="<?= $arResult['NAME'];?>">
            </div>
        </div>
    </div>
</div>

<div class="promzone">
    <div class="promzone__wrap center-content">
        <?php
        foreach($arResult['ITEMS'] as $item):

            $imgSrc = null;
            if (!empty($item['PROPERTIES']['SVG_IMAGE']['VALUE'])){
                $imgSrc = CFile::GetFileSRC(CFile::GetByID($item['PROPERTIES']['SVG_IMAGE']['VALUE'])->Fetch());
            } else {
                $imgSrc = !empty($item['PREVIEW_PICTURE']['SRC'])
                    ? $item['PREVIEW_PICTURE']['SRC']
                    : (
                        !empty($item['DETAIL_PICTURE']['SRC'])
                            ? $item['DETAIL_PICTURE']['SRC']
                            : ''
                    );
            }

            ?>
        <div class="js-block promzone__element promzone__element_type-6">
            <div class="promzone__element-wrap">
                <div class="promzone__left">
                    <div class="promzone__img-wrap">
                        <img class="js-modal-img promzone__img" src="<?= $imgSrc;?>" alt="<?= $item['NAME'];?>">
                    </div>
                    <div class="promzone__head">
                        <div class="promzone__title">
                            <div class="js-modal-title promzone__title-link"><?= $item['NAME'];?></div>
                        </div>

                        <div class="promzone__cart-wrap">
                            <a href="javascript:;" class="btn btn_type-1 js-to-cart" data-method="get" data-action="<?= $pHelper->getAddToBasketUrl(['product_id' => $item['ID']]);?>">
                                подать заявку на тренинг
                            </a>
                        </div>
                    </div>
                </div>

                <div class="promzone__center"><?= !empty($item['PREVIEW_TEXT']) ? $item['PREVIEW_TEXT'] : $item['DETAIL_TEXT'];?></div>

                <div class="promzone__right"></div>
            </div>
        </div>
        <?php endforeach; ?>

    </div>
</div>
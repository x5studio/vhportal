<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

ob_start();

//p($arResult);
?>
<div class="personal-open padding_bottom">

    <div class="marketing-top">
        <div class="marketing__left">
            <div class="back-btn">
                <div class="back-btn__icon-wrap">
                    <a class="back-btn__link" href="<?= $arResult['LIST_PAGE_URL'];?>">
                        <img class="back-btn__icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/arr-back.svg" alt="Персонал">
                    </a>
                </div>
            </div>
            <div class="marketing__head">Персонал</div>
        </div>
    </div>

    <div class="content-fon content-fon_personal mb_60">
        <div class="event-client__top">
            <div class="event-client__top-left">
                <div class="event-client__top-content">
                    <div class="event-client__top-head"><?= $arResult['NAME'] = str_replace_limit(' ', '<br>', $arResult['NAME'])?></div>
                    <div class="event-client__top-text">
                        Вы можете подать заявку на поиск работника.
                    </div>
                    <div class="event-client__top-btn-wrap">
                        <a href="javascript:;" class="btn btn_type-1">
                            опубликовать вакансию
                        </a>
                    </div>
                </div>
            </div>
            <div class="event-client__top-right">
                <div class="event-client__top-img-wrap">
                    <img class="event-client__top-img" src="<?= SITE_TEMPLATE_PATH;?>/img/personal-img.png" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="center-content">
        <div class="head-page">
            <div class="head-page__left">
                <div class="head-page__head">список резюме</div>
            </div>

            <div class="head-page__right">
                <div class="head-page__nav">
                    <?php if ($arResult['UF_RULES']):
                        $rule = [
                            'id' => 'rule-section-' . $arResult['ID'],
                        ];
                        $rule['js'] = "MicroModal.show('{$rule['id']}',{})";
                        ?>
                        <a href="javascript: <?= $rule['js']; ?>" aria-hidden="true"
                           class="head-page__nav-link js-pop-rules" data-pop-id="<?= $rule['id']; ?>">
                            <div class="head-page__nav-icon-wrap">
                                <img src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/rules.svg" class="head-page__nav-icon"
                                     alt="">
                            </div>
                            <div class="head-page__nav-hint">условия работы с персоналом</div>
                        </a>
                    <?php endif; ?>
                    <div class="head-page__select">#SMART_FILTER#</div>
                </div>
            </div>
        </div>
    </div>
    <div class="personal-list" id="items-container">
        <!-- items-container -->
        <?php
        foreach ($arResult['ITEMS'] as $item):
            /** @var array $img */
            $img = !empty($item['PREVIEW_PICTURE'])
                ? $item['PREVIEW_PICTURE']
                : (
                    !empty($item['DETAIL_PICTURE'])
                        ? $item['DETAIL_PICTURE']
                        : ''
                );
            if (!empty($img)){
                $imgSrc = getResizeImage($img, 400, 540, BX_RESIZE_IMAGE_EXACT);
            } else {
                $imgSrc = '';
            }
            ?>
        <div class="personal-list__element">
            <a href="<?= $item['DETAIL_PAGE_URL'];?>" class="personal-list__link">
                <div class="personal-list__photo-wrap">
                    <img src="<?= $imgSrc;?>" alt="<?= $item['NAME'];?>" class="personal-list__photo">
                </div>
                <div class="personal-list__content">
                    <div class="personal-list__name"><?= $item['PROPERTIES']['NAME']['VALUE'];?></div>
                    <div class="personal-list__hint"><?= $item['PROPERTIES']['PROFESSION']['VALUE'];?></div>
                    <?php/*<div class="personal-list__experience"><?= implode(', ', $item['PROPERTIES']['EXPERIENCE']['VALUE']);?></div>*/?>
                </div>
            </a>
        </div>
        <?php endforeach; ?>
        <!-- items-container -->
    </div>
    <div id="pagination-container">
        <!-- pagination-container -->
        <?= $arResult['NAV_STRING'];?>
        <!-- pagination-container -->
    </div>
    <script type="text/javascript">
        BX.ready(function () {
            $(document).on('click', '#pagination-container button', function (e) {
                var link = e.currentTarget.dataset.href;
                e.currentTarget.disabled = true;
                $.ajax(link, {dataType: "json"}).done(function (d) {
                    $('#items-container').append(d.items);
                    $('#pagination-container').html(d.pagination);
                });
            })
        });
    </script>
</div>
<?php

$arResult['CACHED_TPL'] = @ob_get_contents();
ob_end_clean();

<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var CBitrixComponent $this */

$content = $arResult['CACHED_TPL'];

$content = str_replace('#SMART_FILTER#', $APPLICATION->GetViewContent('personal_smart_filter'), $content);

echo $content;



//	ajax page load
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
if ($request->isAjaxRequest())
{
    $content = ob_get_contents();
    ob_end_clean();

    list(, $itemsContainer) = explode('<!-- items-container -->', $content);
    list(, $paginationContainer) = explode('<!-- pagination-container -->', $content);

    global $APPLICATION;

    $APPLICATION->RestartBuffer();
    echo \Bitrix\Main\Web\Json::encode(array(
        'items' => $itemsContainer,
        'pagination' => $paginationContainer,
    ));

    \CMain::FinalActions();
    die();
}

if (!empty($arResult['~UF_RULES'])) {
    ob_start();
    ?>

    <div class="modal micromodal-slide modal-rules" id="rule-section-<?= $arResult['ID']; ?>" aria-hidden="true">
        <div class="modal__overlay" tabindex="-1" data-micromodal-close="">
            <div class="modal-default">
                <div class="modal-close modal-close_callback" data-micromodal-close="">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/remove.svg" alt="">
                </div>
                <div class="modal-default__content">
                    <div class="rules rules__wrap js-default-wrap">
                        <?= $arResult['~UF_RULES']; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php

    $APPLICATION->AddViewContent('footer', ob_get_contents());
    ob_end_clean();
}
<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$this->getComponent()->setResultCacheKeys(['CACHED_TPL', 'UF_RULES', '~UF_RULES']);

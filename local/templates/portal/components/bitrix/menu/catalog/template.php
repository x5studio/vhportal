<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$isFooter = $arParams['IS_FOOTER'] === 'Y';

foreach ($arResult as $item):
    if ($item['DEPTH_LEVEL'] != 1){
        continue;
    }
    $selected = $item['SELECTED'] ? ' active' : '';
    ?>
    <div class="menu__element">
        <a href="<?= $item['LINK'];?>" class="menu__link<?= $selected;?>"><?= $item['TEXT'];?></a>
        <?php

        $children = $item['PARAMS']['CHILDREN'];
        if (empty($children) || !$isFooter){
            echo '</div>';
            continue;
        }

        $showLevel3 = $isFooter && !empty($arResult[$children[0]['INDEX']]['PARAMS']['CHILDREN']);

        if ($showLevel3):
        ?>
        <div class="menu__child">
            <?php
            foreach ($children as $child):
                $child = $arResult[$child['INDEX']];
                ?>
            <div class="menu__child-wrap">
                <div class="menu__child-head"><a class="menu__child-head-link" href="<?= $child['LINK'];?>"><?= $child['TEXT'];?></a></div>
                <div class="menu__child-list">
                    <?php
                    foreach ($child['PARAMS']['CHILDREN'] as $grandChild):
                        $grandChild = $arResult[$grandChild['INDEX']];
                    ?>
                    <div class="menu__child-element">
                        <a href="<?= $grandChild['LINK'];?>" class="menu__child-link"><?= $grandChild['TEXT'];?></a>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <?php else: ?>
        <div class="menu__child">
            <div class="menu__child-wrap">
                <div class="menu__child-list">
                    <?php
                    foreach ($children as $child):
                        $child = $arResult[$child['INDEX']];
                        ?>
                        <div class="menu__child-element">
                            <a href="<?= $child['LINK'];?>" class="menu__child-link"><?= $child['TEXT'];?></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
<?php endforeach; ?>
<?php

$position = $arParams['POSITION'];

$position = !in_array($position, ['top', 'bottom']) ? 'top' : $position;

$count = count($arResult);

global $USER;

$classes = [
    'top'       => '',
    'bottom'    => [
        'user-wrap' => 'user-name__wrap_footer',
        'user-drog' => 'user-drop_footer',
    ]
];

?>
<div class="user-name">
    <div class="user-name__wrap <?= $classes[$position]['user-wrap']; ?>">
        <div class="user-name__val">
            <a href="<?= $USER->IsAuthorized() ? '' : '/personal/'; ?>" class="user-name__val-link">
                <?= $USER->IsAuthorized() ? $USER->GetFormattedName() : 'Войти'; ?>
            </a>
        </div>
        <div class="user-name__icon-wrap">
            <img class="user-name__icon"
                 src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/<?= $USER->IsAuthorized() ? 'menu.svg' : 'user-menu-1-dark.svg'; ?>"
                 alt="">
        </div>
    </div>
    <?php if ($USER->IsAuthorized()): ?>
        <div class="user-drop <?= $classes[$position]['user-drog']; ?>">
            <div class="user-drop__wrap">
                <?php
                foreach ($arResult as $i => $item):
                    $class = 'user-drop__element';
                    if ($i + 1 == $count) {
                        $class .= ' user-drop__element_exit';
                    }
                    $icon = $item['PARAMS']['ICON'];
                    if (empty($icon)) {
                    $icon = $i+1;
                }
                ?>
                <div class="<?= $class;?>">
                    <a href="<?= $item['LINK'];?>" class="user-drop__link">
                        <div class="user-drop__icon-wrap">
                            <img class="user-drop__icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/user-menu-<?= $icon;?>.svg" alt="<?= $item['TEXT'];?>">
                        </div>

                        <div class="user-drop__hint"><?= $item['TEXT'];?>
                            <?php if ($item['PARAMS']['POSITIONS'] > 0): ?>
                                <span class="user-drop__cart-count"> +<?= $item['PARAMS']['POSITIONS']; ?></span>
                            <?php endif; ?>
                        </div>
                    </a>
                </div>
                <?php endforeach; ?>


            </div>
        </div>
    <?php endif; ?>
</div>
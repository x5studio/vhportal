<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

?>

<div class="category">
    <div class="category__element category__element_type-1">
        <div class="category__img-wrap">
            <img src="<?= SITE_TEMPLATE_PATH;?>/img/cat1.jpg" class="category__img" alt="">
        </div>
        <div class="category__content">
            <div class="categoty__hint">
                <a href="/" class="category__hint-link">VisageHall <div class="categoty__hint-note">b2b</div></a>
            </div>
        </div>
    </div>

<?php

$checkFunc = function($item){
    $isIblock   = $item['PARAMS']['IS_IBLOCK'];
    $id         = $item['PARAMS']['ID'];
    $parent     = $item['PARAMS']['PARENT'];
    $depthLevel = $item['DEPTH_LEVEL'];

    return ($isIblock && in_array($id, [CATALOG_PERSONAL, CATALOG_SHOPS]) && $depthLevel == 1)
        || (!$isIblock && $parent == CATALOG_MARKETING && $depthLevel == 2);
};

$i = 0;
foreach ($arResult as $item):
    if (!$checkFunc($item)){
        continue;
    }
    $i++;
    $menuElementType = $i > 2 ? 3 : 2;

    $children = $item['PARAMS']['CHILDREN'];
    ?>


    <div class="category__element category__element_type-<?= $menuElementType;?>">
        <div class="category__img-wrap">
            <img src="<?= CFile::GetPath($item['PARAMS']['PICTURE']);?>" class="category__img" alt="">
        </div>
        <div class="category__content">
            <div class="categoty__hint">
                <a href="<?= $item['LINK'];?>" class="category__hint-link"><?= $item['TEXT'];?></a>
            </div>
            <?php

            // Вывод магазинов
            $showItems = $item['PARAMS']['IS_IBLOCK'] && $item['PARAMS']['ID'] == CATALOG_SHOPS;

            if (!$showItems && empty($children)){
               echo '</div></div>';
               continue;
            }

            if (!$showItems):
            ?>
            <div class="category__child">
                <?php
                foreach($children as $child):
                    $child = $arResult[$child['INDEX']];
                    ?>
                <div class="category__child-element"><a href="<?= $child['LINK'];?>" class="category__child-link"><?= $child['TEXT'];?></a></div>
                <?php endforeach; ?>
            </div>
            <?php else:
                $itemsIter = CIBlockElement::GetList([], [
                        'ACTIVE'    => 'Y',
                        'IBLOCK_ID' => CATALOG_SHOPS,
                ], false, false, ['ID', 'NAME', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL', 'PROPERTY_ADDRESS']);

                $sectionStores = [];
                while ($store = $itemsIter->GetNext()){
                    $sectionStores[ $store['IBLOCK_SECTION_ID'] ][$store['ID']] = [
                            'NAME' => $store['NAME'],
                            'LINK' => $store['DETAIL_PAGE_URL'],
                            'ADDRESS' => $store['PROPERTY_ADDRESS_VALUE'],
                    ];
                }

                foreach($children as $child):
                    $child = $arResult[$child['INDEX']];
                    ?>
                    <div class="category__adds-hint"><?= $child['TEXT'];?></div>
                    <?php
                    if (empty($sectionStores[$child['PARAMS']['ID']])){
                        continue;
                    }
                    ?>
                    <div class="category__child">
                        <?php foreach($sectionStores[$child['PARAMS']['ID']] as $childItem): ?>
                        <div class="category__child-element"><a class="category__child-link" href="<?= $childItem['LINK']; ?>"><?= $childItem['NAME'] . (!empty($childItem['ADDRESS']) ? "<br><span style='font-weight:400;font-size:80%'>{$childItem['ADDRESS']}</span>" : '');?></a></div>
                        <?php endforeach; ?>
                    </div>
                <?php
                endforeach;
            endif;
            ?>
        </div>
    </div>

<?php endforeach; ?>
</div>
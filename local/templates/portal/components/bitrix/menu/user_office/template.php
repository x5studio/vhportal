<div class="office__nav">
    <div class="office-list">
        <?php
        $count = count($arResult);
        foreach ($arResult as $i => $item):
            if ($i + 1 == $count) {
                break;
            }
            $active = $item['SELECTED'] ? ' active' : '';
            $icon = $item['PARAMS']['ICON'];
            if (empty($icon)){
                $icon = $i+1;
            }
            ?>
            <div class="office-list__element">
                <a href="<?= $item['LINK'];?>" class="office-list__link<?= $active;?>">
                    <div class="office-list__icon-wrap">
                        <img class="office-list__icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/user-menu-<?= $icon;?>-dark.svg" alt="<?= $item['TEXT'];?>">
                    </div>
                    <div class="office-list__hint"><?= $item['TEXT'];?>
                        <?php if ($item['PARAMS']['POSITIONS'] > 0): ?>
                            <span class="office-list__cart-count"> +<?= $item['PARAMS']['POSITIONS'];?></span>
                        <?php endif; ?>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>
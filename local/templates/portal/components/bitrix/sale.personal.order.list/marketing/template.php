<div class="office__content office__content_cart">
    <div class="office-head__link office-head__link_border mb_0 mb_20">
        <div class="office-head__icon-wrap">
            <img class="office-head__icon" src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/user-menu-5-dark.svg" alt="">
        </div>

        <div class="office-head__hint">
            Маркетинг-планы
        </div>

        <div class="office-head__right">
            <a href="?show_all=Y" class="head-page__nav-link">
                <div class="office-head__icon-wrap">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/view-dark.svg" class="office-head__icon" alt=""
                         style="width: 26rem;">
                </div>
                <div class="office-head__hint">Смотреть все</div>
            </a>
        </div>

        <? /*
        <div class="office-head__right">
            <div class="office-head__select">
                <div class="office-head__select-element">
                    <div class="select-wrap">
                        <select name="" class="select-wrap__element">
                            <option value="">все бренды</option>
                            <option value="">не все бренды</option>
                        </select>
                    </div>
                </div>

                <div class="office-head__select-element">
                    <div class="select-wrap">
                        <select name="" class="select-wrap__element">
                            <option value="">за все время</option>
                            <option value="">не за все время</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        */?>

    </div>

    <div class="marketing-content">
        <div class="office-card office-card_marketing">

            <?php

            $i = 3;
            foreach ($arResult['ORDERS'] as /*$i =>*/ $tmp):
                $order = $tmp['ORDER'];
                ?>
            <div class="office-card__element office-card__element_status-<?= $i;?>">
                <div class="office-card__wrap"><?php/*
                    <div class="office-card__brand-hint">moschino</div>*/?>
                    <div class="office-card__head mb_0">Заявка №<?= $order['ID'];?> от <?= $order['DATE_INSERT_FORMATED'];?></div>

                    <div class="office-card__nav">
                        <div class="office-card__nav-element">
                            <a href="<?= $order['URL_TO_DETAIL'];?>" class="btn btn_type-4">
                                <div class="btn__icon-wrap">
                                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/view-dark.svg" alt="" class="btn__icon">
                                </div>
                                смотреть
                            </a>
                        </div>
                        <?php/*
                        <div class="office-card__nav-element">
                            <a href="" class="btn btn_type-4">
                                <div class="btn__icon-wrap">
                                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/download.svg" alt="" class="btn__icon">
                                </div>
                                скачать
                            </a>
                        </div>*/?>
                    </div>
                </div>
            </div>
            <?php
            endforeach;
            ?>


        </div>
<?php/*
        <div class="yiiPager-center">
            <ul class="yiiPager pager">
                <li class="prev disabled"><span></span></li>
                <li class="selected"><a href="/news?page=1&amp;per-page=10" data-page="0">1</a></li>
                <li class=""><a href="/news?page=2&amp;per-page=10" data-page="1">2</a></li>
                <li class=""><a href="/news?page=3&amp;per-page=10" data-page="2">3</a></li>
                <li class=""><a href="/news?page=4&amp;per-page=10" data-page="3">4</a></li>
                <li class=""><a href="/news?page=5&amp;per-page=10" data-page="4">...</a></li>
                <li class=""><a href="/news?page=6&amp;per-page=10" data-page="5">13</a></li>
                <li class="next"><a href="/news?page=2&amp;per-page=10" data-page="1"></a></li>
            </ul>
        </div>
*/?>

    </div>

</div>

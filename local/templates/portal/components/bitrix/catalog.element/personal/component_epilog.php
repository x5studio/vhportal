<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$content = $arResult['CACHED_TPL'];

$searches = [];
$replaces = [];

$iter = CIBlockElement::GetList(
    [],
    [
        'IBLOCK_ID' => VACANCY_INVITES_BLOCK_ID,
        'PROPERTY_VACANCY' => $arResult['ID'],
        'PROPERTY_USER' => $USER->GetID(),
        'PROPERTY_DONE' => false,
    ],
    false,
    false,
    ['*', 'PROPERTY_INTERVIEW_DATE']
);
$invite = $iter->Fetch();

ob_start();

if (empty($invite)){
    ?>
    <form method="post" class="vacancy-open__btn" action="<?= $arResult['DETAIL_PAGE_URL']; ?>invite">
        <button type="submit" class="btn btn_type-1">
            Пригласить на собеседование
        </button>
    </form>
    <?php
} else {
    ?>
    <div class="vacancy-open__interview-time">
        <div class="vacancy-open__interview-time-content">
            Приглашение на собеседование уже отправлено
        </div>
    </div>
    <?php
}

$searches[] = '#VACANCY_INVITE#';
$replaces[] = ob_get_contents();

ob_clean();

if (!empty($invite['PROPERTY_INTERVIEW_DATE_VALUE'])){
    ?>
    <div class="vacancy-open__interview-time">
        <div class="vacancy-open__interview-time-content">
            собеседование пройдет <?= $invite['PROPERTY_INTERVIEW_DATE_VALUE'];?>
        </div>
    </div>
    <?php
}
$searches[] = '#INTERVIEW_DATE#';
$replaces[] = ob_get_contents();

ob_end_clean();

$content = str_replace($searches, $replaces, $content);

echo $content;

if (!empty($arResult['SECTION']['~UF_RULES'])) {
    ob_start();
    ?>

    <div class="modal micromodal-slide modal-rules" id="rule-section-<?= $arResult['SECTION']['ID']; ?>"
         aria-hidden="true">
        <div class="modal__overlay" tabindex="-1" data-micromodal-close="">
            <div class="modal-default">
                <div class="modal-close modal-close_callback" data-micromodal-close="">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/remove.svg" alt="">
                </div>
                <div class="modal-default__content">
                    <div class="rules rules__wrap js-default-wrap">
                        <?= $arResult['SECTION']['~UF_RULES']; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php

    $APPLICATION->AddViewContent('footer', ob_get_contents());
    ob_end_clean();
}
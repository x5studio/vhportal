<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

ob_start();

$img = !empty($arResult['DETAIL_PICTURE'])
    ? $arResult['DETAIL_PICTURE']
    : (
    !empty($arResult['PREVIEW_PICTURE'])
        ? $arResult['PREVIEW_PICTURE']
        : ''
    );

if (!empty($img)){
    $imgSrc = getResizeImage($img, 400, 540);
}


?>
<div class="vacancy-open-page padding_bottom">
    <div class="marketing-top">
        <div class="marketing__left">
            <div class="back-btn">
                <div class="back-btn__icon-wrap">
                    <a class="back-btn__link" href="<?= $arResult['SECTION']['SECTION_PAGE_URL']; ?>">
                        <img class="back-btn__icon" src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/arr-back.svg"
                             alt="<?= $arResult['SECTION']['NAME']; ?>">
                    </a>
                </div>
            </div>
            <div class="marketing__head"><?= $arResult['SECTION']['NAME']; ?></div>
        </div>

        <div class="marketing__right">
            <?php if ($arResult['SECTION']['UF_RULES']):
                $rule = [
                    'id' => 'rule-section-' . $arResult['SECTION']['ID'],
                ];
                $rule['js'] = "MicroModal.show('{$rule['id']}',{})";
                ?>
                <a href="javascript: <?= $rule['js']; ?>" aria-hidden="true" class="head-page__nav-link js-pop-rules"
                   data-pop-id="<?= $rule['id']; ?>">
                    <div class="head-page__nav-icon-wrap">
                        <img src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/rules.svg" class="head-page__nav-icon" alt="">
                    </div>
                    <div class="head-page__nav-hint">условия работы с персоналом</div>
                </a>
            <?php endif; ?>
        </div>
    </div>
    <div class="center-content">
        <div class="vacancy-open">
            <div class="vacancy-open__left">
                <a class="vacancy-open__photo-wrap" target="_blank" href="<?= $img['SRC']; ?>">
                    <img class="vacancy-open__photo" src="<?= $imgSrc; ?>"
                         alt="<?= $arResult['PROPERTIES']['NAME']['VALUE']; ?>">
                </a>
            </div>

            <div class="vacancy-open__right">
                #INTERVIEW_DATE#

                <div class="vacancy-open__attr">

                    <?php foreach ($arResult['DISPLAY_PROPERTIES'] as $prop): ?>
                    <div class="vacancy-open__attr-row">
                        <div class="vacancy-open__attr-label"><?= $prop['NAME'];?></div>
                        <div class="vacancy-open__attr-val"><?= $prop['MULTIPLE'] === 'Y' ? implode('<br>', $prop['VALUE']) : $prop['VALUE'];?></div>
                    </div>
                    <?php endforeach; ?>

                </div>

                #VACANCY_INVITE#
            </div>
        </div>
    </div>
</div>
<?php
$arResult['CACHED_TPL'] = ob_get_contents();
ob_end_clean();
<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arResult['SECTION'] += CIBlockSection::GetList([], ['IBLOCK_ID' => $arResult['IBLOCK_ID'], 'ID' => $arResult['SECTION']['ID']], false, ['UF_*'])->GetNext() ?: [];

$this->getComponent()->setResultCacheKeys(['CACHED_TPL', 'DETAIL_PAGE_URL', 'SECTION']);

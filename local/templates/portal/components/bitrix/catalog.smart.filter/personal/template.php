<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$formAction = $arResult['FILTER_URL']; //$arResult['FORM_ACTION'];

$html = '';

$optionTpl = '<option value="#VALUE#" #SELECTED#>#NAME#</option>';
$selectTpl = '<select name="#NAME#" class="select-wrap__element select2-off">#LIST#</select>';

foreach ($arResult['ITEMS'] as $id => $prop){
    if (
        $prop['CODE'] !== $arParams['FILTER_PROP']
        || empty($prop['VALUES'])
    ){
        continue;
    }

    $options = '<option value="">все профессии</option>';
    $name = '';

    foreach ($prop['VALUES'] as $value){
        if ($name === ''){
            $name = $value['CONTROL_NAME_ALT'];
        }
        $options .= str_replace(
            ['#NAME#', '#VALUE#', '#SELECTED#'],
            [$value['VALUE'], $value['HTML_VALUE_ALT'], $value['CHECKED'] ? 'selected="selected"' : ''],
            $optionTpl
        );
    }
    $html = str_replace(
        ['#NAME#', '#LIST#'],
        [$name, $options],
        $selectTpl
    );

    break;
}
if (empty($html)){
    return;
}

$forId = 'form_' . md5($formAction);
?>
<form action="<?= $formAction;?>" id="<?= $forId;?>">
    <input type="hidden" name="set_filter" value="y"/>
    <?php
    foreach ($arResult['HIDDEN'] as $hidden)
        echo "<input type=\"hidden\" name=\"{$hidden['CONTROL_NAME']}\" value=\"{$hidden['HTML_VALUE']}\" />";
    ?>
    <div class="select-wrap"><?= $html;?></div>
</form>
<script type="text/javascript">
(function(){
    let form = document.querySelector('#<?= $forId;?>');

    form.onchange = function(e){
        e.target.closest('form').submit();
    };

})();
</script>
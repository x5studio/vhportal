<?php

/**
 * Фотки
 */

$detailPicture = $arResult['DETAIL_PICTURE'];

$morePhoto = $arResult['PROPERTIES']['MORE_PHOTO']['VALUE'];

$photos = !empty($detailPicture) ? [
    $detailPicture + [
        'SMALL' => CFile::ResizeImageGet($detailPicture, ['width' => 143, 'height' => 99], BX_RESIZE_IMAGE_PROPORTIONAL_ALT)['src']
    ]
] : [];

if (!empty($morePhoto)){
    $iter = Bitrix\Main\FileTable::getList([
        'filter' => ['=ID' => $morePhoto]
    ]);
    while ($img = $iter->fetch()){
        $photos[array_search($img['ID'], $morePhoto)] = $img + [
            'SRC'   => CFile::GetFileSRC($img),
            'SMALL' => CFile::ResizeImageGet($img, ['width' => 143, 'height' => 99], BX_RESIZE_IMAGE_PROPORTIONAL_ALT)['src']
        ];
    }
}
ksort($photos);
$arResult['MORE_PHOTO'] = $photos;

unset($photos);
unset($morePhoto);
unset($detailPicture);

/**
 * =====================================================================
 */
<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$sections = [];
$iter = \Bitrix\Sale\SectionTable::getList([
    'select' => ['ID', 'NAME']
]);
while ($section = $iter->fetch()){
    $sections[$section['ID']] = $section['NAME'];
}


?>
<div class="shop">
    <div class="shop__wrap">
        <div class="grid-container">
            <?php
            foreach ($arResult['ITEMS'] as $i => $shop):
                $address = $shop['PROPERTIES']['ADDRESS']['VALUE'];
                if (!empty($address)){
                    $tmp = strpos($address, 'visagehall.ru');
                    if ($tmp){
                        $shop['DETAIL_PAGE_URL'] = $address . '" target="_blank';
                        $address = '';
                    }
                }
                ?>
            <div class="shop-<?= ($i+1);?>">
                <a href="<?= $shop['DETAIL_PAGE_URL'];?>" class="shop__box">
                    <div class="shop__content">
                        <div class="shop__content-wrap">
                            <div class="shop-info">
                                <div class="shop-info__city"><?= $sections[$shop['IBLOCK_SECTION_ID']];?></div>
                                <div class="shop-info__title"><?= $shop['NAME'];?></div>
                                <div class="shop-info__adds"><?= $address;?></div>
                            </div>
                        </div>
                        <div class="shop__more-link-wrap">
                            <span  class="shop__more-link">подробнее</span>
                        </div>
                    </div>
                    <div class="shop__img-wrap">
                        <img class="shop__img" src="<?= ($shop['DETAIL_PICTURE'] ?? $shop['PREVIEW_PICTURE'])['SRC'];?>" alt="<?= $shop['NAME'];?>">
                    </div>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php

$address = '';
if (!empty($arResult['SECTION']['NAME'])){
    $address .= 'г. '.$arResult['SECTION']['NAME'];
}

if (!empty($arResult['PROPERTIES']['ADDRESS']['VALUE'])){
    $address .= ', '.$arResult['PROPERTIES']['ADDRESS']['VALUE'];
}

?>
<div class="shop-open">
    <div class="shop-open__wrap">
        <div class="shop-open__left">


            <div class="marketing-top">
                <div class="marketing__left">
                    <div class="back-btn">
                        <div class="back-btn__icon-wrap">
                            <a class="back-btn__link" href="<?= $arResult['LIST_PAGE_URL'];?>">
                                <img class="back-btn__icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/arr-back.svg" alt="back">
                            </a>
                        </div>
                    </div>
                    <div class="marketing__head">магазины</div>
                </div>
            </div>


            <div class="shop-open__content">

                <div class="shop-open__title"><?= $arResult['NAME'];?></div>
                <div class="shop-open__info shop-open__block">
                    <div class="shop-open__addss"><?= $address;?></div>
                    <?php if (!empty($arResult['PROPERTIES']['WORK_TIME']['VALUE'])): ?>
                        <div class="shop-open__time">
                            <?php foreach ($arResult['PROPERTIES']['WORK_TIME']['VALUE'] as $workTime): ?>
                                <div class="shop-open__time-element"><?= $workTime;?></div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>


                <?php if (!empty($arResult['PROPERTIES']['CARRIERS']['VALUE'])): ?>
                    <div class="shop-open__border"></div>
                    <div class="shop-open__block">
                        <div class="shop-open__head">Промозоны</div>
                        <div class="shop-open__promo">
                            <?php
                            
                            $classHL = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity(PROMOZONES_HL_BLOCK)->getDataClass();
                            $iter = $classHL::getList([
                                'filter' => [
                                    '=UF_XML_ID' => $arResult['PROPERTIES']['CARRIERS']['VALUE'],
                                ],
                                'select' => ['UF_NAME'],
                            ]);
                            while ($el = $iter->fetch()){ ?>
                                <div class="shop-open__promo-element"><?= $el['UF_NAME'];?></div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                <?php endif; ?>


                <?php

                // Сохранание в кэше
                $this->getComponent()->setResultCacheKeys(['SHOPS_GALLERY']);
                $arResult['SHOPS_GALLERY'] = [];

                if (!empty($arResult['PROPERTIES']['SHOP_SCHEMA']['VALUE'])):
                    if (!is_array($arResult['PROPERTIES']['SHOP_SCHEMA']['VALUE'])){
                        $arResult['PROPERTIES']['SHOP_SCHEMA']['VALUE'] = [$arResult['PROPERTIES']['SHOP_SCHEMA']['VALUE']];
                    }

                    $arResult['SHOPS_GALLERY'] = [];
                    $images = [];

                    foreach ($arResult['PROPERTIES']['SHOP_SCHEMA']['VALUE'] as $imgId){
                        $path = CFile::GetPath($imgId);

                        $arResult['SHOPS_GALLERY'][] = [
                            'shopId' => $arResult['ID'],
                            'title'  => $arResult['NAME'],
                            'short'  => $arResult['PROPERTIES']['SHOP_SCHEMA']['NAME'],
                            'photo'  => $path,
                        ];
                        $images[] = $path;
                    }

                    ?>
                    <div class="shop-open__border"></div>
                    <div class="shop-open__block">
                        <div class="shop-open__head">Схема магазина</div>
                        <div class="shop-open__btn">
                            <a href="javascript:;" class="btn btn_type-1 js-shop-album" data-id-shop="<?= $arResult['ID'];?>">
                                <div class="btn__icon-wrap">
                                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/view.svg" alt="" class="btn__icon">
                                </div>
                                смотреть
                            </a>
                            <a href="javascript:;" id="download-<?= $arResult['ID'];?>" class="btn btn_type-1">
                                <div class="btn__icon-wrap">
                                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/download-light.svg" alt="" class="btn__icon">
                                </div>
                                скачать
                            </a>
                            <script type="text/javascript">
                                (function () {
                                    var images = <?= json_encode($images)?>;
                                    document.getElementById("download-<?= $arResult['ID'];?>").addEventListener('click', function(){
                                        images.forEach(function(i){
                                            if (i) {
                                                window.open(i);
                                            }
                                        });
                                    })
                                })();
                            </script>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (!empty($arResult['PROPERTIES']['BRANDS']['VALUE'])): ?>
                    <div class="shop-open__border"></div>
                    <div class="shop-open__block">
                        <div class="shop-open__head">Список брендов магазина</div>
                        <div class="shop-open__btn">
                            <a href="<?= $arResult['DETAIL_PAGE_URL'];?>brands" class="btn btn_type-1">
                                <div class="btn__icon-wrap">
                                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/view.svg" alt="" class="btn__icon">
                                </div>
                                смотреть
                            </a>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>

        <?php if (!empty($arResult['MORE_PHOTO'])): ?>
            <div class="shop-open__right">
                <div class="shop-open-slide">
                    <div class="shop-open-slide__content">
                        <div class="shop-open-slide__big-img">
                            <?php if (strpos($arResult['MORE_PHOTO'][0]['CONTENT_TYPE'], 'mp4') !== false) : ?>
                                <video src="<?= $arResult['MORE_PHOTO'][0]['SRC'];?>" controls></video>
                            <?php else: ?>
                                <img src="<?= $arResult['MORE_PHOTO'][0]['SRC'];?>" alt="<?= $arResult['NAME'];?>">
                            <?php endif; ?>
                        </div>
                        <div class="shop-open-slide__small-img">
                            <div class="shop-open-slide__small-img-scroll">
                                <div class="shop-open-slide__small-img-list">
                                    <?php foreach ($arResult['MORE_PHOTO'] as $photo):?>
                                        <div class="shop-open-slide__small-img-element">
                                            <?php if (strpos($photo['CONTENT_TYPE'], 'mp4') !== false) : ?>
                                                <video src="<?= $photo['SRC'];?>" playsinline></video>
                                            <?php else: ?>
                                                <img data-image="<?= $photo['SRC'];?>" src="<?= $photo['SMALL'];?>" alt="<?= $arResult['NAME'];?>">
                                            <?php endif; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>


    </div>
</div>

<?php

if (empty($arResult['PROPERTIES']['LONGITUDE']['VALUE']) || empty($arResult['PROPERTIES']['LATITUDE']['VALUE'])){
    return;
}

$longitude = $arResult['PROPERTIES']['LONGITUDE']['VALUE'];
$latitude  = $arResult['PROPERTIES']['LATITUDE']['VALUE'];
?>
<div class="shop-on-map">
    <div class="shop-on-map__head">
        <div class="shop-on-map__head-icon">
            <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/on-map.svg" alt="">
        </div>
        <div class="shop-on-map__head-text">
            Расположение на карте
        </div>


    </div>

    <div class="">
        <div id="shop-map" class="map"></div>
        <script>
            var map;
            function initMap() {
                map = new google.maps.Map(document.getElementById('shop-map'), {
                    zoom: 16,
                    center: new google.maps.LatLng(<?= json_encode($longitude);?>, <?= json_encode($latitude);?>),
                    mapTypeId: 'roadmap',
                    disableDefaultUI: true,
                    styles: [
                        {
                            "featureType": "all",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "weight": "2.00"
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#9c9c9c"
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text",
                            "stylers": [
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#e0e0e0"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape.man_made",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 45
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#eeeeee"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#7b7b7b"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#3898bf"
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#d7bea2"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#070707"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        }
                    ]
                });

                var iconBase = <?= json_encode(SITE_TEMPLATE_PATH)?>;
                var icons = {

                    info: {
                        icon: iconBase + '/img/icons/map.svg'
                    }
                };

                var features = [
                    {
                        position: new google.maps.LatLng(<?= json_encode($longitude);?>, <?= json_encode($latitude);?>),
                        type: 'info'
                    }
                ];

                // Create markers.
                features.forEach(function(feature) {
                    var marker = new google.maps.Marker({
                        position: feature.position,
                        icon: icons[feature.type].icon,
                        map: map,

                    });
                });
            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyKuF7Gl_b9YpLeu6L3V3dEsDHhHUm4DI&callback=initMap">
        </script>
    </div>
</div>
<?php

if (!empty($arResult['VARIABLES']['SMART_FILTER_PATH'])){
    include 'inc/brands.php';
    return;
}

$section = $arResult['VARIABLES'][$arParams['SECTION_ID_VARIABLE']];

$section = CIBlockSection::GetList([], ['CODE' => $section], false, ['*', 'UF_*'])->Fetch();

$subsectionsCount = CIBlockSection::GetCount([
    'SECTION_ID' => $section['ID'],
    'ACTIVE' => 'Y'
]);

include 'sections.php';
<?php

global $APPLICATION;

if ($arResult['VARIABLES']['SMART_FILTER_PATH'] !== 'brands'){
    show404();
}

$elementCode = $arResult['VARIABLES']['ELEMENT_CODE'];

$element = CIBlockElement::GetList([], [
    '=CODE'      => $elementCode,
    '=IBLOCK_ID' => $arParams['IBLOCK_ID'],
], false, false, ['ID', 'DETAIL_PAGE_URL', 'NAME', 'PROPERTY_ADDRESS'])->GetNext();


if (!$element){
    show404();
}

$element['ADDRESS'] = $element['PROPERTY_ADDRESS_VALUE'];


$iter = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $element['ID'], "sort", "asc", array("CODE" => "BRANDS"));
$brands = [];
while ($brand = $iter->Fetch()){
    $brands[] = $brand['VALUE'];
}

$keys = [];
if (!empty($brands)){
    $iter = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity(BRANDS_HL_BLOCK)->getDataClass()::getList([
        'filter' => [
            '@UF_XML_ID' => $brands,
        ]
    ]);
    while ($brand = $iter->fetch()){

        $char = strtoupper(mb_substr($brand['UF_NAME'], 0,1));

        // 48 - 57
        if (ord($char) >= 48 && ord($char) <= 57) {
            $key = '0-9';
        } elseif (ord($char) > 122) {
            $key = 'А-Я';
        } else {
            $key = $char;
        }
        $keys[$key][] = $brand;
    }
}

?>
<div class="marketing-top">
    <div class="marketing__left">
        <div class="back-btn">
            <div class="back-btn__icon-wrap">
                <a class="back-btn__link" href="<?= $element['DETAIL_PAGE_URL'];?>">
                    <img class="back-btn__icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/arr-back.svg" alt="">
                </a>
            </div>
        </div>
        <div class="marketing__head">Список брендов <?= $element['NAME'] . (!empty($element['ADDRESS']) ? ', ' . $element['ADDRESS'] : '');?></div>
    </div>
</div>
<?php

if (empty($keys)){
    return;
}

?>
<div class="padding_bottom">
    <div class="brand-all padding_bottom">
        <div class="brand-all__wrap">
            <div>
                <?php foreach($keys as $key => $brands): ?>
                <div class="brand-all__block">
                    <div class="brand-all__char"><?= $key;?></div>
                    <div class="brand-all__list">
                        <?php foreach ($brands as $brand): ?>
                        <div class="brand-all__element"><a class="brand-all__link" href="javascript:;"><?= $brand['UF_NAME'];?></a></div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
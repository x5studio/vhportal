<?php

$APPLICATION->IncludeComponent('bitrix:catalog.element', '', [

    'IBLOCK_TYPE'  => $arParams['IBLOCK_TYPE'],
    'IBLOCK_ID'    => $arParams['IBLOCK_ID'],

    'ELEMENT_ID'   => $arResult['VARIABLES']['ELEMENT_ID'],
    'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],


    "HIDE_NOT_AVAILABLE" => $arParams['HIDE_NOT_AVAILABLE'],
    "HIDE_NOT_AVAILABLE_OFFERS" => $arParams['HIDE_NOT_AVAILABLE_OFFERS'],
    "INCLUDE_SUBSECTIONS" => $arParams['INCLUDE_SUBSECTIONS'],

    "PAGE_ELEMENT_COUNT" => $arParams['PAGE_ELEMENT_COUNT'],
    "LINE_ELEMENT_COUNT" => $arParams['LINE_ELEMENT_COUNT'],
    "ELEMENT_SORT_FIELD" => $arParams['ELEMENT_SORT_FIELD'],
    "ELEMENT_SORT_ORDER" => $arParams['ELEMENT_SORT_ORDER'],
    "ELEMENT_SORT_FIELD2" => $arParams['ELEMENT_SORT_FIELD2'],
    "ELEMENT_SORT_ORDER2" => $arParams['ELEMENT_SORT_ORDER2'],

    "DISPLAY_PROPERTIES" => [
        "STORES"
    ],

    "MESSAGE_404"    => $arParams['MESSAGE_404'],
    "SET_STATUS_404" => $arParams['SET_STATUS_404'],
    "SHOW_404"       => $arParams['SHOW_404'],

    "SHOW_ALL_WO_SECTION" => "Y",
], $this->getComponent());
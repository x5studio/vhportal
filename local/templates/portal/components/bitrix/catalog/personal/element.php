<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


$element = \Bitrix\Iblock\ElementTable::getRow([
    'select' => ['ID', 'CODE', 'NAME'],
    'filter' => [
        '=IBLOCK_ID' => $arParams['IBLOCK_ID'],
        '=CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
    ]
]);


$arResult['VARIABLES']['INVITE'] = $arResult['VARIABLES']['INVITE'] === 'index.php' ? '' : $arResult['VARIABLES']['INVITE'];


if (!$element){
    show404();
} else {
    if (
        $arResult['VARIABLES']['INVITE'] === 'invite'
        && \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->isPost()
    ){
        $arResult['ID'] = $element['ID'];

        include 'inc/invite.php';

        return;

    } elseif (!empty($arResult['VARIABLES']['INVITE'])) {
        $arResult['VARIABLES']['INVITE'] = '';
        $url = $arResult['FOLDER'] . str_replace(
                array_map(function ($i){
                    return "#{$i}#";
                }, array_keys($arResult['VARIABLES'])),
                array_values($arResult['VARIABLES']),
                $arResult['URL_TEMPLATES']['element']
            );
        LocalRedirect($url);
    }
}


$APPLICATION->IncludeComponent(
    "bitrix:catalog.element",
    "personal",
    Array(
        "IBLOCK_ID" => $arParams['IBLOCK_ID'],
        "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
        "ELEMENT_CODE" => $element["CODE"],
        "ELEMENT_ID" => $element["ID"],
    )
);
<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$section = CIBlockSection::GetList(
    [],
    ['IBLOCK_ID' => $arParams['IBLOCK_ID'], 'CODE' => $arResult['VARIABLES']['SECTION_CODE']],
    false,
    ['*', 'UF_*']
)->Fetch();

if (!$section){
    show404();
}

$params = [
    'IBLOCK_TYPE'  => $arParams['IBLOCK_TYPE'],
    'IBLOCK_ID'    => $arParams['IBLOCK_ID'],

    'SECTION_ID'   => $section['ID'],
    'SECTION_CODE' => $section['CODE'],

    'SECTION_USER_FIELDS' => ['UF_*'],

    'USE_FILTER'  => $arParams['USE_FILTER'],
    'FILTER_NAME' => $arParams['FILTER_NAME'],

    'PAGER_TEMPLATE' => $arParams['PAGER_TEMPLATE'],

    "HIDE_NOT_AVAILABLE" => $arParams['HIDE_NOT_AVAILABLE'],
    "HIDE_NOT_AVAILABLE_OFFERS" => $arParams['HIDE_NOT_AVAILABLE_OFFERS'],
    "INCLUDE_SUBSECTIONS" => $arParams['INCLUDE_SUBSECTIONS'],

    "PAGE_ELEMENT_COUNT" => $arParams['PAGE_ELEMENT_COUNT'],
    "LINE_ELEMENT_COUNT" => $arParams['LINE_ELEMENT_COUNT'],
    "ELEMENT_SORT_FIELD" => $arParams['ELEMENT_SORT_FIELD'],
    "ELEMENT_SORT_ORDER" => $arParams['ELEMENT_SORT_ORDER'],
    "ELEMENT_SORT_FIELD2" => $arParams['ELEMENT_SORT_FIELD2'],
    "ELEMENT_SORT_ORDER2" => $arParams['ELEMENT_SORT_ORDER2'],

    "DISPLAY_PROPERTIES" => [
    ],

    "PRICE_CODE" => ['BASE'],

    "MESSAGE_404"    => $arParams['MESSAGE_404'],
    "SET_STATUS_404" => $arParams['SET_STATUS_404'],
    "SHOW_404"       => $arParams['SHOW_404'],

    "SHOW_ALL_WO_SECTION" => "N",
];


$templates = [
    4 => 'personal_trainings', // Тренинги
    5 => 'personal_vacancies', // Персонал от марок
];

switch ($section['UF_TYPE']){
    case 4:
        $APPLICATION->IncludeComponent('bitrix:catalog.section', 'personal_trainings' , $params, $this);
        break;
    case 5:

        ob_start();
        $APPLICATION->IncludeComponent(
            'bitrix:catalog.smart.filter',
            'personal',
            [
                "FILTER_PROP" => "PROFESSION", // Выводить только это свойство

                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => $section['ID'],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "PRICE_CODE" => $arParams["~PRICE_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "SAVE_IN_SESSION" => "N",
                "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                "XML_EXPORT" => "N",
                "SECTION_TITLE" => "NAME",
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                "SEF_MODE" => 'N', $arParams["SEF_MODE"],
//                "SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
//                "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
            ],
            $this
        );
        $APPLICATION->AddViewContent('personal_smart_filter', ob_get_contents());
        ob_end_clean();

        $APPLICATION->IncludeComponent('bitrix:catalog.section', 'personal_vacancies' , $params, $this);
        break;

    default:
        show404();
}


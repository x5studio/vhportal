<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (false && $_SESSION['PERSONAL']['VACANCIES'][$USER->GetID()][$element['ID']] === true){
    $id = true;
} else {

    $el = new CIBlockElement();

    $iter = CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => VACANCY_INVITES_BLOCK_ID,
            'PROPERTY_VACANCY' => $arResult['ID'],
            'PROPERTY_USER' => $USER->GetID(),
            'PROPERTY_DONE' => false,
        ],
        false,
        false,
        ['ID', 'PROPERTY_INTERVIEW_DATE', 'PROPERTY_VACANCY', 'PROPERTY_USER', 'PROPERTY_DONE']
    );

    if ($invite = $iter->Fetch()){

        $el->LAST_ERROR = 'Приглашение на собеседование уже отправлено';

        $id = false;
    } else {

        $inviteName = "Приглашение на собеседование для {$element['NAME']}[{$element['ID']}] от {$USER->GetFormattedName()}[{$USER->GetID()}]";

        $id = $el->Add([
            'IBLOCK_ID' => VACANCY_INVITES_BLOCK_ID,
            'NAME'      => $inviteName,
            'PROPERTY_VALUES' => [
                'USER' => $USER->GetID(),
                'VACANCY' => $element['ID'],
            ]
        ]);
    }
}

if ($id > 0){
    // Сохранение в сессии чтобы не было дублей
    $_SESSION['PERSONAL']['VACANCIES'][$USER->GetID()][$element['ID']] = true;

    include 'invite_success.php';
} else {
    $arResult['ERROR'] = $el->LAST_ERROR;

    include 'invite_error.php';
}
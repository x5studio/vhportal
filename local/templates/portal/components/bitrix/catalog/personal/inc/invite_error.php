<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

?>
<div class="success-page">
    <div class="success-page__wrap">
        <div class="success-page__head">Произошла шибка</div>
        <div class="success-page__note"><?= $arResult['ERROR'];?></div>
        <div class="success-page__border"></div>
        <div class="success-page__return-link">
            <a href="/catalog/personal/personal-ot-marok/" class="btn btn_type-3">
                <div class="btn__icon-wrap">
                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/back-small.svg" alt="" class="btn__icon">
                </div>
                вернуться к списку резюме
            </a>
        </div>

        <div class="success-page__nav">
            <a href="/" class="btn btn_type-3">
                <div class="btn__icon-wrap">
                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/logo-small.svg" alt="" class="btn__icon">
                </div>
                на главную
            </a>

            <a href="/personal/vacancies" class="btn btn_type-3">
                <div class="btn__icon-wrap">
                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/vacancy.svg" alt="" class="btn__icon">
                </div>
                мои вакансии
            </a>
        </div>
    </div>
</div>

<?php

$APPLICATION->IncludeComponent('bitrix:catalog.section.list', 'root', [
    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "SECTION_ID" => $section["ID"],
    "SECTION_CODE" => $section["CODE"],
    "SECTION_URL" => "",
    "SECTION_USER_FIELDS" => array('UF_*'),
    "COUNT_ELEMENTS" => $arParams['SECTION_COUNT_ELEMENTS'],
    "TOP_DEPTH" => $arParams['SECTION_TOP_DEPTH'],
    "SECTION_FIELDS" => "",
    "ADD_SECTIONS_CHAIN" => "N",
    "CACHE_TYPE" => $arParams['CACHE_TYPE'],
    "CACHE_TIME" => $arParams['CACHE_TIME'],
    "CACHE_NOTES" => $arParams['CACHE_NOTES'],
    "CACHE_GROUPS" => $arParams['CACHE_GROUPS']
]);
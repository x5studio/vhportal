<?php

$section = $arResult['VARIABLES'][$arParams['SECTION_ID_VARIABLE']];

$section = CIBlockSection::GetList(
    [],
    ['CODE' => $section, 'IBLOCK_ID' => $arParams['IBLOCK_ID']],
    false,
    ['*', 'UF_*']
)->Fetch();

$subsectionsCount = CIBlockSection::GetCount([
    'SECTION_ID' => $section['ID'],
    'ACTIVE' => 'Y'
]);

if ($subsectionsCount > 2 && $section['DEPTH_LEVEL'] == 1){
    include 'sections.php';
} else {
    include 'inc/section.php';
}
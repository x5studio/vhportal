<?php

if (!empty($arResult['RULES'])){

    ob_start();
    foreach ($arResult['RULES'] as $sectionId => $rule): ?>

<div class="modal micromodal-slide modal-rules" id="<?= $rule['id'];?>" aria-hidden="false">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close="">
        <div class="modal-default">
            <div class="modal-close modal-close_callback" data-micromodal-close="">
                <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/remove.svg" alt="">
            </div>
            <div class="modal-default__content">
                <div class="rules rules__wrap js-default-wrap">
                    <?= $rule['text'];?>
                </div>
            </div>
        </div>
    </div>
</div>

    <?
    endforeach;

    $APPLICATION->AddViewContent('footer', ob_get_contents());
    ob_end_clean();

}
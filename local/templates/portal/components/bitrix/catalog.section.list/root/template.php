<?php

if (!empty($arResult['SECTION']['ID'])){
    $currentDepthLevel = $arResult['SECTION']['DEPTH_LEVEL'];
    array_unshift($arResult['SECTIONS'], $arResult['SECTION']);
} else {
    $currentDepthLevel = 1;
}

$rootSections = [];
$subsections  = [];

$arResult['RULES'] = [];

// Сохранение в кэше
$this->getComponent()->setResultCacheKeys(['RULES']);


ob_start();
foreach ($arResult['SECTIONS'] as $section){

    // Выводим только 2 уровня вложенности от уровня текущего раздела(корня)
    if ($section['DEPTH_LEVEL'] > $currentDepthLevel + 1){
        continue;
    }

    if ($section['DEPTH_LEVEL'] == $currentDepthLevel){

        if (!empty($section['UF_RULES'])){
            $rule = [
                'id' => 'rule-section-'.$section['ID'],
            ];
            $rule['js'] = "MicroModal.show('{$rule['id']}',{})";
            $arResult['RULES'][$section['ID']] = [
                'id'   => 'rule-section-'.$section['ID'],
                'text' => $section['~UF_RULES'],
            ];
        } else {
            $rule = [];
        }

        if ($section['UF_TECH_DOC'] > 0){
            $path = CFile::GetPath($section['UF_TECH_DOC']);
            $techDoc = "href=\"{$path}\" target=\"_blank\"";
        } else {
            $techDoc = false;//'href="javascript:;"';
        }
        ?>
        <div class="marketing-box">
            <div class="marketing-top">
                <div class="marketing__left">
                    <div class="marketing__head"><?= $section['NAME'];?></div>
                </div>
                <div class="marketing__right">
                    <div class="marketing__nav">
                        <?php if ($techDoc): ?>
                        <a <?= $techDoc;?> class="marketing__nav-link">
                            <div class="marketing__nav-icon-wrap">
                                <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/download.svg" class="marketing__nav-icon" alt="">
                            </div>
                            <div class="marketing__nav-hint">скачать техническое задание</div>
                        </a>
                        <?php endif; ?>
                        <a href="javascript: <?= $rule['js'];?>;" class="marketing__nav-link" data-pop-id="<?= $rule['id'];?>" data-aсtion="/">
                            <div class="marketing__nav-icon-wrap">
                                <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/rules.svg" class="marketing__nav-icon" alt="">
                            </div>
                            <div class="marketing__nav-hint">Правила оформления</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="marketing">#SUBSECTIONS#</div>
        </div>
        <?php
        $rootSections[$section['ID']] = ob_get_contents(); ob_clean();
    } else {
        ?>
        <div class="marketing__element">
            <div class="marketing__img-wrap">
                <img src="<?= $section['PICTURE']['SRC'];?>" class="marketing__img" alt="<?= $section['NAME'];?>">
            </div>
            <div class="marketing__content">
                <div class="marketing__hint">
                    <a href="<?= $section['SECTION_PAGE_URL'];?>" class="marketing__hint-link"><?= $section['NAME'];?></a>
                </div>
            </div>
        </div>
        <?php

        if (!isset($subsections[$section['IBLOCK_SECTION_ID']])){
            $subsections[$section['IBLOCK_SECTION_ID']] = '';
        }
        $subsections[$section['IBLOCK_SECTION_ID']] .= ob_get_contents(); ob_clean();
    }
}

ob_end_clean();

foreach ($rootSections as $id => $sectionHtml){
    if (empty($subsections[$id])){
        continue;
    }

    echo str_replace('#SUBSECTIONS#', $subsections[$id], $sectionHtml);
}
<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult['SECTIONS'])){
    return;
}

?>
<div class="personal">
    <div class="personal__wrap">
        <?php
        $classes = [
            4 => 'personal__element_trenings',
            5 => 'personal__element_personal',
        ];

        foreach ($arResult['SECTIONS'] as $section):
            $class = $classes[$section['UF_TYPE']];

            $section['NAME'] = str_replace_limit(' ', '<br>', $section['NAME']);
            ?>
        <div class="personal__element <?= $class;?>">
            <div class="personal__content">
                <div class="personal__head"><?= $section['NAME'];?></div>
                <div class="personal__link-wrap">
                    <a href="<?= $section['SECTION_PAGE_URL'];?>" class="personal__link">подробнее</a>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
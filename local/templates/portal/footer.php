<?php

global $USER, $APPLICATION;

$portalParams = \VH\Portal\Helper::getInstance();

?>

<?php if (!$USER->IsAuthorized() && NEED_AUTH === true): ?>
    </div>
    </div>
<?php endif; ?>

</main><!-- .content -->

</div><!-- .wrapper -->

<?php if ($USER->IsAuthorized() || NEED_AUTH === false): ?>

    <footer class="footer">
        <div class="footer__center">
            <div class="footer__left">
                <div class="footer__logo-wrap">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/logo.svg" alt="" class="footer__logo">
                </div>

                <div class="footer__menu">
                    <div class="menu menu_footer">
                        <?php
                        // Каталог
                        $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "catalog",
                            array(
                                "ROOT_MENU_TYPE" => "left",
                                "MAX_LEVEL" => "3",
                                "IS_FOOTER" => "Y",
                                "CHILD_MENU_TYPE" => "left",
                                "USE_EXT" => "Y",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "N",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "MENU_CACHE_GET_VARS" => array(),
                                "COMPONENT_TEMPLATE" => "catalog"
                            ),
                            false
                        );
                        ?>
                        </div>
                    </div>

                </div>

                <div class="footer__right">

                    <?php
                    $APPLICATION->IncludeComponent("bitrix:menu","user", [
                        "ROOT_MENU_TYPE" => "user",
                        "POSITION" => "bottom",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "user",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "Y",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => ""
                    ]);/*
                    ?>

                    <div class="menu menu_footer-right">
                        <div class="menu__element">
                            <div class="menu__child">
                                <div class="menu__child-wrap">
                                    <div class="menu__child-list">
                                        <div class="menu__child-element"><a href="" class="menu__child-link">финансы</a></div>
                                        <div class="menu__child-element"><a href="" class="menu__child-link">отчеты</a></div>
                                        <div class="menu__child-element"><a href="" class="menu__child-link">рейтинги</a></div>
                                    </div>
                                </div>
                                <div class="menu__child-wrap">
                                    <div class="menu__child-list">
                                        <div class="menu__child-element"><a href="" class="menu__child-link">Общая информация</a></div>
                                        <div class="menu__child-element"><a href="" class="menu__child-link">Мои заявки</a></div>
                                        <div class="menu__child-element"><a href="" class="menu__child-link">Возврат</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    */
                    ?>

                    <div class="footer__right-bottom">
                        <div class="times"><?= $portalParams->getWorkTime(); ?></div>
                        <div class="phone js-modal-callback"><?= $portalParams->getPhone(); ?></div>
                    </div>
                </div>
        </div>
    </footer>
<?php endif; ?>

        <script
                src="https://code.jquery.com/jquery-3.3.1.min.js"
                integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script src="<?= SITE_TEMPLATE_PATH;?>/js/modal/micromodal.min.js"></script>
        <script src="<?= SITE_TEMPLATE_PATH;?>/js/app.js?hash=<?= md5_file($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/js/app.js'); ?>"></script>

        <script src="//sachinchoolur.github.io/lightGallery/static/js/prettify.js"></script>
        <script src="//sachinchoolur.github.io/lightGallery/static/js/transition.js"></script>
        <script src="//sachinchoolur.github.io/lightGallery/static/js/collapse.js"></script>
        <script src="//unpkg.com/lightgallery@1.10.0/dist/js/lightgallery.min.js"></script>
        <script src="//unpkg.com/lightgallery@1.10.0/modules/lg-video.min.js"></script>

        <script src="<?= SITE_TEMPLATE_PATH;?>/js/datepicker/jquery.daterangepicker.min.js"></script>

        <script type="text/javascript">
            let portalGallery = <?= json_encode( $GLOBALS['SHOPS_GALLERY'] ?? []);?>;
        </script>

        <script type="text/javascript">
            let visageData = {
                shopsGallery: typeof portalGallery === 'undefined' ? [] : portalGallery
            }
        </script>

        <div class="modal micromodal-slide modal-rules" id="modal-main"></div>
        <div class="modal micromodal-slide modal-rules" id="modal-rules" aria-hidden="false">
            <div class="modal__overlay" tabindex="-1" data-micromodal-close="">
                <div class="modal-default">
                    <div class="modal-close modal-close_callback" data-micromodal-close="">
                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/remove.svg" alt="">
                    </div>
                    <div class="modal-default__content">
                        <div class="rules js-default-wrap">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $APPLICATION->ShowViewContent('footer');?>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(57337759, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/57337759" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


    </body>
</html>
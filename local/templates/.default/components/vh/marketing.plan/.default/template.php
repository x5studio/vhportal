<?php

$currentYearCompletion = getYearCompletion() . '%';

$monthsNames = getMonthsNames();

$isOrderPage = $arResult['IS_ORDER_PAGE'];


$cardTypeClasses = [
    'CANCELED'  => 'plan-card_type-1',
    'PROCESS'   => 'plan-card_type-4',
    'COMPLETED' => 'plan-card_type-5',
    'CONFIRMED' => 'plan-card_type-2',
];

$statusText  = [
    'CANCELED'  => 'отменено',
    'PROCESS'   => 'рассматривается',
    'COMPLETED' => 'выполнено',
    'CONFIRMED' => 'подтверждено',
];


$datesByMonth = $arResult['ITEMS_BY_MONTH'];

if ($arParams['ADMIN_MODE'] === 'Y'): ?>
<style type="text/css">
    .filter-block {
        margin: 4px;
        padding: 12px 8px 0;
        border: 1px solid #ccc;
    }
    .filter-block__item {
        display: block;
        padding: 0 0 11px;
    }
    .order-hided {
        display: none !important;
    }
    .order-hided-cancel {
        display: none !important;
    }
</style>
<div class="filter-block">
    <label id="hide-show-all-orders" class="filter-block__item">
        <input onclick="hideOtherOrders(<?= $arParams['ORDER_ID'];?>, this)" type="checkbox" data-state="show">
        Скрыть все кроме текущего заказа
    </label>
    <label class="filter-block__item">
        <input onclick="hideCancelOrders(this.checked)" type="checkbox">
        Скрыть отмены
    </label>
</div>
<?php endif; ?>
<div class="plan padding_bottom center-content" id="<?= $arParams['DIV_ID'];?>"><?php/*
<div class="plan__head">moschino</div>*/?>
    <div class="plan__wrap">
        <div class="plan__time" style="width: <?= $currentYearCompletion;?>"></div>
        <div class="plan-table">

            <?php
            foreach ($monthsNames as $i => $monthName):
                if ($arParams['HIDE_EMPTY_MONTH'] === 'Y' && empty($datesByMonth[$i])){
                    continue;
                }
                ?>
                <div class="plan-table__col">
                    <div class="plan-table__th"><?= $monthName;?></div>
                    <div class="plan-table__content">
                        <?php

                        foreach ($datesByMonth[$i] as $extItemData){
                            $basketItem = $arResult['BASKET'][$extItemData['BASKET_ID']];

                            $brandName = '';
                            if (!empty($extItemData['BRAND'])){
                                $brandName = $extItemData['BRAND'];
                            } else {
                                foreach ($basketItem['PROPS'] as $propValue){
                                    if ($propValue['CODE'] !== 'BRAND'){
                                        continue;
                                    }
                                    $brandName = $propValue['VALUE'];
                                }
                            }

                            $period = $extItemData['DATE_FROM'] == $extItemData['DATE_TO'] ? $extItemData['DATE_TO'] : "{$extItemData['DATE_FROM']} - {$extItemData['DATE_TO']}";

                            $price = $extItemData['PRICE'] > 0 ? CurrencyFormat($extItemData['PRICE'] * $extItemData['QUANTITY'], BASE_CURRENCY) : '';
                            //$price  = $basketItem['PRICE'] ? $basketItem['FORMATED_SUM'] : '';

                            $discount = $extItemData['DISCOUNT'] > 0 ? $extItemData['DISCOUNT'] . '%' : '';

                            $extItemData['STATUS'] = $arResult['CANCELED'] === 'Y' ? 'CANCELED' : (time() > strtotime($extItemData['DATE_TO']) ? 'COMPLETED' : $extItemData['STATUS']);

                            $cardTypeClass = $cardTypeClasses[$extItemData['STATUS']];

                            $showCancelButton = $arParams['NOT_CHECK_CANCEL'] === 'Y' || $extItemData['STATUS'] === 'PROCESS';

                            ?>
                            <div class="plan-table__row order" id="order-<?= $extItemData['ORDER_ID']; ?>"
                                 data-status="<?= $extItemData['STATUS']; ?>">
                                <div class="plan-card <?= $cardTypeClass; ?>">
                                    <div class="plan-card__content">
                                        <div class="plan-card__type"><?= $extItemData['SECTION_NAME'];?></div>
                                        <div class="plan-card__date"><?php
                                            $shop = $arResult['SHOPS'][$extItemData['SHOP_ID']];
                                            if ($shop) {
                                                echo $shop['CODE'];
                                                if ($shop['CITY']) {
                                                    echo ', ' . $shop['CITY'];
                                                }
                                            }
                                            ?></div>
                                        <div class="plan-card__name"><?= $extItemData['NAME']; ?></div>
                                        <div class="plan-card__name"><?= $brandName; ?></div>
                                        <div class="plan-card__date"><?= $period; ?></div>
                                        <div class="plan-card__price mt_40"
                                             style="text-transform:uppercase;margin-bottom:5px"><?= $price ?: $discount; ?></div>
                                        <div class="plan-card__name ext-item-status"><?= $statusText[$extItemData['STATUS']]; ?></div>
                                        <?php
                                        if ($arParams['ADMIN_MODE'] === 'Y' && $extItemData['STATUS'] !== 'COMPLETED'):
                                            if ($arParams['ORDER_ID'] == $extItemData['ORDER_ID']): ?>
                                                <div class="plan-card__type" style="color: red; font-weight: bold">
                                                    Текущий заказ
                                                </div>
                                            <?php else: ?>
                                                <a href="<?= $APPLICATION->GetCurPage() . '?ID=' . $extItemData['ORDER_ID']; ?>"
                                                   class="plan-remove" data-ext-item-id="<?= $extItemData['ID']; ?>"
                                                   style="margin-bottom:5px"
                                                >
                                                    <br>
                                                    <div class="plan-remove__icon-wrap">
                                                        <img src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/view-dark.svg"
                                                             alt="" class="plan-remove__icon">
                                                    </div>
                                                    Перейти к заказу
                                                </a>
                                            <?php endif; ?>
                                            <a href="javascript:;"
                                                <?php if ($extItemData['STATUS'] === 'CANCELED') echo 'style="display: none"'; ?>
                                               class="plan-remove cancel-request-basket-item" data-ext-item-id="<?= $extItemData['ID'];?>"
                                            >
                                                <br>
                                                <div class="plan-remove__icon-wrap">
                                                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/remove-plan.svg" alt="" class="plan-remove__icon">
                                                </div>
                                                Отменить
                                            </a>
                                            <a href="javascript:;"
                                                <?php if ($extItemData['STATUS'] === 'CONFIRMED') echo 'style="display: none"'; ?>
                                               class="plan-remove confirm-request-basket-item" data-ext-item-id="<?= $extItemData['ID'];?>"
                                            >
                                                <br>
                                                <div class="plan-remove__icon-wrap">
                                                    <img style="width: 83%;" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/check.svg" alt="" class="plan-remove__icon">
                                                </div>
                                                Подтвердить
                                            </a>
                                        <?php elseif ($showCancelButton): ?>
                                            <a href="javascript:;"
                                               class="plan-remove cancel-by-user-request-basket-item" data-ext-item-id="<?= $extItemData['ID'];?>">
                                                <div class="plan-remove__icon-wrap">
                                                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/remove-plan.svg" alt="" class="plan-remove__icon">
                                                </div>
                                                заявка на отмену
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                    <?php if ($arParams['ADMIN_MODE'] !== 'Y'): ?>
                                    <div class="plan-card__img-wrap">
                                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/plan-1.svg" class="plan-card__img" alt="<?= $basketItem['NAME'];?>">
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>

</div>
<?php if (true): ?>
    <script type="text/javascript">

        <?php if ($arParams['ADMIN_MODE'] === 'Y') : ?>
            function  hideOtherOrders(orderId, el){
                document.querySelectorAll(
                    '.plan-table .plan-table__row.order:not(#order-' + orderId + ')'
                ).forEach(function (i) {
                    i.classList.toggle('order-hided');
                });
            }
            document.querySelector('#hide-show-all-orders input').click();

            function  hideCancelOrders(){
                document.querySelectorAll('.plan-table .plan-table__row.order[data-status=CANCELED]').forEach(function (i) {
                    i.classList.toggle('order-hided-cancel');
                });
            }
        <?php endif; ?>

        (function(){
            let url = '/local/ajax/order/?action=item_status';

            let isOrderPage = <?= json_encode($isOrderPage);?>;

            let statusesText = <?= json_encode($statusText)?>;
            let statusesClasses = <?= json_encode($cardTypeClasses)?>;

            let statusConfirm = '<?= ExtBasketItemTable::STATUS_CONFIRMED;?>';
            let statusCancel = '<?= ExtBasketItemTable::STATUS_CANCELED;?>';

            function doAction(type, el, after){

                let types2mode = {
                    "confirm": "admin",
                    "cancel": "admin",
                    "cancelByUser": "request"
                };
                if (!types2mode[type]) {
                    return;
                }

                BX.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        type: type,
                        id:   el.dataset.extItemId,
                        mode: types2mode[type]
                    },
                    dataType: 'json',
                    timeout: 3,
                    async: true,
                    processData: true,
                    start: true,
                    onsuccess: function(data){
                        if (data.success){
                            if (typeof(after) === 'function'){
                                after();
                            }
                            if (isOrderPage && typeof(data.need_refresh) !== 'undefined' && data.need_refresh) {
                                window.location.href += '&sale_order_view_active_tab=marketingPlan_marketingPlan';
                            }
                            let p = el.closest('.plan-card');
                            p.className = 'plan-card ' + statusesClasses[data.status];
                            p.querySelector('.ext-item-status').textContent = statusesText[data.status];

                            p.querySelector('.cancel-request-basket-item').style.display = data.status === statusCancel ? 'none' : '';
                            p.querySelector('.confirm-request-basket-item').style.display = data.status === statusConfirm ? 'none' : '';
                        }
                    },
                    onfailure: console.log
                });
            }
            <?php if ($arParams['ADMIN_MODE'] === 'Y'): ?>
                function cancel(el){
                    doAction('cancel', el, function () {
                        alert('Заявка отменена');
                    });
                }

                function confirm(el){
                    doAction('confirm', el, function () {
                        alert('Заявка подтверждена');
                    });
                }

                document.querySelectorAll('#<?= $arParams['DIV_ID'];?> .cancel-request-basket-item').forEach(function (i) {
                    i.addEventListener('click', function (e) {
                        let el = e.currentTarget ? e.currentTarget : e.target;
                        cancel(el);
                    });
                });

                document.querySelectorAll('#<?= $arParams['DIV_ID'];?> .confirm-request-basket-item').forEach(function (i) {
                    i.addEventListener('click', function (e) {
                        let el = e.currentTarget ? e.currentTarget : e.target;
                        confirm(el);
                    });
                });
            <?php endif; ?>

            function cancelByUser(el){
                doAction('cancelByUser', el, function () {
                    alert('Заявка отменена');
                });
            }

            document.querySelectorAll('#<?= $arParams['DIV_ID'];?> .cancel-by-user-request-basket-item').forEach(function (i) {
                console.log(i);
                i.addEventListener('click', function (e) {
                    let el = e.currentTarget ? e.currentTarget : e.target;
                    cancelByUser(el);
                });
            });

        })();
    </script>
<?php endif; ?>
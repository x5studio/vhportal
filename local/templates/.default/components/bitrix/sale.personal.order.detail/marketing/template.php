<div class="marketing-top">
    <div class="marketing__left">
        <div class="back-btn">
            <div class="back-btn__icon-wrap">
                <a class="back-btn__link" href="<?= $arResult['URL_TO_LIST'];?>">
                    <img class="back-btn__icon" src="<?= SITE_TEMPLATE_PATH;?>/img/icons/arr-back.svg" alt="">
                </a>
            </div>
        </div>
        <div class="marketing__head">Заявка №<?= $arResult['ID'];?> от <?= $arResult['DATE_INSERT_FORMATED'];?></div>
    </div>
    <?php/*
    <div class="marketing__right">
        <div class="office-head__btn">
            <a href="" class="btn btn_type-1">
                <div class="btn__icon-wrap">
                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/download-light.svg" alt="" class="btn__icon">
                </div>
                скачать excel
            </a>
        </div>
    </div>
    */?>
</div>
<?php

$APPLICATION->IncludeComponent('vh:marketing.plan', '.default', [
    'ORDER_ID' => $arResult['ID'],
    'ADMIN_MODE' => 'N',
    'SHOW_ALL' => 'N',

    'HIDE_EMPTY_MONTH' => 'Y',
]);


?>
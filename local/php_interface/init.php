<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");
// Пользование сайтом только для авторизованных пользователей
define('NEED_AUTH', true);

//
function p($d){
    echo "<pre>".print_r($d, true)."</pre>";
}

trait SingletonTrait {
    protected static $instance;
    final public static function getInstance(){
        return isset(static::$instance)
            ? static::$instance
            : static::$instance = new static;
    }

    final private function __construct() {
        $this->init();
    }

    protected function init() {}
    final private function __wakeup() {}
    final private function __clone() {}
}

include_once 'classes/portal.php';

CModule::IncludeModule('iblock');
CModule::IncludeModule('highloadblock');
CModule::IncludeModule('catalog');
CModule::IncludeModule('sale');

// Константы
define('CATALOG_MARKETING', 1);
define('CATALOG_PERSONAL', 2);
define('CATALOG_SHOPS', 3);

define('CATALOG_VENDOR_ITEMS_ID', 7);

define('VACANCY_INVITES_BLOCK_ID', 6);

define('OFFERS_CATALOG', 4);

define('BRANDS_HL_BLOCK', 1);
define('PROMOZONES_HL_BLOCK', 3);
define('VENDORS_HL_BLOCK', 4);

define('BASE_PRICE', 1);
define('BASE_CURRENCY', 'RUB');

//=================================
// Подключение обработчиков событий
include_once 'events/main.php';
//=================================

include_once 'helpers.php';

include_once 'classes/basket_item_ext.php';

include_once 'classes/import_helper.php';

include_once 'classes/stocks.php';

//=================================
// VH Reports
include_once 'classes/reports/reports.php';
include_once 'classes/reports/sale.php';
include_once 'classes/reports/shop.php';
include_once 'classes/reports/group.php';
include_once 'classes/reports/stock.php';
include_once 'classes/reports/store.php';
include_once 'classes/reports/rating.php';
include_once 'classes/reports/barcode.php';
include_once 'classes/reports/product.php';
include_once 'classes/reports/product_type.php';
// VH Entities
include_once 'classes/reports/entities/entity.php';
include_once 'classes/reports/entities/main.php';

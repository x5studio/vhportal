<?php
use Bitrix\Main\Diag\Debug;


final class ImportHelper {
    const LOG_FILE = 'local/logs/import.log';

    // Catalog IDs
    protected static $iblockId = CATALOG_VENDOR_ITEMS_ID;

    // ID brands highloadblock
    protected static $brandsId  = BRANDS_HL_BLOCK;
    protected static $vendorsId = VENDORS_HL_BLOCK;

    // props
    protected static $articleProp = 'ARTNUMBER';
    protected static $brandsProp  = 'BRAND_REF';
    protected static $vendorProp  = 'VENDOR_REF';

    // cache
    protected static $directoryCache  = [];
    protected static $sectionsCache   = [];
    protected static $priceTypesCache = [];
    protected static $storageCache    = [];
    protected static $itemsCache      = [];


    public static function commit(int $timestamp){
        $iblocks = self::$iblockId.', '.self::$offersIblockId;
        $f = true;
        try {
            $sql = <<<SQL
UPDATE
    b_catalog_product p
    INNER JOIN b_iblock_element el
        ON p.ID = el.ID
	LEFT JOIN b_catalog_store_product s
        ON p.ID = s.PRODUCT_ID
SET p.AVAILABLE='N', p.QUANTITY = 0, s.AMOUNT = 0, p.TIMESTAMP_X = p.TIMESTAMP_X, el.TIMESTAMP_X = p.TIMESTAMP_X
WHERE p.TIMESTAMP_X < FROM_UNIXTIME({$timestamp}) AND el.IBLOCK_ID IN ({$iblocks})
SQL;
            self::getConnection()->query($sql);

            self::clearIblockCache();
        } catch (\Throwable $e){
            $f = false;
        }
        return $f;
    }

    public static function clearIblockCache(){
        // Очистка кэша инфоблоков
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag('iblock_id_'.self::$iblockId);
        $CACHE_MANAGER->ClearByTag('iblock_id_'.self::$offersIblockId);
    }

    protected static function getConnection(){
        static $conn = null;
        if ($conn === null){
            $conn = \Bitrix\Main\Application::getConnection();
        }
        return $conn;
    }

    protected static function getPrices($itemId){
        static $cache = [];
        if (empty($cache)){
            $iter = \Bitrix\Catalog\PriceTable::getList([
                'select' => ['ID', 'PRODUCT_ID', 'PRICE', 'PRICE_ID' => 'CATALOG_GROUP_ID']
            ]);
            while ($price = $iter->fetch()){
                $cache[$price['PRODUCT_ID']][$price['PRICE_ID']] = [$price['ID'], $price['PRICE']];
            }
        }
        return $cache[$itemId];
    }

    protected static function prices($itemId, $v){
        $prices = self::getPrices($itemId);

        foreach ($v as $price){
            $price = (array)$price;
            $priceXmlId = key($price);
            $price = $price[$priceXmlId];

            $priceId = self::getPriceId($priceXmlId);

            if (isset($prices[$priceId])){
                if ($prices[$priceId][1] == $price) continue;
                \Bitrix\Catalog\PriceTable::update($prices[$priceId][0], [
                    'PRICE' => $price,
                    'PRICE_SCALE' => $price,
                ]);
            } else {
                \Bitrix\Catalog\PriceTable::add([
                    'CATALOG_GROUP_ID'  => $priceId,
                    'PRODUCT_ID'        => $itemId,
                    'PRICE'             => $price,
                    'PRICE_SCALE'       => $price,
                    'CURRENCY'          => 'RUB',
                ]);
            }
        }
    }

    protected static function getStocks($itemId){
        static $cache = [];
        if (empty($cache)){
            $iter = \Bitrix\Sale\StoreProductTable::getList([
                'select' => ['ID', 'PRODUCT_ID', 'AMOUNT', 'STORE_ID']
            ]);
            while ($amount = $iter->fetch()){
                $cache[$amount['PRODUCT_ID']][$amount['STORE_ID']] = [$amount['ID'], $amount['AMOUNT']];
            }
        }
        return $cache[$itemId];
    }

    protected static function stocksOld($itemId, $v){
        $qty = 0;
        $stocks = self::getStocks($itemId);

        foreach ($v as $amount){
            $amount = (array)$amount;
            $storeXmlId = key($amount);
            $amount = $amount[$storeXmlId];

            $storeId = self::getStorageId($storeXmlId);

            if (empty($storeId)){
                continue;
            }

            if (isset($stocks[$storeId])){
                if ($stocks[$storeId][1] != $amount) {
                    \Bitrix\Sale\StoreProductTable::update($stocks[$storeId][0], [
                        'AMOUNT' => $amount,
                    ]);
                }
            } else {
                \Bitrix\Sale\StoreProductTable::add([
                    'STORE_ID'   => $storeId,
                    'PRODUCT_ID' => $itemId,
                    'AMOUNT'     => $amount,
                ]);
            }
            $qty += $amount;
            unset($stocks[$storeId]);
        }

        // обновление доступности и общего остатка

        /**
         * не использовать, долго выполняется
         */
        /*
        \Bitrix\Catalog\Model\Product::update($itemId, [
            'QUANTITY' => $qty,
            'AVAILABLE' => $qty > 0 ? 'Y' : 'N'
        ]);
        */

        self::updateProductAvailable($itemId, $qty, $qty > 0 ? 'Y' : 'N');

        $linkId = 0;//self::getLinkItem($itemId);
        static $cache = [];

        if ($linkId > 0 && !$cache[$linkId]){
            $cache[$linkId] = true;
            // Обновляем доступность родительского товара при наличии
            self::updateProductAvailable($linkId, 0, $qty > 0 ? 'Y' : 'N');
        }

        if (empty($stocks)){
            return;
        }
        // Склады для которых не поступило остатков
        foreach ($stocks as $storeId => $amount){
            \Bitrix\Sale\StoreProductTable::update($stocks[$storeId][0], [
                'AMOUNT' => 0,
            ]);
        }
    }

    protected static function updateProductAvailable($itemId, $quantity, $available, $type = ''){
        $table = \Bitrix\Catalog\ProductTable::getTableName();
        $sql = "update {$table} set TIMESTAMP_X = CURRENT_TIMESTAMP, QUANTITY = {$quantity}, AVAILABLE = '{$available}' #TYPE# WHERE ID = {$itemId}";
        if (!empty($type)){
            $type = ', TYPE = '.$type;
        } else {
            $type = '';
        }
        $sql = str_replace('#TYPE#', $type, $sql);

        self::getConnection()->query($sql);

        /**
         * Не использовать, долго выполняется
         */
        /*
        \Bitrix\Catalog\ProductTable::update($itemId, [
            'QUANTITY'  => 0,
            'AVAILABLE' => $qty > 0 ? 'Y' : 'N',
            // 'TYPE'      => $type,
            'TIMESTAMP_X' => \Bitrix\Main\Type\DateTime::createFromTimestamp(time()),
        ]);
        */
    }

    public static function pricesAndStocks($data){
        if (empty($data)){
            return;
        }

        foreach ($data as $v){
            $itemXmlId = !empty($v->trade_offer)
                ? $v->trade_offer
                : $v->goods;
            $itemId = self::getProductId($itemXmlId);

            if (empty($itemId)){
                continue;
            }
            if (!empty($v->price)){
                self::prices($itemId, $v->price);
            }

            if (!empty($v->stock)){
                self::stocks($itemId, $v->stock);
            }
        }
    }

    public static function getPriceId(string $xmlId){
        if (empty(self::$priceTypesCache)){
            self::$priceTypesCache = [];

            $iter = \Bitrix\Catalog\GroupTable::getList([
                'select' => ['ID', 'XML_ID']
            ]);
            while ($type = $iter->fetch()){
                self::$priceTypesCache[$type['XML_ID']] = $type['ID'];
            }
        }
        return self::$priceTypesCache[$xmlId];
    }

    public static function setPriceId(string $xmlId, $id){
        self::$priceTypesCache[$xmlId] = $id;
    }

    public static function priceTypes($data){
        if (empty($data) || empty($data->values)){
            return;
        }

        $head = array_flip($data->head);
        foreach ($data->values as $type){
            $priceId = self::getPriceId($type[$head['id']]);
            if (empty($priceId)){
                $res = \Bitrix\Catalog\GroupTable::add([
                    'XML_ID' => $type[$head['id']],
                    'NAME'   => $type[$head['name']],
                ]);
                if ($res->isSuccess()){
                    self::setStorageId($type[$head['id']], $res->getId());
                }
            }
        }
    }

    public static function date($date){
        if (empty($date)){
            return;
        }
        $date = new \Bitrix\Main\Type\Date($date);

        $dateRow = \VH\Portal\ReportDatesTable::getRow([
            'filter' => [
                '=DATE' => $date,
            ]
        ]);

        if ($dateRow){
            \VH\Portal\ReportDatesTable::update([
                'DATE' => $dateRow['DATE'],
            ], [
                'UPDATED' => new \Bitrix\Main\Type\DateTime(),
            ]);
        } else {
            \VH\Portal\ReportDatesTable::add([
                'DATE' => $date,
            ]);
        }
    }

    /**
     * Импорт данных по остаткам на складах
     * @param string $date
     * @param $data
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function stocks(string $date, $data){
        $exists = [];
        $iter = VH\Portal\StocksTable::getList([
            'select' => [
                'DATE', 'PRODUCT_ID', 'STORAGE_ID', 'AMOUNT'
            ],
            'filter' => [
                '=DATE' => new \Bitrix\Main\Type\Date($date),
            ]
        ]);
        while ($i = $iter->fetch()){
            $exists[$i['STORAGE_ID']][$i['PRODUCT_ID']] = [
                $i['ID'],
                $i['AMOUNT']
            ];
        }

        $head = array_flip($data->head);
        foreach ($data->values as $v){
            $storageId = self::getStorageId($v[$head['storage']]);
            $itemId = self::getProductId($v[$head['item']]);

            if (!$storageId || !$itemId){
                continue;
            }

            if (isset($exists[$storageId][$itemId])){
                if ($v[$head['count']] == $exists[$storageId][$itemId][1]){
                    continue;
                }
                VH\Portal\StocksTable::update($exists[$storageId][$itemId][0], [
                    'AMOUNT' => $v[$head['count']],
                ]);
            } else {
                VH\Portal\StocksTable::add([
                    'DATE' => $date,
                    'PRODUCT_ID' => $itemId,
                    'STORAGE_ID' => $storageId,
                    'AMOUNT' => $v[$head['count']],
                ]);
            }
        }
    }

    /**
     * Импорт складов
     * @param $data
     */
    public static function storage($data){
        if (empty($data) || empty($data->values)){
            return;
        }

        $head = array_flip($data->head);
        foreach ($data->values as $store) {
            $storeId = self::getStorageId($store[$head['id']]);
            if (empty($storeId)){
                try {
                    $res = \Bitrix\Catalog\StoreTable::add([
                        'XML_ID'    => $store[$head['id']],
                        'TITLE'     => $store[$head['name']],
                        'ADDRESS'   => $store[$head['name']],
                    ]);
                    if ($res->isSuccess()){
                        self::setStorageId($store[$head['id']], $res->getId());
                    }
                } catch (\Throwable $e){;}
            }
        }
    }

    public static function getStorageId(string $xmlId){
        if (empty(self::$storageCache)){
            self::$storageCache = [];

            $iter = \Bitrix\Catalog\StoreTable::getList([
                'select' => ['ID', 'XML_ID']
            ]);
            while ($store = $iter->fetch()){
                self::$storageCache[$store['XML_ID']] = $store['ID'];
            }
        }
        return self::$storageCache[$xmlId];
    }

    public static function setStorageId(string $xmlId, $id){
        self::$storageCache[$xmlId] = $id;
    }


    /**
     * Импорт штрихкодов товаров
     * @param $data
     */
    public static function barCodes($data){
        if (empty($data) || empty($data->values)){
            return;
        }
        $head = array_flip($data->head);
        foreach ($data->values as $v){
            $barcode = $v[$head['barcode']];
            $itemId  = self::getProductId($v[$head['id']]);

            if (empty($itemId) || empty($barcode) || self::barCodeExists($barcode)){
                continue;
            }

            try {
                \Bitrix\Catalog\StoreBarcodeTable::add([
                    'PRODUCT_ID' => $itemId,
                    'BARCODE'    => $barcode,
                ]);
            } catch (\Throwable $e){;}
        }
    }

    protected static function barCodeExists(string $barcode){
        static $cache = [];
        if (empty($cache)){
            $iter = \Bitrix\Catalog\StoreBarcodeTable::getList([
                'select' => ['BARCODE']
            ]);
            while ($i = $iter->fetch()){
                $cache[$i['BARCODE']] = true;
            }
        }
        return $cache[$barcode] ?? false;
    }


    /**
     * Работа с товарами
     * @param $products
     */
    public static function items($products){

        if (empty($products) && empty($products->values)){
            return;
        }

        /**
         * Включение отложенного обновления фасетного индекса
         */
        \Bitrix\Iblock\PropertyIndex\Manager::enableDeferredIndexing();

        $el = new CIBlockElement();

        $head = array_flip($products->head);
        foreach ($products->values as $i => $item) {

            $xmlId = $item[$head['id']];

            $fields = [
                'IBLOCK_ID'         => self::$iblockId,
                'IBLOCK_SECTION_ID' => self::getSectionId($item[$head['type']]),

                'XML_ID' => $xmlId,
                'NAME'   => $item[$head['name']],
            ];

            $id = self::getProductId($xmlId);
            if ($id > 0){
                continue;
                //$f = $el->Update($id, $fields) !== false;
            } else {
                $rId = $el->Add($fields);

                if ($rId > 0){
                    self::setProductId($xmlId, $rId);
                    $f = true;
                } else {
                    $f = false;
                }
            }

            if ($f) {

                $propsValues = [
                    self::$articleProp => $item[$head['article']],
                    self::$brandsProp  => $item[$head['brand']],
                    self::$vendorProp  => $item[$head['vendor']],
                ];

                CIBlockElement::SetPropertyValuesEx(self::$itemsCache[$xmlId], $fields['IBLOCK_ID'], $propsValues);
            }
        }

        /**
         * Включение отложенного обновления фасетного индекса
         */
        \Bitrix\Iblock\PropertyIndex\Manager::disableDeferredIndexing();

    }

    protected static function setProductId(string $xmlId, $id){
        self::$itemsCache[$xmlId] = $id;
    }

    protected static function getProductId(string $xmlId){
        if (empty(self::$itemsCache)){
            self::$itemsCache = [];
            $iter = \Bitrix\Iblock\ElementTable::getList([
                'select' => ['XML_ID', 'ID'],
                'filter' => ['=IBLOCK_ID' => self::$iblockId],
            ]);
            while ($item = $iter->fetch()){
                self::$itemsCache[$item['XML_ID']] = $item['ID'];
            }
        }
        return self::$itemsCache[$xmlId];
    }


    /**
     * Работа с разделами
     * @param $itemTypes
     */
    public static function sections($itemTypes){
    if (
        empty($itemTypes)
        || empty($itemTypes->values)
        || empty($itemTypes->head)
    ){
        return;
    }

    $head = array_flip($itemTypes->head);

    if (isset($head['Ссылка'])){
        $head['id'] = $head['Ссылка'];
        unset($head['Ссылка']);
        $head['name'] = $head['Наименование'];
        unset($head['Наименование']);
    }

    $sObj = new CIBlockSection();
    foreach ($itemTypes->values as $section){
        $id = self::getSectionId($section[$head['id']]);

        if ($id > 0){
            continue;
        }

        $fields = [
            'NAME'              => $section[$head['name']],
            'XML_ID'            => $section[$head['id']],
            'IBLOCK_ID'         => self::$iblockId,
            'ACTIVE'            => 'Y',
        ];

        $sectionId = $sObj->Add($fields);

        self::setSectionId($section[$head['id']], $sectionId);
    }
}

    public static function setSectionId(string $xmlId, $id){
        self::$sectionsCache[$xmlId] = $id;
    }

    public static function getSectionId(string $xmlId){
        if (empty(self::$sectionsCache)){
            self::$sectionsCache = [];
            $iter = \Bitrix\Iblock\SectionTable::getList([
                'filter' => ['IBLOCK_ID' => self::$iblockId],
                'select' => ['ID', 'XML_ID'],
                'order'  => ['DEPTH_LEVEL' => 'DESC', 'SORT' => 'ASC']
            ]);
            while ($section = $iter->fetch()){
                if (empty($section['XML_ID'])){
                    continue;
                }
                self::$sectionsCache[$section['XML_ID']] = $section['ID'];
            }
        }
        return self::$sectionsCache[$xmlId] ?? false;
    }


    /**
     * Бренды
     * @param $data
     */
    public static function brands($data){
        self::directory(self::$brandsId, $data);
    }


    /**
     * Поставщики
     * @param $data
     */
    public static function vendors($data){
        self::directory(self::$vendorsId, $data);
    }


    /**
     * @param $id
     * @return mixed
     * @return Bitrix\Main\ORM\Entity
     */
    public static function getDirectoryClass($id){
        static $cache = null;
        if ($cache === null){
            $cache = [];
        }
        if (!$cache[$id]){
            try {
                $class = Bitrix\HighloadBlock\HighloadBlockTable::compileEntity($id)->getDataClass();
            } catch (\Throwable $e){
                $class = null;
            }
            $cache[$id] = $class;
        }
        return $cache[$id];
    }

    public static function setDirectoryRowId($directoryId, $xmlId, $id){
        self::$directoryCache[$directoryId][$xmlId] = $id;
    }

    public static function getDirectoryRowId($directoryId, $xmlId){
        if (empty(self::$directoryCache[$directoryId])){
            $hl = self::getDirectoryClass($directoryId);
            if (!$hl){
                return null;
            }
            $iter = $hl::getList([
                'select' => ['ID', 'UF_XML_ID']
            ]);
            self::$directoryCache[$directoryId] = [];
            while ($i = $iter->fetch()){
                self::$directoryCache[$directoryId][$i['UF_XML_ID']] = $i['ID'];
            }
        }
        return self::$directoryCache[$directoryId][$xmlId];
    }

    public static function directory($directoryId, $data){

        if (empty($directoryId) || empty($data) || empty($data->values)){
            return;
        }

        $hl = self::getDirectoryClass($directoryId);

        if (!$hl){
            return;
        }


        $head = array_flip($data->head);
        foreach ($data->values as $row){

            if (self::getDirectoryRowId($directoryId, $row[$head['id']]) > 0) {
                continue;
            }

            /** @var Bitrix\Main\ORM\Data\AddResult $res */
            $res = $hl::add([
                'UF_NAME'   => $row[$head['name']],
                'UF_XML_ID' => $row[$head['id']],
            ]);

            if ($res->isSuccess()){
                self::setDirectoryRowId($directoryId, $row[$head['id']], $res->getId());
            }
        }
    }
}
<?php

use Bitrix\Main;

class ExtBasketItemTable
    extends Main\Entity\DataManager {

    // Status index in list of statuses (self::getStatuses)
    const STATUS_DEFAULT   = 'PROCESS';
    const STATUS_PROCESS   = 'PROCESS';
    const STATUS_CANCELED  = 'CANCELED';
    const STATUS_CONFIRMED = 'CONFIRMED';
    const STATUS_COMPLETED = 'COMPLETED';

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName(){
        return 'cbr_basket_item_ext';
    }

    public static function getNewOrderStatus(){
        return 'R';
    }

    public static function getStatuses(){
        return [
            self::STATUS_PROCESS,
            self::STATUS_CANCELED,
            self::STATUS_CONFIRMED,
            self::STATUS_COMPLETED,
        ];
    }

    /**
     * Returns entity map definition.
     * @return array
     * @throws Main\ArgumentException
     * @throws Main\SystemException
     */
    public static function getMap(){
        return array(
            new Main\Entity\IntegerField(
                'ID',
                array(
                    'primary'      => true,
                    'autocomplete' => true,
                )
            ),

            new Main\Entity\IntegerField(
                'BASKET_ID',
                array(
                    'required' => true,
                )
            ),
            new Main\Entity\IntegerField(
                'ORDER_ID',
                array(
                    'required' => true,
                )
            ),

            new Main\Entity\EnumField(
                'STATUS',
                [
                    'values' => self::getStatuses(),
                    'default_value' => self::STATUS_DEFAULT,
                ]
            ),

            new Main\Entity\DateField(
                'DATE_FROM'
            ),
            new Main\Entity\DateField(
                'DATE_TO'
            ),

            // Номер месяца [0..11] по началу периода
            new Main\ORM\Fields\ExpressionField(
                'MONTH',
                'MONTH(%s) - 1',
                ['DATE_FROM']
            ),

            new Main\ORM\Fields\Relations\Reference(
                'BASKET',
                Bitrix\Sale\Internals\BasketTable::class,
                array(
                    '=this.BASKET_ID' => 'ref.ID'
                )
            ),

            new Main\ORM\Fields\Relations\Reference(
                'BASKET_PROP',
                Bitrix\Sale\Internals\BasketPropertyTable::class,
                array(
                    '=this.BASKET_ID' => 'ref.BASKET_ID'
                )
            ),

            new Main\ORM\Fields\Relations\Reference(
                'ORDER',
                Bitrix\Sale\Internals\OrderTable::class,
                array(
                    '=this.ORDER_ID' => 'ref.ID'
                )
            ),
        );
    }

    protected static function setStatus($id, $status){
        $r = self::update($id, [
            'STATUS' => $status,
        ]);

        $t = $r->getData();
        $t['NEED_REFRESH_PAGE'] = defined('NEED_REFRESH_PAGE_EXT_ITEM_'.$id);
        $t['STATUS'] = $status;

        return $r->setData($t);
    }

    public static function cancel($id){
        return self::setStatus($id, self::STATUS_CANCELED);
    }

    public static function confirm($id){
        return self::setStatus($id, self::STATUS_CONFIRMED);
    }

    public static function complete($id){
        return self::setStatus($id, self::STATUS_COMPLETED);
    }

    public static function deleteByOrder($orderId){
        try {
            Main\Application::getConnection()
                ->query('DELETE FROM ' . self::getTableName() . ' WHERE ORDER_ID = ' . $orderId);
        } catch (\Throwable $e){}
    }

    public static function onAfterUpdate(Main\Event $event){
        $r = $event->getParameter('id');
        $id = $r['ID'];

        if (!$id){
            return;
        }

        //Изменение доступного количества товара
        self::setAvailable($event);

        $newOrderStatus = self::getNewOrderStatus();

        $iter = self::getListByItemId($id);
        $needSetStatus = true;
        $orderId = null;
        while ($item = $iter->fetch()){
            if ($orderId === null){
                $orderId = $item['ORDER_ID'];
            }
            // Если хоть у одной позиции стоит статус по умолчанию
            // или если статус заказа рассмотрен или выполнен
            $needSetStatus = $needSetStatus
                    && ($item['STATUS'] != self::STATUS_DEFAULT)
                    && ($item['ORDER_STATUS'] != $newOrderStatus && $item['ORDER_STATUS'] != 'F');
            // ничего не меняем
            if (!$needSetStatus){
                return;
            }
        }
        // Проставляем заказу нужный статус
        // так как у позиций корзины меняли статус
        global $USER;
        if ($USER->IsAdmin()) {
            $order = \Bitrix\Sale\Order::load($orderId);
            $order->setField('STATUS_ID', $newOrderStatus);
            $res = $order->save();
            if ($res->isSuccess()) {
                define('NEED_REFRESH_PAGE_EXT_ITEM_' . $id, true);
            }
        }
    }
    public static function setAvailable(Bitrix\Main\Event $event){
        $ID = $event->getParameter('id');

       $iter = ExtBasketItemTable::getRow([
            'filter' => [
                '=ID' => $ID
            ],
            'select' => [
                'STATUS',
                'DATE_FROM',
                'DATE_TO',
                'PRODUCT_ID' => 'BASKET.PRODUCT_ID',
                'QUANTITY'=>'CATALOG.QUANTITY',
                'QUANTITY_TRACE'=>'CATALOG.QUANTITY_TRACE'

            ],
            'runtime' => [
                new Bitrix\Main\ORM\Fields\Relations\Reference(
                    'CATALOG',
                    \Bitrix\Catalog\ProductTable::class,
                    \Bitrix\Main\ORM\Query\Join::on('this.BASKET.PRODUCT_ID', 'ref.ID')
                )
            ]
        ]);
        $quantityTrace = \Bitrix\Main\Config\Option::get('catalog', 'default_quantity_trace')==='Y';





        if ($quantityTrace && $iter['QUANTITY_TRACE'] === 'D' || $iter['QUANTITY_TRACE'] === 'Y') {
            $arFields = [];
            $productProps = [];
            $linkElementID = null;

            $mxResult = CCatalogSku::GetProductInfo(
                $iter['PRODUCT_ID']
            );
            if ($mxResult['ID']){
                CIBlockElement::GetPropertyValuesArray(
                    $productProps,
                    CATALOG_MARKETING,
                    ['=ID' => $mxResult['ID']],
                    false,
                    ['GET_RAW_DATA' => 'Y']
                );
                $productProps = $productProps[$mxResult['ID']];
            }
            if($productProps['LINK_ELEMENT']['VALUE']){
                $link_element = CCatalogSKU::getOffersList(
                    $productProps['LINK_ELEMENT']['VALUE'],
                    CATALOG_MARKETING,
                    array(
                        'PROPERTY_POST_DATE_FROM'=>$iter['DATE_FROM']->format('Y-m-d'),
                        'PROPERTY_POST_DATE_TO'=>$iter['DATE_TO']->format('Y-m-d')
                    ),
                    array('ID', 'AVAILABLE')
                );
                if ($link_element) {
                    $linkElementID = reset($link_element[$productProps['LINK_ELEMENT']['VALUE']])['ID'];
                }
            }


            if ($iter['QUANTITY'] > 0 && $iter['STATUS'] === 'CONFIRMED') {
                $arFields = array(
                    "QUANTITY" => 0,
                    "AVAILABLE" => "N"
                );
            } elseif ($iter['STATUS'] === 'CANCELED') {
                //Если кол. учет включен и статус заказа ОТМЕНЕН
                $arFields = array(
                    "QUANTITY" => 1,
                    "AVAILABLE" => "Y"
                );
            }

            if (!empty($arFields)) {
                \Bitrix\Catalog\Model\Product::update($iter['PRODUCT_ID'], $arFields);
                if ($linkElementID > 0) {
                    \Bitrix\Catalog\Model\Product::update($linkElementID, $arFields);
                }
            }

        }

    }
    /**
     * Получение записей одного заказа по id какой-либо записи
     * @param $id
     * @return Main\ORM\Query\Result
     * @throws Main\ArgumentException
     * @throws Main\ObjectPropertyException
     * @throws Main\SystemException
     */
    public static function getListByItemId($id){
        return self::getList([
            'select' => ['*', 'ORDER_STATUS' => 'ORDER.STATUS_ID'],
            'filter' => [
                '=SELF_BY_ORDER.ID' => $id,
            ],
            'runtime' => [
                new Main\ORM\Fields\Relations\Reference(
                    'SELF_BY_ORDER',
                    self::class,
                    ['=this.ORDER_ID' => 'ref.ORDER_ID'],
                    ['join_type' => 'INNER']
                ),
            ]
        ]);
    }
}
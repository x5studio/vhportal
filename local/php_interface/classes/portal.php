<?php

namespace VH\Portal;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Type\Date;
use Bitrix\Main\UserTable;
use Bitrix\Sale\Basket;
use Bitrix\Sale\Fuser;
use SingletonTrait;
use ExtBasketItemTable;
use CEvent;


class Helper {
    use SingletonTrait;

    protected $workTime;
    protected $phone;

    protected $basket = null;

    protected function init(){
        $this->workTime = 'Центральный офис Пн - Пт  9:00 - 19:00';
        $this->phone = '+7 8722 67 18 18';
    }

    public function getPhone(){
        return $this->phone;
    }

    public function getWorkTime(){
        return $this->workTime;
    }

    public function getUserBrands(){
        global $USER;

        $brands = UserTable::getRow([
            'select' => ['UF_BRANDS'],
            'filter' => ['=ID' => $USER->GetID()],
        ])['UF_BRANDS'];

        $brandsEntity = HighloadBlockTable::compileEntity(BRANDS_HL_BLOCK)->getDataClass();

//        if(empty($brands))
//            return null;

        $iter = $brandsEntity::getList([
            'select' => ['*'],
            'filter' => [
                '@ID' => $brands,
            ],
        ]);
        $r = null;
        while ($brand = $iter->fetch()){
            $r[$brand['ID']] = $brand;
        }

        return $r;
    }

    /**
     * Отправка уведомления подтвержденным заявкам
     * за 30 дней до начала
     * о необходимости предоставить материалы
     * VH_BEFORE_ORDER_COMPLETE
     */
    public static function sendNotify(){
        /** Статус рассмотрен */
        $status = 'R';

        $beforeDays = 30;

        $iter = ExtBasketItemTable::getList([
            'filter' => [
                '=ORDER.STATUS_ID'  => $status,
                '=BASKET_PROP.CODE' => 'BRAND',
                '=STATUS'           => ExtBasketItemTable::STATUS_CONFIRMED,
                '=DATE_FROM'        => (new Date)->add("P{$beforeDays}D"),
            ],
            'select' => [
                '*',
                'BRAND'   => 'BASKET_PROP.VALUE',
                'NAME'    => 'BASKET.NAME',
                'EMAIL'   => 'ORDER.USER.EMAIL',
                'USER_ID' => 'ORDER.USER.ID',
                'SITE_ID' => 'ORDER.LID',
            ]
        ]);
        $users = [];
        while ($item = $iter->fetch()){
            $users[$item['USER_ID']]['EMAIL'] = $item['EMAIL'];

            $users[$item['USER_ID']]['BRANDS'][$item['BRAND']]['NAME']    = $item['BRAND'];
            $users[$item['USER_ID']]['BRANDS'][$item['BRAND']]['SITE_ID'] = $item['SITE_ID'];

            $users[$item['USER_ID']]['BRANDS'][$item['BRAND']]['ITEMS'][] = [
                'NAME' => $item['NAME'],
                'DATE_FROM' => $item['DATE_FROM'],
            ];
        }

        if (empty($users)){
            return;
        }

        $dateFrom = null;
        foreach ($users as $user){

            foreach ($user['BRANDS'] as $brand){
                $itemList = [];

                foreach ($brand['ITEMS'] as $item){
                    $itemList[] = "<b>{$item['NAME']}</b> - {$item['DATE_FROM']}";
                    if (!$dateFrom){
                        $dateFrom = $item['DATE_FROM'];
                    }
                }

                $message = [
                    'EMAIL'     => $user['EMAIL'],
                    'BRAND'     => $brand['NAME'],
                    'DATE_FROM' => $dateFrom,
                    'ITEM_LIST' => implode('<br>', $itemList),
                ];

                CEvent::SendImmediate('VH_BEFORE_ORDER_COMPLETE', $brand['SITE_ID'], $message);
                // Отложенная отправка
                //CEvent::Send('VH_BEFORE_ORDER_COMPLETE', $brand['SITE_ID'], $message);
            }
        }
    }

    // Заглушка
    public function getUserAccountValue(){
        return 0;
    }

    public function getBasket(){
        $this->basket = Basket::loadItemsForFUser(Fuser::getId(), SITE_ID);
        return $this->basket;
    }

    public function getUserBasketPositions(){
        return $this->getBasket()->count();
    }

    public function getBasketUrl(){
        return '/personal/cart/';
    }

    public function getAddToBasketUrl(array $params = []){
        $url = new \Bitrix\Main\Web\Uri('/local/ajax/basket/');

        if (!empty($params)){
            $url->addParams($params);
        }

        return $url->getUri();
    }

    public static function getProductBarcode($id, bool $multiple = false, bool $useCache = false){
        static $cache = null;
        $res = null;
        if ($useCache){
            if (empty($cache)){
                $iter = \Bitrix\Catalog\StoreBarcodeTable::getList([
                    'select' => [
                        'PRODUCT_ID',
                        'BARCODE',
                    ],
                ]);
                while ($r = $iter->fetch()){
                    $cache[$r['PRODUCT_ID']][] = $r['BARCODE'];
                }
            }
            $res = $cache[$id];
        } else {
            $iter = \Bitrix\Catalog\StoreBarcodeTable::getList([
                'filter' => [
                    '=PRODUCT_ID' => $id,
                ],
                'select' => [
                    'BARCODE',
                ],
            ]);
            while ($r = $iter->fetch()){
                $res[] = $r['BARCODE'];
            }
        }
        return $multiple ? $res : $res[0];
    }

    /**
     * @param Date $date
     * @param $brands
     * @param bool $needIter
     * @param bool $multipleBarcode
     * @param string $barCodesDelimiter
     * @return array|\Bitrix\Main\DB\Result
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\Db\SqlQueryException
     */
    public function getReportDataForStocks(Date $date, $brands, bool $needIter = false, bool $multipleBarcode = false, $barCodesDelimiter = "\n"){
        if (!is_array($brands)){
            $brands = [$brands];
        }
        $conn = Application::getConnection();
        $date = $date->format('Y-m-d');

        $sHelper = $conn->getSqlHelper();
        $brandsRes = [];
        foreach ($brands as $brand){
            $brandsRes[] = $sHelper->forSql($brand);
        }
        $brandsRes = "'" . implode("', '", $brandsRes) . "'";

        $barcodeSelectField = $multipleBarcode
            ? "GROUP_CONCAT(BARCODE separator '{$barCodesDelimiter}')"
            : 'MAX(BARCODE)';

        $sql = <<<SQL
select
    (@i:=@i+1) as i,
	el.ID,
	el.NAME,
	props.PROPERTY_53 as ARTNUMBER,
	props.PROPERTY_54 as BRAND_REF,
	brands.UF_NAME as BRAND,
	-- '' as BARCODE,
    barcodes.BARCODE,
	st.AMOUNT
from (select @i:=0) as tmp_r, b_iblock_element el
	inner join b_iblock_element_prop_s7 props on props.IBLOCK_ELEMENT_ID = el.ID
	inner join (
			select
				PRODUCT_ID,
				SUM(AMOUNT) as AMOUNT
			from cbr_vh_stocks
			where DATE = '{$date}'
			group by PRODUCT_ID
		) st on st.PRODUCT_ID = el.ID
	left join vh_brand_reference brands on brands.UF_XML_ID = props.PROPERTY_54
	left join (
        select PRODUCT_ID, {$barcodeSelectField} as BARCODE
        from b_catalog_store_barcode
        group by PRODUCT_ID
	) barcodes on barcodes.PRODUCT_ID = el.ID

where props.PROPERTY_54 in ({$brandsRes})
SQL;

        $iter = $conn->query($sql);
        /*
        $iter->addFetchDataModifier(function(&$data) use ($multipleBarcode){
            $data['BARCODE'] = implode("\n", self::getProductBarcode($data['ID'], $multipleBarcode, true) ?: ['']);
        });
        */

        return $needIter ? $iter : $iter->fetchAll();
    }

    public static function writeCsvDataFromIter($file, $iterator, $header, $fieldsKey, $delimiter = ';'){

        // UTF-8 fix
        fputs($file, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($file, $header, $delimiter);
        while ($i = $iterator->fetch()){
            $row = [];
            foreach ($fieldsKey as $key){
                $row[] = $i[$key];
            }
            fputcsv($file, $row, $delimiter);
        }
    }
}

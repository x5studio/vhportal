<?php

namespace Vh\Reports;

use Bitrix\Main\Application;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Fields\Validators\RegExpValidator;

class BarcodeTable extends DataManager
{

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'vh_reports_barcode';
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getMap()
    {
        return [

            (new IntegerField('ID'))
                ->configurePrimary()
                ->configureAutocomplete(),

            (new IntegerField('PRODUCT_ID'))
                ->configureRequired(),

            (new StringField('BARCODE'))
                ->configureRequired(),

            (new Reference(
                'PRODUCT',
                ProductTable::class,
                Join::on('this.PRODUCT_ID', 'ref.ID')
            )),

        ];
    }

    /**
     * Создание таблицы с необязательными полями.
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\Db\SqlQueryException
     * @throws \Bitrix\Main\SystemException
     */
    public static function createDbTable()
    {
        $sql = self::getEntity()->compileDbTableStructureDump();

        $res = [];
        if (preg_match_all("/\`(\w+)\`[\w\s\(\)]+\,/", $sql[0], $res, PREG_SET_ORDER)) {

            $fields = self::getEntity()->getScalarFields();
            $new = trim(substr($sql[0], 0, strpos($sql[0], '('))) . " (";
            foreach ($res as $i) {
                $f = $fields[$i[1]];
                if (!$f->isPrimary() && !$f->isUnique() && !$f->isRequired()) {
                    $i[0] = str_replace(' NOT NULL', '', $i[0]);
                }
                $new .= "{$i[0]} ";
            }
            $new .= trim(substr($sql[0], strrpos($sql[0], 'PRIMARY KEY'), strlen($sql[0]) - strrpos($sql[0], 'PRIMARY KEY')));

            $sql[0] = $new;

        }

        foreach ($sql as $s) {
            Application::getConnection()->query($s);
        }
    }
}
<?php

namespace VH\Reports;


use Bitrix\Main\ArgumentException;
use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\ArgumentTypeException;
use Bitrix\Main\ORM\Data\Result;
use Bitrix\Main\SystemException;
use VH\Reports\Entities\Barcode;
use VH\Reports\Entities\Brand;
use VH\Reports\Entities\Entity;
use VH\Reports\Entities\Group;
use VH\Reports\Entities\Offer;
use VH\Reports\Entities\Product;
use VH\Reports\Entities\ProductType;
use VH\Reports\Entities\Rating;
use VH\Reports\Entities\Sale;
use VH\Reports\Entities\Shop;
use VH\Reports\Entities\Stock;
use VH\Reports\Entities\Store;

final class Report
{
    /** @var array */
    public $data;

    /** @var Entity[] */
    public $entities;


    /** @var Entity[] */
    protected static $map = [
        'brands' => Brand::class,
        'types' => ProductType::class,
        'groups' => Group::class,
        'shops' => Shop::class,
        'stores' => Store::class,
        'products' => Product::class,
        'offers' => Offer::class,
        'barcodes' => Barcode::class,
        'sales' => Sale::class,
        'stocks' => Stock::class,
        'ratings' => Rating::class,
    ];


    /**
     * Report constructor.
     * @param $data
     * @throws ArgumentNullException
     * @throws ArgumentTypeException
     */
    public function __construct($data)
    {

        if (empty($data)) throw new ArgumentNullException('data');
        if (!is_array($data)) throw new ArgumentTypeException('data', 'array');

        $this->data = $data;

        foreach (self::$map as $type => $class) {
            if (!empty($this->data[$type])){
                $this->entities[$type] = new $class($this->data[$type], (array)array_intersect_key($this->entities, $class::getRequiredRef()));
            }
        }

    }


    /**
     * @return array
     */
    public function getTypeArray()
    {
        return array_intersect(array_keys(self::$map), array_keys($this->data));
    }


    /**
     * @return Result
     * @throws ArgumentException
     * @throws SystemException
     */
    public function save()
    {
        $result = new Result();

        foreach ($this->entities as $type => $entity) {
            if ($type == 'brands') continue;
            $r = $entity->validate()->prepare()->save();
            if (!$r->isSuccess()) {
                $result->addErrors($r->getErrors());
            }
        }

        return $result;
    }


}

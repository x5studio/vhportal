<?php

namespace Vh\Reports;

use Bitrix\Main\Application;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\ORM\Fields\Relations\OneToMany;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Fields\Validators\RegExpValidator;

class ProductTable extends DataManager
{

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'vh_reports_product';
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getMap()
    {
        return [

            (new IntegerField('ID'))
                ->configurePrimary()
                ->configureAutocomplete(),

            (new StringField('XML_ID'))
                ->configureUnique(),

            (new StringField('NAME')),

            (new IntegerField('BRAND_REF_ID')),

            (new IntegerField('PRODUCT_TYPE_ID')),

            (new IntegerField('GROUP_ID')),

            (new IntegerField('SKU_ID')),

            (new StringField('PROPS')),

            (new StringField('ARTICLE')),


            (new Reference(
                'BRAND',
                HighloadBlockTable::compileEntity(BRANDS_HL_BLOCK)->getDataClass(),
                Join::on('this.BRAND_REF_ID', 'ref.ID')
            )),

            (new Reference(
                'PRODUCT_TYPE',
                ProductTypeTable::class,
                Join::on('this.PRODUCT_TYPE_ID', 'ref.ID')
            )),

            (new Reference(
                'GROUP',
                GroupTable::class,
                Join::on('this.GROUP_ID', 'ref.ID')
            )),

            (new OneToMany(
                'BARCODES',
                BarcodeTable::class,
                'PRODUCT'
            ))->configureJoinType('left'),

            (new Reference(
                'SKU',
                ProductTable::class,
                Join::on('this.SKU_ID', 'ref.ID')
            )),

        ];
    }

    /**
     * Создание таблицы с необязательными полями.
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\Db\SqlQueryException
     * @throws \Bitrix\Main\SystemException
     */
    public static function createDbTable()
    {
        $sql = self::getEntity()->compileDbTableStructureDump();

        $res = [];
        if (preg_match_all("/\`(\w+)\`[\w\s\(\)]+\,/", $sql[0], $res, PREG_SET_ORDER)) {

            $fields = self::getEntity()->getScalarFields();
            $new = trim(substr($sql[0], 0, strpos($sql[0], '('))) . " (";
            foreach ($res as $i) {
                $f = $fields[$i[1]];
                if (!$f->isPrimary() && !$f->isUnique() && !$f->isRequired()) {
                    $i[0] = str_replace(' NOT NULL', '', $i[0]);
                }
                $new .= "{$i[0]} ";
            }
            $new .= trim(substr($sql[0], strrpos($sql[0], 'PRIMARY KEY'), strlen($sql[0]) - strrpos($sql[0], 'PRIMARY KEY')));

            $sql[0] = $new;

        }

        foreach ($sql as $s) {
            Application::getConnection()->query($s);
        }
    }
}
<?php

namespace Vh\Reports;

use Bitrix\Main\Type\Date;
use Bitrix\Main\Application;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\Entity\FloatField;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\DateField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\ORM\Fields\Relations\Reference;

class RatingTable extends DataManager
{

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'vh_reports_rating';
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getMap()
    {
        return [

            (new DateField('DATE'))
                ->configurePrimary(),

            (new IntegerField('BRAND_REF_ID'))
                ->configurePrimary(),

            (new IntegerField('STORE_ID'))
                ->configurePrimary(),

            (new IntegerField('PRODUCT_TYPE_ID'))
                ->configurePrimary(),

            (new FloatField('QUANTITY'))
                ->configureDefaultValue(0),

            (new FloatField('PRICE'))
                ->configureDefaultValue(0),

            (new Reference(
                'BRAND',
                HighloadBlockTable::compileEntity(BRANDS_HL_BLOCK)->getDataClass(),
                Join::on('this.BRAND_REF_ID', 'ref.ID')
            ))->configureJoinType('inner'),

            (new Reference(
                'STORE',
                StoreTable::class,
                Join::on('this.STORE_ID', 'ref.ID')
            ))->configureJoinType('inner'),

            (new Reference(
                'PRODUCT_TYPE',
                ProductTypeTable::class,
                Join::on('this.PRODUCT_TYPE_ID', 'ref.ID')
            ))->configureJoinType('inner'),

        ];
    }

    /**
     * Создание таблицы с необязательными полями.
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\Db\SqlQueryException
     * @throws \Bitrix\Main\SystemException
     */
    public static function createDbTable()
    {
        $sql = self::getEntity()->compileDbTableStructureDump();

        $res = [];
        if (preg_match_all("/\`(\w+)\`[\w\s\(\)]+\,/", $sql[0], $res, PREG_SET_ORDER)) {

            $fields = self::getEntity()->getScalarFields();
            $new = trim(substr($sql[0], 0, strpos($sql[0], '('))) . " (";
            foreach ($res as $i) {
                $f = $fields[$i[1]];
                if (!$f->isPrimary() && !$f->isUnique() && !$f->isRequired()) {
                    $i[0] = str_replace(' NOT NULL', '', $i[0]);
                }
                $new .= "{$i[0]} ";
            }
            $new .= trim(substr($sql[0], strrpos($sql[0], 'PRIMARY KEY'), strlen($sql[0]) - strrpos($sql[0], 'PRIMARY KEY')));

            $sql[0] = $new;

        }

        foreach ($sql as $s) {
            Application::getConnection()->query($s);
        }
    }
}
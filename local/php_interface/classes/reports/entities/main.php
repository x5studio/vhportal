<?php

namespace VH\Reports\Entities;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Type\Date;


class Brand extends Entity
{

    public function __construct(array &$data, array $refData = [])
    {
        $data['head'] = ['UF_XML_ID', 'UF_NAME'];
        parent::__construct($data, $refData);
    }

    public static function getClass()
    {
        return HighloadBlockTable::compileEntity(BRANDS_HL_BLOCK)->getDataClass();
    }

    public static function getExist(array $filter = [])
    {
        return array_column(parent::getExist($filter), null, 'UF_XML_ID');
    }

    public function findExist($item, $columns = [])
    {
        return parent::findExist($item, ['UF_XML_ID']);
    }

}


class ProductType extends Entity
{

    public function __construct(array &$data, array $refData = [])
    {
        $data['head'] = ['XML_ID', 'NAME'];
        parent::__construct($data, $refData);
    }

    public static function getClass()
    {
        return \VH\Reports\ProductTypeTable::class;
    }

    public static function getExist(array $filter = [])
    {
        return array_column(parent::getExist($filter), null, 'XML_ID');
    }

    public function findExist($item, $columns = [])
    {
        return parent::findExist($item, ['XML_ID']);
    }

}


class Group extends Entity
{

    public function __construct(array &$data, array $refData = [])
    {
        $data['head'] = ['XML_ID', 'NAME', 'PARENT_ID', 'RATING_TYPE'];
        parent::__construct($data, ['groups' => $this]);
    }

    public static function getClass()
    {
        return \VH\Reports\GroupTable::class;
    }

    public static function getRequiredRef()
    {
        return [
            'groups' => ['PARENT_ID' => 'XML_ID'],
        ];
    }

    public static function getExist(array $filter = [])
    {
        return array_column(parent::getExist($filter), null, 'XML_ID');
    }

    public function findExist($item, $columns = [])
    {
        return parent::findExist($item, ['XML_ID']);
    }

    public function save()
    {
        parent::save();
        $this->prepare();
        $r = parent::save();
        $this->rebuildTree();
        return $r;
    }

    public function rebuildTree() {
        $this->toUpdate = [];

        $res = [];
        $map = array_column(parent::getExist(['select' => ['ID', 'PARENT_ID'], 'order' => ['PARENT_ID', 'NAME']]), null, 'ID');

        foreach ($map as $i => $v) {
            if ($v['PARENT_ID']) {
                $map[$v['PARENT_ID']]['CHILDREN'][$v['ID']] = $v['ID'];
            } else {
                $res[$v['ID']] = $v['ID'];
            }
        }

        $count = 0;
        $depth = 0;
        $counter = function ($arr) use (&$map, &$count, &$depth, &$counter) {
            ++$depth;
            foreach ($arr as $id) {
                $map[$id]['DEPTH_LEVEL'] = $depth;
                $map[$id]['LEFT_MARGIN'] = ++$count;

                $counter($map[$id]['CHILDREN']);
                unset($map[$id]['CHILDREN']);

                $map[$id]['RIGHT_MARGIN'] = ++$count;
            }
            --$depth;
        };

        $counter($res);

        foreach ($map as $i => $v) {
            $this->toUpdate[serialize(['ID' => $i])] = $v;
        }

        parent::save();
    }

}


class Shop extends Entity
{

    public function __construct(array &$data, array $refData = [])
    {
        $data['head'] = ['XML_ID', 'NAME'];

        // Костыль для пустых подразделений в продажах
        $data['values'][] = ["", "Неизвестно"];

        parent::__construct($data, $refData);
    }

    public static function getClass()
    {
        return \VH\Reports\ShopTable::class;
    }

    public static function getExist(array $filter = [])
    {
        return array_column(parent::getExist($filter), null, 'XML_ID');
    }

    public function findExist($item, $columns = [])
    {
        return parent::findExist($item, ['XML_ID']);
    }

}


class Store extends Entity
{

    public function __construct(array &$data, array $refData = [])
    {
        $data['head'] = ['XML_ID', 'NAME'];
        parent::__construct($data, $refData);
    }

    public static function getClass()
    {
        return \VH\Reports\StoreTable::class;
    }

    public static function getExist(array $filter = [])
    {
        return array_column(parent::getExist($filter), null, 'XML_ID');
    }

    public function findExist($item, $columns = [])
    {
        return parent::findExist($item, ['XML_ID']);
    }

}


class Product extends Entity
{

    public function __construct(array &$data, array $refData = [])
    {
        $data['head'] = ['XML_ID', 'NAME', 'BRAND_REF_ID', 'PRODUCT_TYPE_ID', 'GROUP_ID', 'ARTICLE'];
        parent::__construct($data, $refData);
    }

    public static function getClass()
    {
        return \VH\Reports\ProductTable::class;
    }

    public static function getRequiredRef()
    {
        return [
            'brands' => ['BRAND_REF_ID' => 'UF_XML_ID'],
            'types' => ['PRODUCT_TYPE_ID' => 'XML_ID'],
            'groups' => ['GROUP_ID' => 'XML_ID'],
        ];
    }

    public static function getExist(array $filter = [])
    {
        return array_column(parent::getExist($filter), null, 'XML_ID');
    }

    public function findExist($item, $columns = [])
    {
        return parent::findExist($item, ['XML_ID']);
    }

}


class Offer extends Entity
{

    public function __construct(array &$data, array $refData = [])
    {
        $data['head'] = ['XML_ID', 'SKU_ID', 'NAME', 'PROPS', 'ARTICLE'];
        parent::__construct($data, $refData);
    }

    public static function getClass()
    {
        return \VH\Reports\ProductTable::class;
    }

    public static function getRequiredRef()
    {
        return [
            'brands' => ['BRAND_REF_ID' => 'UF_XML_ID'],
            'types' => ['PRODUCT_TYPE_ID' => 'XML_ID'],
            'groups' => ['GROUP_ID' => 'XML_ID'],
            'products' => ['SKU_ID' => 'XML_ID'],
        ];
    }

    public static function getExist(array $filter = [])
    {
        return array_column(parent::getExist($filter), null, 'XML_ID');
    }

    public function findExist($item, $columns = [])
    {
        return parent::findExist($item, ['XML_ID']);
    }

    public function validate()
    {
        parent::validate();

        foreach ($this->data as $i => &$item) {
            $refItem = $this->reference['products']->find('XML_ID', $item['SKU_ID']);
            $item['GROUP_ID'] = $refItem['GROUP_ID'];
            $item['BRAND_REF_ID'] = $refItem['BRAND_REF_ID'];
            $item['PRODUCT_TYPE_ID'] = $refItem['PRODUCT_TYPE_ID'];
        }

        return $this;
    }

}


class Barcode extends Entity
{

    public function __construct(array &$data, array $refData = [])
    {
        $data['head'] = ['PRODUCT_ID', 'OFFER_ID', 'BARCODE'];
        parent::__construct($data, $refData);
    }

    public static function getClass()
    {
        return \VH\Reports\BarcodeTable::class;
    }

    public static function getRequiredRef()
    {
        return [
            'products' => ['PRODUCT_ID' => 'XML_ID'],
            'offers' => ['OFFER_ID' => 'XML_ID', 'PRODUCT_ID'],
        ];
    }

    public static function getExist(array $filter = [])
    {
        $res = [];
        foreach(parent::getExist($filter) as $item) {
            $res[$item['PRODUCT_ID']][$item['BARCODE']] = $item;
        }
        return $res;
    }

    public function findExist($item, $columns = [])
    {
        return parent::findExist($item, ['PRODUCT_ID', 'BARCODE']);
    }

}


class Sale extends Entity
{

    public function __construct(array &$data, array $refData = [])
    {
        $data['head'] = ['DATE', 'PRODUCT_ID', 'OFFER_ID', 'SHOP_ID', 'QUANTITY', 'BASE_PRICE', 'PRICE', 'COST_PRICE'];
        array_walk($data['values'], function (&$v, $k) {
            $v[0] = Date::createFromTimestamp(strtotime($v[0]));
        });
        parent::__construct($data, $refData);
    }

    public static function getClass()
    {
        return \VH\Reports\SaleTable::class;
    }

    public static function getRequiredRef()
    {
        return [
            'products' => ['PRODUCT_ID' => 'XML_ID'],
            'offers' => ['OFFER_ID' => 'XML_ID', 'PRODUCT_ID'],
            'shops' => ['SHOP_ID' => 'XML_ID'],
        ];
    }

    public static function getExist(array $filter = [])
    {
        $res = [];
        foreach (parent::getExist($filter) as $item) {
            $res[(string)$item['DATE']][$item['SHOP_ID']][$item['PRODUCT_ID']] = $item;
        }
        return $res;
    }

    public function findExist($item, $columns = [])
    {
        return parent::findExist($item, ['DATE', 'SHOP_ID', 'PRODUCT_ID']);
    }

}


class Stock extends Entity
{

    public function __construct(array &$data, array $refData = [])
    {
        $data['head'] = ['DATE', 'STORE_ID', 'PRODUCT_ID', 'OFFER_ID', 'QUANTITY'];
        array_walk($data['values'], function (&$v, $k) {
            $v[0] = Date::createFromTimestamp(strtotime($v[0]));
        });
        parent::__construct($data, $refData);
    }

    public static function getClass()
    {
        return \VH\Reports\StockTable::class;
    }

    public static function getRequiredRef()
    {
        return [
            'stores' => ['STORE_ID' => 'XML_ID'],
            'products' => ['PRODUCT_ID' => 'XML_ID'],
            'offers' => ['OFFER_ID' => 'XML_ID', 'PRODUCT_ID'],
        ];
    }

    public static function getExist(array $filter = [])
    {
        $res = [];
        foreach (parent::getExist($filter) as $item) {
            $res[(string)$item['DATE']][$item['STORE_ID']][$item['PRODUCT_ID']] = $item;
        }
        return $res;
    }

    public function findExist($item, $columns = [])
    {
        return parent::findExist($item, ['DATE', 'STORE_ID', 'PRODUCT_ID']);
    }

}


class Rating extends Entity
{

    public function __construct(array &$data, array $refData = [])
    {
        $data['head'] = ['DATE', 'BRAND_REF_ID', 'STORE_ID', 'PRODUCT_TYPE_ID', 'QUANTITY', 'PRICE'];
        $res = [];
        foreach ($data['values'] as $i) {
            $i[0] = Date::createFromTimestamp(strtotime($i[0]));
            foreach ($i[3] as $v) {
                $res[] = [$i[0], $i[1], $i[2], $v['type'], $v['count'], $v['amount']];
            }
        }
        $data['values'] = $res;

        parent::__construct($data, $refData);
    }

    public static function getClass()
    {
        return \VH\Reports\RatingTable::class;
    }

    public static function getRequiredRef()
    {
        return [
            'brands' => ['BRAND_REF_ID' => 'UF_XML_ID'],
            'stores' => ['STORE_ID' => 'XML_ID'],
            'types' => ['PRODUCT_TYPE_ID' => 'XML_ID'],
        ];
    }

    public static function getExist(array $filter = [])
    {
        $res = [];
        foreach (parent::getExist($filter) as $item) {
            $res[(string)$item['DATE']][$item['STORE_ID']][$item['BRAND_REF_ID']][$item['PRODUCT_TYPE_ID']] = $item;
        }
        return $res;
    }

    public function findExist($item, $columns = [])
    {
        return parent::findExist($item, ['DATE', 'STORE_ID', 'BRAND_REF_ID', 'PRODUCT_TYPE_ID']);
    }

}

<?php

namespace VH\Reports\Entities;

use Bitrix\Main\ORM\Data\Result;
use Bitrix\Main\SystemException;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\ORM\Fields\ScalarField;

abstract class Entity
{

    /** @var array */
    public $map = [], $data = [], $exists = [], $toAdd = [], $toUpdate = [], $toDelete = [], $warnings = [];

    /** @var boolean */
    protected $isValid, $isPrepare;

    /** @var ScalarField[] */
    public $fields = [];

    /** @var Entity[] */
    public $reference = [];


    /**
     * Entity constructor.
     * @param array $data
     * @param array $refData
     * @throws ArgumentException
     * @throws SystemException
     */
    public function __construct(Array &$data, Array $refData = [])
    {
        if (empty($data) || (isset($data['head']) && empty($data['values']))) {
            throw new ArgumentNullException('data');
        } elseif (!empty(array_diff_key(static::getRequiredRef(), $refData))) {
            throw new ArgumentNullException('refData');
        }


        if (isset($data['head'])) {
            $head = $data['head'];
            array_walk($data['values'], function (&$i, $k) use ($head) {
                $i = array_combine($head, $i);
            });
        }

        if (isset($data['values'])) {
            $this->data = &$data['values'];
        } else {
            $this->data = &$data;
        }

        $this->reference = $refData;

        $this->fields = static::getClass()::getEntity()->getScalarFields();

        $this->exists = static::getExist();

    }


    public static function getExist(array $filter = [])
    {
        return (array)static::getClass()::getList($filter)->fetchAll();
    }


    public static function getRequiredRef()
    {
        return [];
    }


    /**
     * @return DataManager|string
     */
    public static function getClass()
    {
        return DataManager::class;
    }


    public function find($column, $value)
    {
        if (!isset($this->map[$column])) {
            $this->map[$column] = array_column($this->data, null, $column);
        }

        return (array)$this->map[$column][$value];
    }


    public function findExist($item, $columns = [])
    {
        $columns = is_array($columns) ? array_values($columns) : [$columns];

        $exist = [];

        for ($key = current($columns), $data = $this->exists; $key !== false; $key = next($columns)) {
            $data = $exist = $data[ (string)$item[$key] ];
        }

        return (array)$exist;
    }


    /**
     * @return $this
     * @throws ArgumentException
     * @throws SystemException
     */
    public function validate()
    {
        foreach ($this->data as $i => &$item) {
            foreach (static::getRequiredRef() as $k => $v) {
                $thisKey = key($v);

                $field = $this->fields[$thisKey];
                $required = $field && ($field->isRequired() || $field->isUnique() || $field->isPrimary());

                if (in_array($item[$thisKey], [null, ""]) && !$required) {
                    unset($item[$thisKey]);
                    continue;
                }

                $refKey = current($v);
                $refObject = $this->reference[$k];

                $refItem = $refObject->find($refKey, $item[$thisKey]);

                if (empty($refItem)) {
                    $this->warnings['Ref404']["{$i}:{$thisKey}"] = $item;
                    unset($this->data[$i]);
                    continue 2;
                }

                if ($replaceKey = next($v)) {
                    $item[$replaceKey] = $item[$thisKey];
                    unset($item[$thisKey]);
                }
            }

            $item = array_intersect_key($item, $this->fields);

            $result = new Result();
            static::getClass()::checkFields($result, null, $item);
            if (!$result->isSuccess()) {
                $this->warnings['Invalid'][$i] = $result->getErrorMessages();
                unset($this->data[$i]);
            }
        }

        $this->isValid = true;

        return $this;
    }


    /**
     * @return $this
     * @throws ArgumentException
     * @throws SystemException
     */
    public function prepare()
    {
        if (!$this->isValid) {
            $this->validate();
        }

        $this->toAdd = [];
        $this->toUpdate = [];
        $this->toDelete = [];

        $primary = static::getClass()::getEntity()->getPrimaryArray();

        foreach (static::getRequiredRef() as $k => $v) {
            $this->reference[$k]->exists = $this->reference[$k]::getExist();
        }

        foreach ($this->data as $i => $item) {
            foreach (static::getRequiredRef() as $k => $v) {
                $thisKey = key($v);

                if (!empty($item[$thisKey]) && !empty($refItem = $this->reference[$k]->exists[$item[$thisKey]])) {
                    $item[$thisKey] = $refItem['ID'];
                } else {
                    unset($item[$thisKey]);
                }
            }

            if (empty($exist = $this->findExist($item))) {
                $this->toAdd[] = $item;
            } elseif ($dif = array_diff_assoc($item, $exist)) {
                $this->toUpdate[serialize(array_intersect_key($exist, array_flip($primary)))] = $dif;
            }

        }

        $this->isPrepare = true;

        return $this;
    }

    /**
     * @return Result
     * @throws ArgumentException
     * @throws SystemException
     */
    public function save()
    {
        $result = new Result();

        if (!$this->isPrepare) {
            $this->prepare();
        }

        if (!empty($this->toAdd)) {
            $r = static::getClass()::addMulti($this->toAdd, true);
            if (!$r->isSuccess()) {
                $result->addErrors($r->getErrors());
            }
        }

        if (!empty($this->toUpdate)) {
            $multiChanges = [
                'DATA' => [],
                'COUNT' => 0
            ];
            foreach ($this->toUpdate as $primary => $item) {
                $primary = unserialize($primary);
                foreach ($item as $field => $value) {
                    if (!isset($multiChanges['DATA'][$field][(string)$value])) {
                        $multiChanges['COUNT']++;
                    }
                    $multiChanges['DATA'][$field][(string)$value][] = $primary;
                }
            }

            if (count($this->toUpdate) > $multiChanges['COUNT']) {
                foreach ($multiChanges['DATA'] as $field => $values) {
                    foreach ($values as $value => $primaries) {
                        $r = static::getClass()::updateMulti($primaries, [$field => $value]);
                        if (!$r->isSuccess()) {
                            $result->addErrors($r->getErrors());
                        }
                    }
                }
            } else {
               foreach ($this->toUpdate as $primary => $item) {
                   $primary = unserialize($primary);
                   $r = static::getClass()::update($primary, $item);
                   if (!$r->isSuccess()) {
                       $result->addErrors($r->getErrors());
                   }
               }
            }
        }

        if (!empty($this->toDelete)) {

        }

        $this->exists = static::getExist();

        return $result;
    }

}


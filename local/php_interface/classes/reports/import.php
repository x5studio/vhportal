<?php

@set_time_limit(0);
@ignore_user_abort(true);
@ini_set("memory_limit", "4G");


$currentPath = dirname(__FILE__);

$_SERVER["DOCUMENT_ROOT"] = realpath($currentPath."/../../../../");

$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_STATISTIC","Y");
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_WITH_ON_AFTER_EPILOG', true);
define('BX_NO_ACCELERATOR_RESET', true);

include $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

$t = date('d.m.Y H:i:s');

\Bitrix\Main\Diag\Debug::dumpToFile($t, 'start', 'local/import/import.log');

($client = new Bitrix\Main\Web\HttpClient())
    ->setHeader('Content-Type', 'application/json; charset=UTF-8')
    ->setHeader('Accept', 'application/json; charset=UTF-8')
    ->setAuthorization('WebUser', 'HD(*@#E_)Id_@ObngklzJ"PJKgf#_+@#)E+#)')
    ->setTimeout(600)
    ->setStreamTimeout(600);

$type = "stocks"; // sales / stocks / ratings
$date = new \Bitrix\Main\Type\Date('01.08.2020');

$res = $client->post('http://185.189.100.110:15963/UT11_WORK/hs/b2b/VH/portalinfo', json_encode([
    "type"=> $type,
    "dates" => [
        "from" =>  $date->format('d.m.Y'),
        "to" => $date->add('1M')->add('-1D')->format('d.m.Y')
    ]
]));

$tmpZip = $_SERVER['DOCUMENT_ROOT'] . "/local/import/import.zip";

if ($res) {
    file_put_contents($tmpZip, $res);

    if (($zip = zip_open($tmpZip)) !== false) {
        $file = zip_read($zip);
        zip_entry_open($zip, $file, 'r');
        $json = zip_entry_read($file, zip_entry_filesize($file));
        zip_entry_close($file);

        file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/local/import/{$type}.json", $json);

        $rep = new \VH\Reports\Report(json_decode($json, true));
        $rep->save();
        foreach ($rep->entities as $type => $entity) {
            \Bitrix\Main\Diag\Debug::dumpToFile(['toAdd' => count($entity->toAdd), 'toUpdate' => count($entity->toUpdate), 'warnings' => $entity->warnings], $type, 'local/import/import.log');
         }
    }

} else {
    \Bitrix\Main\Diag\Debug::dumpToFile($client->getError(), 'errors', 'local/import/import.log');
}
\Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'end', 'local/import/import.log');


// Старая версия обмена - Deprecated
/*
$file = $_SERVER['DOCUMENT_ROOT'] . '/local/import/import.json';

$json = json_decode(file_get_contents($file), true);

if (json_last_error() > 0){
    throw new Exception('json_parse_error: ' . json_last_error_msg());
}

\Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'start', 'local/import/import.log');


if (!empty($json['groups'])) {
    (new \VH\Reports\Entities\Group($json['groups']))->identify()->compare()->save();
    \Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'Checkpoint - Group', 'local/import/import.log');
}

if (!empty($json['types'])) {
    (new \VH\Reports\Entities\ProductType($json['types']))->identify()->compare()->save();
    \Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'Checkpoint - ProductType', 'local/import/import.log');
}

if (!empty($json['shops'])) {
    (new \VH\Reports\Entities\Shop($json['shops']))->identify()->compare()->save();
    \Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'Checkpoint - Shop', 'local/import/import.log');
}

if (!empty($json['stores'])) {
    (new \VH\Reports\Entities\Store($json['stores']))->identify()->compare()->save();
    \Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'Checkpoint - Store', 'local/import/import.log');
}

if (!empty($json['products'])) {
    (new \VH\Reports\Entities\Product($json['products']))->identify()->compare()->save();
    \Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'Checkpoint - Product', 'local/import/import.log');
}

if (!empty($json['offers'])) {
    (new \VH\Reports\Entities\Offer($json['offers']))->identify()->compare()->save();
    \Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'Checkpoint - Offer', 'local/import/import.log');
}

if (!empty($json['barcodes'])) {
    (new \VH\Reports\Entities\Barcode($json['barcodes']))->identify()->compare()->save();
    \Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'Checkpoint - Barcode', 'local/import/import.log');
}

if (!empty($json['stocks'])) {
    (new \VH\Reports\Entities\Stock($json['stocks']))->identify()->compare()->save();
    \Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'Checkpoint - Stock', 'local/import/import.log');
}

if (!empty($json['sales'])) {
    (new \VH\Reports\Entities\Sale($json['sales']))->identify()->compare()->save();
    \Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'Checkpoint - Sale', 'local/import/import.log');
}

if (!empty($json['ratings'])) {
    (new \VH\Reports\Entities\Rating($json['ratings']))->identify()->compare()->save();
    \Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'Checkpoint - Rating', 'local/import/import.log');
}



\Bitrix\Main\Diag\Debug::dumpToFile(date('d.m.Y H:i:s'), 'end', 'local/import/import.log');


<?php

namespace VH\Portal;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main;
use Bitrix\Main\ORM\Event;

/**
 * Остатки товара PRODUCT_ID на складе STORAGE_ID
 * на дату DATE
 * Class StocksTable
 * @package VH\Portal
 */
class StocksTable extends DataManager {

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName(){
        return 'cbr_vh_stocks';
    }

    protected static function beforeUpdate(Event $event){
        $r = new Main\ORM\EventResult();
        $fields = $event->getParameter('fields');

        if (is_string($fields['DATE']) && !empty($fields['DATE'])){
            $r->modifyFields([
                'DATE' => new Main\Type\Date($fields['DATE'])
            ]);
        }
        return $r;
    }

    public static function onBeforeAdd(Event $event){
        return self::beforeUpdate($event);
    }

    public static function onBeforeUpdate(Event $event){
        return self::beforeUpdate($event);
    }

    public static function getMap(){
        return [
            new Main\ORM\Fields\IntegerField(
                'ID',
                array(
                    'primary'      => true,
                    'autocomplete' => true,
                )
            ),
            new Main\ORM\Fields\DateField(
                'DATE',
                [
                    'required' => true,
                ]
            ),
            new Main\ORM\Fields\IntegerField(
                'PRODUCT_ID',
                [
                    'required' => true,
                ]
            ),
            new Main\ORM\Fields\IntegerField(
                'STORAGE_ID',
                [
                    'required' => true,
                ]
            ),
            new Main\ORM\Fields\IntegerField(
                'AMOUNT',
                [
                    'required' => true,
                ]
            ),

            new Main\ORM\Fields\Relations\Reference(
                'PRODUCT',
                \Bitrix\Iblock\ElementTable::class,
                array(
                    '=this.PRODUCT_ID' => 'ref.ID'
                )
            ),
            new Main\ORM\Fields\Relations\Reference(
                'STORAGE',
                \Bitrix\Catalog\StoreTable::class,
                array(
                    '=this.STORAGE_ID' => 'ref.ID'
                )
            ),
        ];
    }
}

class ReportDatesTable extends DataManager {

    public static function getTableName(){
        return 'cbr_vh_report_dates';
    }

    public static function getMap(){
        return [
            new Main\ORM\Fields\DateField(
                'DATE',
                array(
                    'primary' => true,
                )
            ),
            new Main\ORM\Fields\DatetimeField('UPDATED'),
            new Main\ORM\Fields\IntegerField('MONTH'),
            new Main\ORM\Fields\IntegerField('YEAR'),
        ];
    }

    public static function onBeforeAdd(Event $event){
        return self::beforeUpdate($event);
    }

    public static function onBeforeUpdate(Event $event){
        return self::beforeUpdate($event);
    }

    protected static function beforeUpdate(Event $event){
        $fields = $event->getParameter("fields");
        if (is_string($fields['DATE']) && !empty($fields['DATE'])){
            $fields['DATE'] = new Main\Type\Date($fields['DATE']);
        }
        if (empty($fields['DATE'])){
            return null;
        }

        $r = new Main\ORM\EventResult();
        $r->modifyFields([
            'DATE'    => $fields['DATE'],
            'UPDATED' => new \Bitrix\Main\Type\DateTime(),
            'MONTH'   => new Main\DB\SqlExpression('MONTH(?s)', $fields['DATE']->format('Y-m-d')),
            'YEAR'    => new Main\DB\SqlExpression('YEAR(?s)', $fields['DATE']->format('Y-m-d')),
        ]);
        return $r;
    }
}
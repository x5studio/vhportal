<?php

function show404(){
    \Bitrix\Iblock\Component\Tools::process404("",true,true,true, false);
}

/**
 * @param string $search
 * @param string $replace
 * @param string $str
 * @param int $limit
 * @return string
 */
function str_replace_limit(string $search, string $replace, string $str, int $limit = 1):string {
    $rStr = $str;
    while ($limit > 0 && ($pos = strpos($rStr, $search)) !== false){
        $rStr = substr($rStr,0, $pos) . $replace . substr($rStr, $pos + strlen($search));
        $limit--;
    }
    return $rStr;
}

function getDateFormat(bool $withTime = false){
    static $format = [];
    if (!$format[$withTime]){
        $format[$withTime] = $GLOBALS['DB']->DateFormatToPHP(CSite::GetDateFormat($withTime ? "FULL" : "SHORT"));
    }
    return $format[$withTime];
}

function addDaysToDate($date, $days){
    return (new DateTime())->setTimestamp($date->getTimestamp())->add(new DateInterval(("P{$days}D")));
}

function getOfferByDate($beginDate, array &$dates, $periodDays = 0, array $props = []){
    if (empty($dates)){
        return null;
    }
    $f = false;
    $defId = null;
    foreach ($dates as $id => $date){
        if (!$date['FROM_DATE'] && !$date['TO_DATE']){
            $defId = $id;
            continue;
        }
        if ($date['FROM_DATE'] <= $beginDate && $beginDate <= $date['TO_DATE']){
            if (empty($props)){
                $f = true;
            } else {
                $ff = true;
                foreach ($date['PROPS'] as $code => $val){
                    if (!isset($props[$code]) || $val['VALUE'] !== $props[$code]){
                        $ff = false;
                        break;
                    }
                }
                $f = $ff;
            }
            if ($f){
                break;
            }
        }
    }
    if (!$f && $defId !== null){
        $f = true;
        $id = $defId;
    }
    if ($f && !empty($id)){
        if ($periodDays > 0 || $periodDays === false){
            $endDate = $periodDays === false ? clone $beginDate : addDaysToDate($beginDate, $periodDays - 1);

            $dates[$id]['FROM'] = $beginDate->format(getDateFormat());
            $dates[$id]['TO'] = $endDate->format(getDateFormat());
            $dates[$id]['FROM_DATE'] = $beginDate;
            $dates[$id]['TO_DATE'] = $endDate;
        }
        return $id;
    }
    return null;
}

function getIblockProperties(int $iblockId){
    $iter = \Bitrix\Iblock\PropertyTable::getList([
        'filter' => ['=IBLOCK_ID' => $iblockId],
        'select' => ['CODE'],
    ]);
    $codes = [];
    while ($prop = $iter->fetch()){
        $codes[] = $prop['CODE'];
    }
    return $codes;
}

function addItemsToShopsGallery(array $gallery){
    $curr = $GLOBALS['SHOPS_GALLERY'];
    if (empty($curr)){
        $curr = [];
    }
    $curr = array_merge($curr, $gallery);
    $GLOBALS['SHOPS_GALLERY'] = $curr;
}

function getYearCompletion(){
    list($dayOfYear, $isLeapYear) = explode(':', date('z:L'));
    return 100 * (((int)$dayOfYear + 1) / ( $isLeapYear == '1' ? 366 : 365));
}

function checkBasketItemCancelDate($beginDate){
    if (is_string($beginDate)){
        $beginDate = \Bitrix\Main\Type\Date::createFromText($beginDate);
    }
    if(empty($beginDate) || ! $beginDate instanceof \Bitrix\Main\Type\Date){
        throw new \Bitrix\Main\ArgumentException('Argument type \'%s\' is not Date');
    }
    /** @var \Bitrix\Main\Type\Date $beginDate */
    $beginDate->add('-2 month');

    return $beginDate->getTimestamp() >= (new \Bitrix\Main\Type\Date())->getTimestamp();
}

function getMonthsNames(){
    return [
        'янв',
        'фев',
        'мар',
        'апр',
        'май',
        'июн',
        'июл',
        'авг',
        'сен',
        'окт',
        'ноя',
        'дек',
    ];
}


/**
 * Ресайз картинки
 * @param array $img
 * @param $width
 * @param $height
 * @param int $resizeType
 * @return string|null
 * @throws ImagickException
 * @throws \Bitrix\Main\ArgumentOutOfRangeException
 */
function getResizeImage(array $img, $width, $height, $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL_ALT){

    if (
        empty($img)
        || empty($width)
        || empty($height)
    ){
        return null;
    }

    if (
        !empty($img['SRC'])
        && (
            empty($img['FILE_NAME'])
            || empty($img['SUBDIR'])
        )
    ){
        $tSrc = explode('/', trim($img['SRC'], '/'));

        // Убираем первый элемент
        $tSrc = array_slice($tSrc, 1);

        $img['FILE_NAME'] = $tSrc[ count($tSrc) - 1 ];

        $tSrc = array_slice($tSrc, 0, count($tSrc) - 1);

        $img['SUBDIR'] = implode('/', $tSrc);
    }

    if (empty($img['FILE_NAME']) || empty($img['SUBDIR'])){
        return null;
    }

    $tmpImg = CFile::ResizeImageGet(
        $img,
        [
            'width'  => $width,
            'height' => $height,
        ],
        $resizeType
    );

    return $tmpImg['src'];
}
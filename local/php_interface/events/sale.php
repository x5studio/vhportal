<?php

/**
 * Добавление свойств торгового предложения в корзину, если не добавлены
 */
$eventManager->addEventHandler('sale', 'OnSaleBasketItemBeforeSaved', [
    SaleEvents::class, 'setBasketItemData'
]);

/**
 * Дополнительная информация по записям корзины
 */
$eventManager->addEventHandler('sale', 'OnSaleOrderDeleted', [
    SaleEvents::class, 'orderDeleted'
]);

/**
 * Удаление доп.информации по записям корзины с заказом
 */
$eventManager->addEventHandler('sale', 'OnSaleOrderSaved', [
    SaleEvents::class, 'addExtBasketItem'
]);


class SaleEvents {
    public static function setBasketItemData(Bitrix\Main\Event $event){
        if (defined('ADMIN_SECTION') && ADMIN_SECTION === true){
            return;
        }
    }

    public static function orderDeleted(Bitrix\Main\Event $event){
        ExtBasketItemTable::deleteByOrder($event->getParameter('ENTITY')->getId());
    }

    public static function addExtBasketItem(Bitrix\Main\Event $event){
        $order = $event->getParameter('ENTITY');
        $isNew = $event->getParameter('IS_NEW') || $order->isNew();
        if (!$isNew){
            return;
        }

        /** @var \Bitrix\Sale\Order $order */

        foreach ($order->getBasket() as $basketItem){
            /** @var \Bitrix\Sale\BasketItem $basketItem */

            $propValues = $basketItem->getPropertyCollection()->getPropertyValues();

            $dates = [];

            $period = $propValues['PERIOD']['VALUE'];

            $tmp = unserialize($period);
            if (!$tmp){
                list($from, $to) = explode('-', $period);
                $from = trim($from);
                $to = trim($to);
                if (!empty($from)){
                    if (empty($to)){
                        $to = $from;
                    }
                    $dates[] = [
                        'FROM' => $from,
                        'TO'   => $to,
                    ];
                }
            } elseif (is_array($tmp)){
                foreach ($tmp as $d){
                    $dates[] = [
                        'FROM' => $d,
                        'TO'   => $d,
                    ];
                }
            }

            foreach ($dates as $d){
                ExtBasketItemTable::add([
                    'ORDER_ID'  => $order->getId(),
                    'BASKET_ID' => $basketItem->getId(),
                    'DATE_FROM' => \Bitrix\Main\Type\Date::createFromText($d['FROM']),
                    'DATE_TO'   => \Bitrix\Main\Type\Date::createFromText($d['TO']),
                ]);
            }
        }
    }

}
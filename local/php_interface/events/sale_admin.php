<?php
/**
 * Добавление своей вкладки при просмотре заказа в админке
 * для просмотра и изменения маркетинг плана
 */
$eventManager->addEventHandler("main", "OnAdminSaleOrderView", array("MarketingPlanAdminOrderFormTabs", "onInit"));

class MarketingPlanAdminOrderFormTabs{
    function onInit(){
        $class = static::class;
        return array(
            "TABSET" => "marketingPlan",
            "GetTabs" => array($class, "getTabs"),
            "ShowTab" => array($class, "showTabs"),
            "Action" => array($class, "onSave"),
            "Check" => array($class, "onBeforeSave"),
        );
    }

    /*
    Возвращает массив вкладок
    */
    function getTabs($args){
        return array(
            array(
                "DIV" => "marketingPlan",
                "TAB" => "Маркетинг план",
                "TITLE" => "Календарь",
                "SORT" => 1
            ),
            array(
                "DIV" => "cancelRequests",
                "TAB" => "Заявки на отмену",
                "TITLE" => "Список",
                "SORT" => 1
            ),
        );
    }

    /**
     * Выводит вкладку
     * @param $tabName
     * @param $args
     * @param $varsFromForm
     */
    function showTabs($tabName, $args, $varsFromForm){
        switch ($tabName) {
            case 'marketingPlan':
                static::showMarketingPlan($args, $varsFromForm);
                break;
            case 'cancelRequests':
                static::showCancelRequests($args, $varsFromForm);
                break;
            default:
                return;
        }
    }

    public static function showCancelRequests($args, $varsFromForm){
        echo "ТЕСТ";
    }

    /**
     * Вывод вкладки с маркетинг планом
     * @param $args
     * @param $varsFromForm
     */
    public static function showMarketingPlan($args, $varsFromForm){

        $tpl = '<tr><td>#CONTENT#</td></tr>';

        ob_start();

        $siteTemplatePath = '/local/templates/portal';

        if (!defined('SITE_TEMPLATE_PATH')){
            define('SITE_TEMPLATE_PATH', $siteTemplatePath);
        }
        global $APPLICATION;

        $divId = "m-plan-{$args['ID']}";

        $APPLICATION->IncludeComponent('vh:marketing.plan', '.default', [
            'ORDER_ID'      => $args['ID'],
            'DIV_ID'        => $divId,
            'ADMIN_MODE'    => 'Y',
            'SHOW_ALL'      => 'Y',

            'HIDE_EMPTY_MONTH' => 'N',
        ]);

        ?>
        <script type="text/javascript">
            (function(){
                let html = document.querySelector('html');
                html.style.fontSize = '1px';

                document.querySelector("#<?= $divId;?>").closest('.adm-detail-content-item-block').style.padding = '4px 0px 30px';
            })();
        </script><?php

        $tpl = str_replace('#CONTENT#', ob_get_contents(), $tpl);
        ob_end_clean();

        echo $tpl;
    }

    /**
     * Вызывается перед onSave
     * Для формы просмотра бесполезно, написано для примера
     */
    function onBeforeSave($args){
        return true;
    }

    /**
     * Вызывается после onBeforeSave при отправке формы
     * Для формы просмотра бесполезно, написано для примера
     */
    function onSave($args){
        return true;
    }
}
<?php

AddEventHandler('main', 'OnUserTypeBuildList', ['CUserTypeExtHlblock', 'GetUserTypeDescription'], 1000);


class CUserTypeExtHlblock extends CUserTypeHlblock
{
    const USER_TYPE_ID = "hlblock";

    const DISPLAY_LIST = 'LIST';
    const DISPLAY_CHECKBOX = 'CHECKBOX';
    const DISPLAY_UI = 'UI';

    function GetEditFormHTMLMulty($arUserField, $arHtmlControl){

        $html = parent::GetEditFormHTMLMulty($arUserField, $arHtmlControl);

        $customHtml = '';
        if ($arUserField['SETTINGS']['DISPLAY'] === self::DISPLAY_UI){
            $customHtml = <<<HTML
<style type="text/css">
.main-ui-select-inner {
    overflow: auto;
    max-height: 400px;
}
</style>
HTML;
        }

        return  $customHtml . $html;
    }

    function GetUserTypeDescription()
    {
        return array(
            "USER_TYPE_ID" => self::USER_TYPE_ID,
            "CLASS_NAME" => "CUserTypeExtHlblock",
            "DESCRIPTION" => GetMessage('USER_TYPE_HLEL_DESCRIPTION') . '(Ext)',
            "BASE_TYPE" => "int",
        );
    }

    public function getFilterData($userField, $additionalParameters)
    {
        $userField['ID'] = $userField['FIELD_NAME'];
        $userField['NAME'] = $userField['LIST_FILTER_LABEL'];
        return parent::getFilterData($userField, $additionalParameters);
    }

    function PrepareSettings($arUserField)
    {
        $r = parent::PrepareSettings($arUserField);

        $display = $arUserField["SETTINGS"]["DISPLAY"];

        if($display!=self::DISPLAY_CHECKBOX && $display!=self::DISPLAY_LIST && $display!=self::DISPLAY_UI){
            $display = self::DISPLAY_LIST;
        }

        $r['DISPLAY'] = $display;

        return $r;
    }

    function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
    {
        $result = '';

        if($bVarsFromForm)
            $hlblock_id = $GLOBALS[$arHtmlControl["NAME"]]["HLBLOCK_ID"];
        elseif(is_array($arUserField))
            $hlblock_id = $arUserField["SETTINGS"]["HLBLOCK_ID"];
        else
            $hlblock_id = "";

        if($bVarsFromForm)
            $hlfield_id = $GLOBALS[$arHtmlControl["NAME"]]["HLFIELD_ID"];
        elseif(is_array($arUserField))
            $hlfield_id = $arUserField["SETTINGS"]["HLFIELD_ID"];
        else
            $hlfield_id = "";

        if($bVarsFromForm)
            $value = $GLOBALS[$arHtmlControl["NAME"]]["DEFAULT_VALUE"];
        elseif(is_array($arUserField))
            $value = $arUserField["SETTINGS"]["DEFAULT_VALUE"];
        else
            $value = "";

        if(CModule::IncludeModule('highloadblock'))
        {
            $dropDown = static::getDropDownHtml($hlblock_id, $hlfield_id);

            $result .= '
			<tr>
				<td>'.GetMessage("USER_TYPE_HLEL_DISPLAY").':</td>
				<td>
					'.$dropDown.'
				</td>
			</tr>
			';
        }

        if($hlblock_id > 0 && strlen($hlfield_id) && CModule::IncludeModule('highloadblock'))
        {
            $result .= '
			<tr>
				<td>'.GetMessage("USER_TYPE_HLEL_DEFAULT_VALUE").':</td>
				<td>
					<select name="'.$arHtmlControl["NAME"].'[DEFAULT_VALUE]" size="5">
						<option value="">'.GetMessage("IBLOCK_VALUE_ANY").'</option>
			';

            $rows = static::getHlRows(array('SETTINGS' => array('HLBLOCK_ID' => $hlblock_id, 'HLFIELD_ID' => $hlfield_id)));

            foreach ($rows as $row)
            {
                $result .= '<option value="'.$row["ID"].'" '.($row["ID"]==$value? "selected": "").'>'.htmlspecialcharsbx($row['VALUE']).'</option>';
            }

            $result .= '</select>';
        }
        else
        {
            $result .= '
			<tr>
				<td>'.GetMessage("USER_TYPE_HLEL_DEFAULT_VALUE").':</td>
				<td>
					<input type="text" size="8" name="'.$arHtmlControl["NAME"].'[DEFAULT_VALUE]" value="'.htmlspecialcharsbx($value).'">
				</td>
			</tr>
			';
        }

        if($bVarsFromForm)
            $value = $GLOBALS[$arHtmlControl["NAME"]]["DISPLAY"];
        elseif(is_array($arUserField))
            $value = $arUserField["SETTINGS"]["DISPLAY"];
        else
            $value = self::DISPLAY_LIST;
        $result .= '
		<tr>
			<td class="adm-detail-valign-top">'.GetMessage("USER_TYPE_ENUM_DISPLAY").':</td>
			<td>
				<label><input type="radio" name="'.$arHtmlControl["NAME"].'[DISPLAY]" value="'.self::DISPLAY_LIST.'" '.(self::DISPLAY_LIST==$value? 'checked="checked"': '').'>'.GetMessage("USER_TYPE_HLEL_LIST").'</label><br>
				<label><input type="radio" name="'.$arHtmlControl["NAME"].'[DISPLAY]" value="'.self::DISPLAY_CHECKBOX.'" '.(self::DISPLAY_CHECKBOX==$value? 'checked="checked"': '').'>'.GetMessage("USER_TYPE_HLEL_CHECKBOX").'</label><br>
				<label><input type="radio" name="'.$arHtmlControl["NAME"].'[DISPLAY]" value="'.self::DISPLAY_UI.'" '.(self::DISPLAY_UI==$value? 'checked="checked"': '').'>UI</label><br>
			</td>
		</tr>
		';

        if($bVarsFromForm)
            $value = intval($GLOBALS[$arHtmlControl["NAME"]]["LIST_HEIGHT"]);
        elseif(is_array($arUserField))
            $value = intval($arUserField["SETTINGS"]["LIST_HEIGHT"]);
        else
            $value = 5;
        $result .= '
		<tr>
			<td>'.GetMessage("USER_TYPE_HLEL_LIST_HEIGHT").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[LIST_HEIGHT]" size="10" value="'.$value.'">
			</td>
		</tr>
		';

        return $result;
    }

    public static function getHlRows($userfield, $clearValues = false)
    {
        global $USER_FIELD_MANAGER;

        $rows = array();

        $hlblock_id = $userfield['SETTINGS']['HLBLOCK_ID'];
        $hlfield_id = $userfield['SETTINGS']['HLFIELD_ID'];

        if (!empty($hlblock_id))
        {
            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
        }

        if (!empty($hlblock))
        {
            $userfield = null;

            if ($hlfield_id == 0)
            {
                $userfield = array('FIELD_NAME' => 'ID');
            }
            else
            {
                $userfields = $USER_FIELD_MANAGER->GetUserFields('HLBLOCK_'.$hlblock['ID'], 0, LANGUAGE_ID);

                foreach ($userfields as $_userfield)
                {
                    if ($_userfield['ID'] == $hlfield_id)
                    {
                        $userfield = $_userfield;
                        break;
                    }
                }
            }

            if ($userfield)
            {
                // validated successfully. get data
                $hlDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();

                $rows = $hlDataClass::getList(array(
                    'select' => array('ID', $userfield['FIELD_NAME']),
                    'order' => empty($userfields['UF_NAME'])
                        ? (empty($userfields['UF_SORT']) ? 'ID' : 'UF_SORT')
                        : 'UF_NAME',
                ))->fetchAll();

                foreach ($rows as &$row)
                {
                    if ($userfield['FIELD_NAME'] == 'ID')
                    {
                        $row['VALUE'] = $row['ID'];
                    }
                    else
                    {
                        //see #0088117
                        if ($userfield['USER_TYPE_ID'] != 'enumeration' && $clearValues)
                        {
                            $row['VALUE'] = $row[$userfield['FIELD_NAME']];
                        }
                        else
                        {
                            $row['VALUE'] = $USER_FIELD_MANAGER->getListView($userfield, $row[$userfield['FIELD_NAME']]);
                        }
                        $row['VALUE'] .= ' ['.$row['ID'].']';
                    }
                }
            }
        }

        return $rows;
    }
}
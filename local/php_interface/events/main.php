<?php

$eventManager = \Bitrix\Main\EventManager::getInstance();


/**
 * События модуля "Интернет-Магазин"
 */
include_once 'sale.php';


/**
 * События модуля "Интернет-Магазин": админка
 */
include_once 'sale_admin.php';


/**
 * Расширение пользовательского поля для привязки к пользователю бренда
 */
include_once 'uf_hlblock_type.php';


/**
 * Генерация UF_XML_ID, если не задан
 */
$eventManager->addEventHandler('', 'PromozonesOnBeforeAdd', function(Bitrix\Main\Entity\Event $event){
    $result = new Bitrix\Main\Entity\EventResult;

    $data = $event->getParameter("fields");

    $newValues = [];

    if (empty($data['UF_XML_ID'])){
        $newValues['UF_XML_ID'] = new Bitrix\Main\DB\SqlExpression('UUID()');
    }

    if (!empty($newValues)){
        $result->modifyFields($newValues);
        return $result;
    }
    return null;
});


/**
 * Отправка письма о регистрации нового пользователя
 */
AddEventHandler("main", "OnAfterUserAdd", "OnAfterUserRegisterHandler");
AddEventHandler("main", "OnAfterUserRegister", "OnAfterUserRegisterHandler");
AddEventHandler("main", "OnAfterUserSimpleRegister", "OnAfterUserRegisterHandler");
function OnAfterUserRegisterHandler(&$arFields){
    if (intval($arFields["ID"])>0){
        $siteId = defined('SITE_ID') && SITE_ID != LANGUAGE_ID ? SITE_ID : 's1';

        $toSend = Array();
        $toSend["PASSWORD"] = $arFields["CONFIRM_PASSWORD"];
        $toSend["EMAIL"] = $arFields["EMAIL"];
        $toSend["USER_ID"] = $arFields["ID"];
        $toSend["USER_IP"] = $arFields["USER_IP"];
        $toSend["USER_HOST"] = $arFields["USER_HOST"];
        $toSend["LOGIN"] = $arFields["LOGIN"];
        $toSend["NAME"] = (trim ($arFields["NAME"]) == "")? $toSend["NAME"] = htmlspecialchars('<Не указано>'): $arFields["NAME"];
        $toSend["LAST_NAME"] = (trim ($arFields["LAST_NAME"]) == "")? $toSend["LAST_NAME"] = htmlspecialchars('<Не указано>'): $arFields["LAST_NAME"];


        CEvent::SendImmediate("MY_NEW_USER", $siteId, $toSend);
    }
    return $arFields;
}

AddEventHandler("main", "OnAfterUserUpdate", function(&$arFields){
    if (
        $_POST["user_info_event"] !== "Y"
        || !$arFields['RESULT']
        || empty($_POST['NEW_PASSWORD_CONFIRM'])
        || $_POST['NEW_PASSWORD_CONFIRM'] !== $_POST['NEW_PASSWORD']
    ){
        return null;
    }

    $arFields['CONFIRM_PASSWORD'] = $_POST['NEW_PASSWORD_CONFIRM'];

    return OnAfterUserRegisterHandler($arFields);
});


/**
 * Запрет восстановления пароля
 */
AddEventHandler('main', 'OnBeforeUserSendPassword', function (&$params){
    global $APPLICATION;
    $APPLICATION->ThrowException('Для восстановления пароля обратитесь к администратору портала: vhportal@visagehall.ru');
    return false;
});
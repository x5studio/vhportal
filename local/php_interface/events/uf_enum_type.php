<?php


use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\ArrayField;
use Bitrix\Main\ORM\Fields\BooleanField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Fields\Validators\LengthValidator;
use Bitrix\Main\ORM\Query\Join;

class CUserTypeExtEnum extends CUserTypeEnum
{
    const USER_TYPE_ID = "enumeration";

    function GetUserTypeDescription()
    {
        $result = parent::GetUserTypeDescription();
        $result['DESCRIPTION'] .= " v2";
        $result['CLASS_NAME'] = static::class;
        return $result;
    }

    function getEntityReferences($userfield, \Bitrix\Main\Entity\ScalarField $entityField)
    {
        if ($userfield['USER_TYPE_ID'] === "enumeration")
        {
            return array(
                new \Bitrix\Main\Entity\ReferenceField(
                    $entityField->getName().'_REF',
                    UserFieldEnumTable::class,
                    ['=this.'.$entityField->getName() => 'ref.ID']
                )
            );
        }

        return array();
    }
}



class UserFieldTable extends DataManager {

    public static function getTableName()
    {
        return "b_user_field";
    }

    public static function getMap()
    {
        return [

            (new IntegerField("ID"))
                ->configurePrimary()
                ->configureAutocomplete(),

            (new StringField("ENTITY_ID"))
                ->addValidator(new LengthValidator(1, 255))
                ->configureRequired(),

            (new StringField("FIELD_NAME"))
                ->addValidator(new LengthValidator(1, 255))
                ->configureRequired(),

            (new StringField("USER_TYPE_ID"))
                ->addValidator(new LengthValidator(1, 255))
                ->configureRequired(),

            (new StringField("XML_ID"))
                ->addValidator(new LengthValidator(1, 255)),

            (new IntegerField("SORT"))
                ->configureRequired()
                ->configureDefaultValue(500),

            (new BooleanField("MULTIPLE"))
                ->configureValues('N', 'Y')
                ->configureDefaultValue('N'),

            (new BooleanField("MANDATORY"))
                ->configureValues('N', 'Y')
                ->configureDefaultValue('N'),

            (new BooleanField("SHOW_FILTER"))
                ->configureValues('N', 'Y')
                ->configureDefaultValue('N'),

            (new BooleanField("SHOW_IN_LIST"))
                ->configureValues('N', 'Y')
                ->configureDefaultValue('N'),

            (new BooleanField("EDIT_IN_LIST"))
                ->configureValues('N', 'Y')
                ->configureDefaultValue('N'),

            (new BooleanField("IS_SEARCHABLE"))
                ->configureValues('N', 'Y')
                ->configureDefaultValue('N'),

            (new ArrayField("SETTINGS"))
                ->configureSerializationPhp(),

        ];
    }

}



class UserFieldEnumTable extends DataManager {

    public static function getTableName()
    {
        return "b_user_field_enum";
    }

    public static function getMap()
    {
        return [

            (new IntegerField("ID"))
                ->configurePrimary()
                ->configureAutocomplete(),

            (new IntegerField("USER_FIELD_ID"))
                ->configureRequired(),

            (new Reference(
                'USER_FIELD',
                \VH\UserFieldTable::class,
                Join::on('this.USER_FIELD_ID', 'ref.ID')
            )),

            (new StringField("VALUE")),

            (new BooleanField("DEF"))
                ->configureValues('N', 'Y')
                ->configureDefaultValue('N'),

            (new IntegerField("SORT"))
                ->configureRequired()
                ->configureDefaultValue(500),

            (new StringField("XML_ID"))
                ->addValidator(new LengthValidator(1, 255))
                ->configureRequired(),

        ];
    }

}
<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php';

if ($_POST['confirm'] !== 'Y') {
    ob_start();
    ?>
    Необходимо согласие с &nbsp;<a target="_blank" href="https://docs.google.com/document/d/11BOcoeMOA4p8cMNIhdbvuaCHmOJzI6l2oGHR8M_mvGI/edit?usp=sharing" style="text-decoration: underline">правилами оформления</a>
    &nbsp; в Visage Hall
    <?php

    $res = (new \Bitrix\Sale\Result())
        ->addError(new \Bitrix\Main\Error(ob_get_contents()));
    ob_end_clean();

} else {

    $basket = \VH\Portal\Helper::getInstance()->getBasket();

    if ($basket->count() > 0){
        $order = \Bitrix\Sale\Order::create(SITE_ID, $USER->GetID());

        $res = $order->setBasket($basket);
    } else {
        $res = (new \Bitrix\Sale\Result())
            ->addError(new \Bitrix\Main\Error('В корзину ничего не добавлено'))
            ->addError(new \Bitrix\Main\Error('Перейдите в нужный раздел, чтобы добавить в корзину'));
    }
}

$success = $res->isSuccess();

if ($success){
    $res = $order->save();
    $success = $res->isSuccess();
}

if (!$success){
    include 'error.php';
} else {

    ?>
    <div class="success-page success-page_bron">
        <div class="success-page__wrap">
            <div class="success-page__icon-wrap">
                <img class="success-page__icon" src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/success.svg" alt="" class="success-page__icon">
            </div>
            <div class="success-page__head">заявка на Подтверждение
                брони отправлена
            </div>

            <div class="success-page__note">
                <div class="mb_25">
                    Ознакомьтесь с <a target="_blank" href="https://docs.google.com/document/d/11BOcoeMOA4p8cMNIhdbvuaCHmOJzI6l2oGHR8M_mvGI/edit?usp=sharing" style="text-decoration: underline">правилами оформления</a>
                    в Visage Hall
                </div>
                <?php /*
                <div class="mb_25">
                    Сформированный договор будет выслан в течении 27 дней, к вам придет уведомление с подтверждением
                    брони.
                </div>
                <div class="mb_75">
                    Договора автоматически появляются в Личном кабинете, в разделах ”Общая информация” и “Договора”
                </div>
                */?>
            </div>


            <div class="success-page__return-link">
                <a href="/personal/orders/" class="btn btn_type-3">
                    <div class="btn__icon-wrap">
                        <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/back-small.svg" alt="" class="btn__icon">
                    </div>
                    Список заявок
                </a>
            </div>


            <div class="success-page__nav">
                <a href="/" class="btn btn_type-3">
                    <div class="btn__icon-wrap">
                        <img src="<?= SITE_TEMPLATE_PATH; ?>/img/icons/logo-small.svg" alt="" class="btn__icon">
                    </div>
                    на главную
                </a>

            <?php/*
            <a href="" class="btn btn_type-3">
                <div class="btn__icon-wrap">
                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/user-menu-3-dark.svg" alt="" class="btn__icon">
                </div>
                Договора
                <span class="btn__note">(личный кабинет)</span>
            </a>
            */ ?>
            </div>
        </div>
    </div>
    <?php
}

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php';

?>
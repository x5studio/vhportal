<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
$aMenuLinksExt = array();

if(CModule::IncludeModule('iblock'))
{
	$arFilter = array(
		"TYPE"      => "catalog",
		"ACTIVE"    => "Y",
		"SITE_ID"   => SITE_ID,
	);

	$dbIBlock = CIBlock::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter);
	$dbIBlock = new CIBlockResult($dbIBlock);

	$cache = Bitrix\Main\Application::getInstance()->getCache();
	$taggedCache = Bitrix\Main\Application::getInstance()->getTaggedCache();

	$result = [];

	$i = 0;
	while ($arIBlock = $dbIBlock->GetNext())
	{
		if(defined("BX_COMP_MANAGED_CACHE"))
            $taggedCache->registerTag("iblock_id_".$arIBlock["ID"]);

		if($arIBlock["ACTIVE"] == "Y")
		{

		    $result[] = [
                $arIBlock['NAME'],
                $arIBlock['LIST_PAGE_URL'],
                '',
                array(
                    'PARENT'        => null,
                    'FROM_IBLOCK'   => true,
                    'IS_PARENT'     => true,
                    'IS_IBLOCK'     => true,
                    'ID'            => $arIBlock['ID'],
                    'DEPTH_LEVEL'   => 1,
                    'PICTURE'       => $arIBlock['PICTURE'],
                    'CHILDREN'      => [],
                )
            ];
		    $iblockIndex = $i;
		    $i++;

            if ($cache->initCache(86400, "menu_{$arIBlock['IBLOCK_TYPE_ID']}:{$arIBlock['ID']}")) {
                $sections = $cache->getVars();
            } else {
                $cache->startDataCache();

                $sectionsIter = CIBlockSection::GetList(['left_margin' => 'asc'], [
                    "IBLOCK_TYPE" => $arIBlock['IBLOCK_TYPE_ID'],
                    "IBLOCK_ID"   => $arIBlock['ID'],
                ], false, ['UF_*']);

                $sections = [];
                while ($section = $sectionsIter->GetNext()) {
                    $sections[$section['ID']] = $section;
                }

                $cache->endDataCache($sections);
            }

            $parents = [];
            foreach ($sections as $section) {
                $parents[$section['ID']] = $i;

                if ($section["DEPTH_LEVEL"] == 1) {
                    $result[$iblockIndex][3]['CHILDREN'][] = [
                        'ID'    => $section['ID'],
                        'INDEX' => $i,
                    ];
                }

                $parent = !empty($section['IBLOCK_SECTION_ID']) ? $section['IBLOCK_SECTION_ID'] : $arIBlock['ID'];
                $result[] = [
                    $section['NAME'],
                    $section['SECTION_PAGE_URL'],
                    '',
                    array(
                        "FROM_IBLOCK"   => true,
                        "ID"            => $section['ID'],
                        "PARENT"        => $parent,
                        "PICTURE"       => $section['PICTURE'],
                        "IS_PARENT"     => false,
                        "IS_IBLOCK"     => false,
                        "DEPTH_LEVEL"   => $section["DEPTH_LEVEL"] + 1,
                        "CHILDREN"      => [],
                        "UF"            => array_filter($section, function ($k){
                            return strpos($k, 'UF_') === 0;
                        }, ARRAY_FILTER_USE_KEY)
                    ),
                ];

                if (
                    $section['IBLOCK_SECTION_ID'] > 0
                    && $parentIndex = $parents[$section['IBLOCK_SECTION_ID']]
                ) {
                    $result[$parentIndex][3]['IS_PARENT'] = true;
                    $result[$parentIndex][3]['CHILDREN'][] = [
                        'ID'    => $section['ID'],
                        'INDEX' => $i,
                    ];
                }
                $i++;
            }

		}
	}

	$statFile = '/stats.pdf';
	$result[] = [
        'О нас',
        $statFile.'?v='.md5_file($_SERVER['DOCUMENT_ROOT'].$statFile).' " target="_blank',
        '',
        array(
            'PARENT'        => null,
            'FROM_IBLOCK'   => false,
            'IS_PARENT'     => false,
            'IS_IBLOCK'     => false,
            'ID'            => null,
            'DEPTH_LEVEL'   => 1,
            'PICTURE'       => null,
            'CHILDREN'      => [],
        )
    ];

    $aMenuLinksExt = $result;

    if(defined("BX_COMP_MANAGED_CACHE"))
        $taggedCache->registerTag("iblock_id_new");
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);

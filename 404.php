<?php
if (!defined('B_PROLOG_INCLUDED')){
    include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
}

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");
define("HIDE_SIDEBAR", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");

$APPLICATION->SetTitle("Страница не найдена");

?>
<div class="success-page success-page_404">
    <div class="p-404__wrap">
        <div class="p-404__icon-wrap">
            <img class="success-page__icon" src="<?= SITE_TEMPLATE_PATH;?>/img//404.png" alt="" class="success-page__icon">
        </div>
        <div class="p-404__hint">упс...</div>
        <div class="p-404__head">404</div>
        <div class="p-404__note">похоже тут что-то сломалось...</div>
        <?php/*
        <div class="success-page__return-link">
            <a href="" class="btn btn_type-3">
                <div class="btn__icon-wrap">
                    <img src="<?= SITE_TEMPLATE_PATH;?>/img/icons/back-small.svg" alt="" class="btn__icon">
                </div>
                вернуться
            </a>
        </div>
        */?>
    </div>
</div>
<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");

?>
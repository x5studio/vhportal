<?
$aMenuLinks = Array(/*
	Array(
		"Общая информация", 
		"", 
		Array(),
        Array(
            'ICON' => 1,
        ),
		"" 
	),*/
	Array(
		"Корзина",
        \VH\Portal\Helper::getInstance()->getBasketUrl(),
		Array(), 
		Array(
		    'POSITIONS' => \VH\Portal\Helper::getInstance()->getUserBasketPositions(),
            'ICON'      => 2,
        ),
		"" 
	),/*
	Array(
		"Договоры", 
		"", 
		Array(), 
		Array(
            'ICON' => 3,
        ),
		"" 
	),
	Array(
		"Финансы", 
		"", 
		Array(),
        Array(
            'ICON' => 4,
        ),
		"" 
	),*/
	Array(
		"Маркетинг-планы",
		"/personal/orders/",
		Array(),
        Array(
            'ICON' => 5,
        ),
		"" 
	),/*
	Array(
		"Отчеты",
		"/personal/reports",
		Array(),
        Array(
            'ICON' => 6,
        ),
		""
	),*/
	Array(
		"Рейтинги",
        "/personal/ratings",
		Array(),
        Array(
            'ICON' => 7,
        ),
		"" 
	),
	Array(
		"Персонал", 
		"/personal/vacancies",
		Array(),
        Array(
            'ICON' => 8,
        ),
		"" 
	),
	Array(
		"Выйти из аккаунта", 
		"/?logout=yes", 
		Array(),
        Array(
            'ICON' => 9,
        ),
		"" 
	)
);
?>
<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

global $USER;
if ($USER->GetID() != 1){
    http_response_code(404);
    die();
}

$output = fopen("php://output",'w');

// UTF-8 fix
fputs($output, chr(0xEF).chr(0xBB).chr(0xBF));

$date = new Bitrix\Main\Type\Date('01.03.2020');

$iter = \VH\Portal\Helper::getReportDataForStocks(
    $date,
    ['bbf819b9-1513-11ea-80d1-9cb654b5a121', '1674f0e0-1514-11ea-80d1-9cb654b5a121'],
    true
);


fputcsv($output, array('Номер', 'Наименование', 'Артикул', 'Штрих-код', 'Остаток'));
while ($i = $iter->fetch()){
    fputcsv($output, [
        $i['i'],
        $i['NAME'],
        $i['ARTNUMBER'],
        $i['BARCODE'],
        $i['AMOUNT'],
    ]);
}

$fileName = "stock_report_{$date->toString()}.csv";

header("Content-Type: application/csv");
header("Content-Disposition: attachment; filename={$fileName}");

fclose($output);